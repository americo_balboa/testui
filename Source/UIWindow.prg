/*
* Aplicacion  :
* Clase       : TWindowsUI
* Descripcion : Panel Contenedor inicial de Ventana sin t�tulo
*               - Clases que heredaran de TWindowUI
*                        - TGrdAppUI   : Ventana con Cuadr�culas
*                        - TSplAppUI   : Ventana con Marco Vertical / separacion
*                        - TGrdSplUI   : Ventana mixta de las anteriores

* Ver documentacion Windows 8 Aplicaciones, en Carpeta Favoritos, Visual 2012
* Describe propiedades, etc. y tipos de Aplicaciones y su comportamiento.
*
*/

#include "colores.ch"
//----------------------------------------------------------------------------//
#include "fivewin.ch"
//#include "dll.ch"

#include "ribbon.ch"
//#include "slider.ch"
#include "xbrowse.ch"

#include "error.ch"

#define NTRIM(n)    ( LTrim( Str( n ) ) )

#define DLG_TITLE "FiveWin for Harbour"
#command QUIT => ( PostQuitMessage( 0 ), __Quit() )

//----------------------------------------------------------------------------//

#define BTN_UP              1
#define BTN_DOWN            2
#define BTN_DISABLE         3
#define BTN_OVERMOUSE       4

#define COL_EXTRAWIDTH        6

#define DT_TOP 0x00000000
#define DT_LEFT 0x00000000
#define DT_CENTER 0x00000001
#define DT_RIGHT 0x00000002
#define DT_VCENTER 0x00000004
#define DT_BOTTOM 0x00000008
#define DT_WORDBREAK 0x00000010
#define DT_SINGLELINE 0x00000020
#define DT_EXPANDTABS 0x00000040
#define DT_TABSTOP 0x00000080
#define DT_NOCLIP 0x00000100
#define DT_EXTERNALLEADING 0x00000200
#define DT_CALCRECT 0x00000400
#define DT_NOPREFIX 0x00000800


//----------------------------------------------------------------------------//
Static aWindowsUI  := {}
Static aActivos    := {}
Static aBmpsWndUI  := {}
Static aUIConex    := {}

static nLevelAlpha       := 255
static bUserAction
//----------------------------------------------------------------------------//

CLASS TWindowsUI //FROM TMDiFrame

      CLASSDATA lRegistered AS LOGICAL
      DATA   lDemoUI                     // Hacer aplicaci�n Demo
      DATA   lMdiUI                      // Simular entorno MDI
      DATA   oWndUI                      // Si no hereda de TMDIFrame
      DATA   bWndUI                      // Funcion crear ventana
      DATA   bInitUI
      DATA   bControls
      DATA   cBmpWndUI
      DATA   nPosUI
      DATA   lSaveBmp
      DATA   lDlgsParent                 // Heredar Dlgs Laterales
      DATA   oParentUI

      DATA   nTpMonitor
      DATA   nPorWidth
      DATA   nPorHeight

      DATA   bInitUserUI
      DATA   oInitUserUI

      DATA   aPos
      DATA   lPosInit      // Prioridad de bInitUserUI -> .T. // DEFAULT -> .F.
      DATA   lMax
      DATA   lRepaintUI

      DATA   nRowBttUI
      DATA   nColBttUI
      DATA   nRowBttIcoUI
      DATA   nColBttIcoUI

      DATA   lTitUI
      DATA   cTitUI
      DATA   bTitUI
      DATA   oTitUI

      DATA   lTUserUI
      DATA   cTUserUI
      DATA   bTUserUI
      DATA   oTUserUI

      DATA   lRbbUI
      DATA   oRbbUI
      DATA   bRbbUI

      DATA   lBarUI
      DATA   oBarUI
      DATA   bBarUI

      DATA   lFldUI
      DATA   oFldUI
      DATA   bFldUI

      DATA   lFldExUI
      DATA   oFldExUI
      DATA   bFldExUI

      DATA   lXBrwUI
      DATA   oXBrwUI
      DATA   bXBrwUI

      DATA   lXMnuUI
      DATA   oXMnuUI
      DATA   bXMnuUI

      DATA   lPnelUI
      DATA   oPnelUI
      DATA   bPnelUI
      DATA   nRowPnelUI
      DATA   nColPnelUI

      DATA   lWPnelUI
      DATA   oWPnelUI
      DATA   bWPnelUI
      DATA   cBmpPnel
      DATA   nRowWPnelUI
      DATA   nColWPnelUI

      DATA   lBttIconUI
      DATA   oBtnIconUI
      DATA   bFilIconUI        // Hacer Boton con File externo
      DATA   bValIconUI
      DATA   aBttIconUI

      DATA   lBttExitUI
      DATA   oBtnExitUI
      DATA   bFilExitUI        // Hacer Boton con File externo
      DATA   bValExitUI
      DATA   aBttExitUI

      DATA   lBttMiniUI
      DATA   oBtnMiniUI
      DATA   bFilMiniUI
      DATA   aFilMiniUI

      DATA   lBttMaxiUI
      DATA   oBtnMaxiUI
      DATA   bFilMaxiUI
      DATA   aFilMaxiUI

      DATA   lBttNormUI
      DATA   oBtnNormUI
      DATA   bFilNormUI
      DATA   aFilNormUI

      DATA   nStyleUI

      DATA   oFontTitleUI
      DATA   oFontUserUI
      DATA   oFontXBrowUI
      DATA   oFontApliUI
      DATA   oFontGrXbUI
      DATA   oFontTileUI
      DATA   oFontGroupUI
      DATA   oFontBottomUI
      DATA   oFontDlgUI

      DATA   nColorFontUI       // �?
      DATA   nColorForeUI       // �?
      DATA   nColorBackUI       // �?

      DATA   aBmpsXUI           // Array Bitmaps MenuUI Inicial
      DATA   aTitsGrps          // Titulos de grupos del Menu Inicial

      DATA   lDlgLeft           // Crear Panel izquierdo
      DATA   lDlgRight          // Crear Panel derecho
      DATA   lDlgBottom         // Crear Panel inferior
      DATA   lDlgTop            // Crear Panel superior

      DATA   lDialogoR          // Panel derecho esta Activo/Inactivo
      DATA   oMainBarR          // Objeto Dialogo (Bar) Derecho
      DATA   lDialogoL          // Panel izquierdo Activo/Inactivo
      DATA   oMainBarL          // Objeto Dialogo (Bar) Izquierdo
      DATA   lDialogoT          // Panel superior Activo/Inactivo
      DATA   oMainBarT          // Objeto Dialogo (Bar) Superior
      DATA   lDialogoB          // Panel inferior Activo/Inactivo
      DATA   oMainBarB          // Objeto Dialogo (Bar) Inferior

      // A�adido Cnl 10/08/2014
      DATA   aSplitsV           // Contenedor de Splits Verticales
      DATA   aSplitsH           // Contenedor de Splits Horizonatales
      DATA   aPnels             // Contenedor de Paneles en Dialogos
      DATA   aWPnels            // Contenedor de Peneles en Dialogos
      DATA   aOBars             // Contenedor de Bar en Dialogos/Ventanas
      //

      DATA   nDlgWidth

      DATA   cFichBmps          // Plantilla nombres BMPs pantallas guardadas

      DATA   oUIConex

      DATA   bEnd               // Codeblock a ejecutar al cerrar la ventana
                                // por ejemplo, cerrar conexiones, etc.

      DATA   bUIMenu

      DATA   lDrag

      //DATA   aWindowsUI

      METHOD New( lWin, lMax, lMdi, nColor, nStyl, oWnd, bWnd ) CONSTRUCTOR
      METHOD HazWinUI( lWin, lMax, lMdi, nColor, nStyl, oWnd, bWnd )
      METHOD PnelPPal( lWin, lMax, lMdi, nColor, cTit, cUser, aBtt ) CONSTRUCTOR
      METHOD ActivaUI()
      METHOD End()
      METHOD IniFunUI()
      METHOD Initiate(  lWin, lMax , lMdi, nColor, nStyl, oWnd, bWnd  )
      METHOD PaintUI()
      METHOD NoTitle()
      METHOD NewUI( nR, nC, nH, nW )
      METHOD SetStyle( nStyl, nSection )
      METHOD SetBrush( nStyl, nSection, nColor )
      METHOD SetTitleUI( cTit , nRow, nCol, nPos, oUI, aColors  )
      METHOD SetTUserUI( cTit , nRow, nCol, nPos, oUI, aColors, oFont  )
      METHOD SetBarUI( aPromptsUI, nWidthUI, nHeightUI, oUI )
      METHOD SetXBrwUI( oDlg, aPrompt, aSizes, aDatos, aBmps )
      METHOD MnuXBrwUI( oUI, aPrompt, aSizes, aDatos, aBmps )
      METHOD SetSpltUI( oUI )
      METHOD SetPnelUI( oUI, nCol1, nCol2, nHt, oBrh, nColor )
      METHOD WndPnelUI( oUI, nRow1, nCol1, nCol2, nHt, oBrh, nColor, cBmp )
      METHOD SetMenuUI()
      METHOD SetQuickstartUI()

      METHOD SRightUpUI( oWnd, bControls, nWd, nColor, oFont1 )
      METHOD SLeftUpUI( oWnd, bControls, nWd, nColor, oFont1 )
      METHOD STopUpUI( oWnd, bControls, nWd, nColor, oFont1 )
      METHOD SBottUpUI( oWnd, bControls, nWd, nColor, oFont1 )

      METHOD LoadMdiUI( lTxt, nWd )
      //METHOD SetPanelUI( nRow, nCol, nKeyF )
      METHOD MouseMoveUI( nRow, nCol, nKeyF )
      METHOD LButtonDownUI( nRow, nCol, nFlags )
      METHOD LButtonUpUI( nRow, nCol, nFlags )

      METHOD SetImgUI( aBmps, nH, nW, oButton, nPos  )
      METHOD GetListBmps()
      METHOD GetListWnds()
      METHOD GetListActivos()
      METHOD GetListConex()
      METHOD SetListConex( oCn )
      METHOD PosWndUI()
      METHOD GetPosUI( oWnd )

      METHOD SetFoco()
      METHOD UIXPnel( oDlg, aBmps, aTGrps, nSzBmp, aTxtBmps, bActions, nWidth, nColor )

      METHOD TipoMonitor()
      METHOD CalculaRes( nTp ) //nTp ->0 Ancho - nTp ->1 Alto
      METHOD VerDlgs( nRow, nCol, nKeyFlags, oWnd )
      METHOD XBrwElige( nPos, oBrw, x, y, nRow, nCol, nF, oCol )
      METHOD MiMMove( nRow, nCol, nKeyFlags, bMove, oWnd )
      METHOD SetMnuLeft( oP, x, y )

      METHOD SetWndMnu( oMnu )

ENDCLASS

//----------------------------------------------------------------------------//

METHOD New( lWin, lMax , lMdi, nColor, nStyl, oWnd, bWnd ) CLASS TWindowsUI

DEFAULT lWin          := .F.
DEFAULT lMax          := .F.
DEFAULT lMdi          := .F.
DEFAULT nColor        := METRO_NAPPWORKSPACE
DEFAULT nStyl         := 0
DEFAULT oWnd          := Nil
DEFAULT bWnd          := Nil

   ::TipoMonitor()
   ::CalculaRes()

   ::Initiate(  lWin, lMax , lMdi, nColor, nStyl, oWnd, bWnd )

   if lWin
      ::HazWinUI( lWin, lMax, lMdi, nColor, nStyl, oWnd, bWnd )
   endif

Return Self

//----------------------------------------------------------------------------//

METHOD Initiate(  lWin, lMax , lMdi, nColor, nStyl, oWnd, bWnd ) CLASS TWindowsUI

   ::lDemoUI        := .F.
   ::lRepaintUI     := .T.
   ::bEnd           := { || Self:End() }

   ::lMax           := lMax
   ::lMdiUI         := lMdi
   ::nStyleUI       := nStyl
   ::oParentUI      := oWnd
   ::bWndUI         := bWnd

   ::lSaveBmp       := .F.
   ::lDlgsParent    := .F.

   ::aBmpsXUI       := {}
   ::aTitsGrps      := {}

   ::lDlgLeft       := .F.
   ::lDlgRight      := .F.
   ::lDlgBottom     := .F.
   ::lDlgTop        := .F.

   ::lDialogoR      := .F.    // El Panel derecho esta Activo/Inactivo
   ::lDialogoL      := .F.
   ::lDialogoT      := .F.
   ::lDialogoB      := .F.

   ::nDlgWidth      := 64
   ::cFichBmps      := "WndUI"

   ::lPosInit       := .F.

   ::lBttIconUI     := .F.
   ::aBttIconUI     := {,,,}
   ::bValIconUI     := Nil

   ::lBttExitUI     := .F.
   ::bValExitUI     := { || .T. }
   ::aBttExitUI     := { , , 48, 20 }

   ::lBttMaxiUI     := .F.
   ::aFilMaxiUI     := { , }

   ::lBttMiniUI     := .F.
   ::aFilMiniUI     := { , }

   ::lBttNormUI     := .F.
   ::aFilNormUI     := { , }

   ::nRowBttUI      := 1
   ::nColBttUI      := ScreenWidth() - 26 //::oWndUI:nWidth - 24   //1

   ::nRowBttIcoUI   := 1
   ::nColBttIcoUI   := 1

   ::lBarUI         := .F.
   ::lFldExUI       := .F.
   ::lFldUI         := .F.
   ::lRbbUI         := .F.
   ::lTitUI         := .F.
   ::lPnelUI        := .F.
   ::nRowPnelUI     := 0
   ::nColPnelUI     := 0
   ::lWPnelUI       := .F.
   ::cBmpPnel       := ""
   ::nRowWPnelUI    := 0
   ::nColWPnelUI    := 0
                                             //SemiLight //BOLD
   ::oFontTitleUI   := TFont():New("Segoe UI Light", 0 , -54 )    //-12
   ::oFontUserUI    := TFont():New("Segoe UI SemiLight", 0 , -24 )
   ::oFontApliUI    := TFont():New("Segoe UI Light", 0 , -22 )

   ::aPos           := { 0, 0 } //, 0, 0 , .F. )

   ::bUIMenu        := Nil
   ::lDrag          := .F.

Return nil

//----------------------------------------------------------------------------//

METHOD HazWinUI( lWin, lMax, lMdi, nColor, nStyl, oWnd, bWnd ) CLASS TWindowsUI
local oMnu
local oBrush
local nPos

   if lWin

    MENU oMnu
    ENDMENU

    oBrush           := ::SetBrush( , , nColor )

    if Valtype( ::bWndUI ) <> "B"

      if ::lMdiUI

         DEFINE WINDOW ::oWndUI MDI ;
                MENU oMnu ;  //          OF aWindowsUI[1] ;
                BRUSH oBrush
                //::oWndUI:nStyle := nAnd( ::oWndUI:nStyle, WS_CLIPCHILDREN )
      else

         if !Empty( aWindowsUI )

            if !empty( oWnd )

               nPos  := ::GetPosUI( oWnd:oWndUI )

               if !empty( nPos )

                  DEFINE WINDOW ::oWndUI ;
                      MENU oMnu ;
                      OF aWindowsUI[ nPos ] ;
                      BRUSH oBrush

               else

                  DEFINE WINDOW ::oWndUI ;
                      MENU oMnu ;
                      OF oWnd:oWndUI   ;
                      BRUSH oBrush

               endif

            else

               DEFINE WINDOW ::oWndUI ;
                   MENU oMnu ;
                   BRUSH oBrush
            endif

         else

            DEFINE WINDOW ::oWndUI ;
                MENU oMnu ;
                BRUSH oBrush

         endif

      endif

    else

      ::oWndUI      := Eval( ::bWndUI, oWnd )

    endif

    ::NoTitle()

    oBrush:End()

   endif
   oBrush           := Nil
   oMnu             := Nil

Return  nil

//----------------------------------------------------------------------------//

METHOD PnelPPal( lWin, lMax, lMdi, nColor, cTit, cUser, aBtt ) CLASS TWindowsUI
Local oObjUI
Local nRowTit1  := 42
Local nColTit1  := 120
Local nColorFnt := METRO_GRIS2

DEFAULT lWin    := .F.
DEFAULT lMax    := .F.
DEFAULT lMdi    := .F.
DEFAULT nColor  := METRO_AZUL3
DEFAULT cTit    := "Inicio"
DEFAULT cUser   := WNetGetUser()
DEFAULT aBtt    := { ".\Res\Guest32.bmp", ".\Res\Guest32.bmp", 32, 32 }
if nColor <> METRO_WINDOW
   nColorFnt := METRO_WINDOW
endif

   //oObjUI := ::New( lWin, lMax, lMdi, nColor,,,,)
   ::TipoMonitor()
   ::CalculaRes()

   ::Initiate(  lWin, lMax , lMdi, nColor, , ,  )

   if lWin
      ::HazWinUI( lWin, lMax, lMdi, nColor, , ,  )

      ::oWndUI:OnMouseMove := { | o, nR, nC, nKF | Self:MouseMoveUI( o, nR, nC, nKF ) } // Self:SetPanelUI( nRow, nCol, nKeyF ) }
      ::oWndUI:bLClicked   := { | nR, nC, nKF, o | Self:LButtonDownUI( nR, nC, nKF, o ) }

      ::lDemoUI        := .F.
      if !empty( aBtt )
         ::lBttIconUI     := .T.
         if Len( aBtt ) = 1
            ::aBttIconUI     := aBtt
         else
            ::aBttIconUI     := aBtt[ 1 ]
         endif
         // Posibilidad de definir posicion del IconoBmp - Y los demas botones
         ::nRowBttIcoUI  :=  74
         ::nColBttIcoUI  := GetSysMetrics(0) - 74 - ( 36 * ( Len( aBtt ) - 1 ) )
      endif

      ::SetTitleUI( cTit , nRowTit1, nColTit1, 0, , ;
                    { nColorFnt, nColor }, ::oFontTitleUI )
                                                         // 100
      ::SetTUserUI( cUser, ::nRowBttIcoUI - 4 , ;
                 ::nColBttIcoUI - (Len( cUser )+2)*::oFontUserUI:nWidth, 0, , ;
                 { nColorFnt, nColor }, ::oFontUserUI )

   endif

Return Self //oObjUI

//----------------------------------------------------------------------------//

METHOD ActivaUI() CLASS TWindowsUI
local oUI            := if( ::lMdiUI, ::oWndUI:oWndClient, ::oWndUI )
local cShow          := IF( ::lMax, "MAXIMIZED", "NORMAL" )
local bMove

if !empty( ::oWndUI )
   if ::lMax
      ::lBttMaxiUI    := .F.
      ::lBttNormUI    := .F.
   endif

   ::oWndUI:bPainted    := { | hDC, cPS | Self:PaintUI() }
   if Empty( ::oWndUI:bInit )
      ::oWndUI:bInit       := { | o | Self:IniFunUI() }
   endif
   ::oWndUI:bValid      := ::bValExitUI

   ::cBmpWndUI      := Upper( RTrim( ::cFichBmps ) ) + ;
                       StrZero( Len( aWindowsUI ) + 1, 3 )+".BMP"

   AAdd( aWindowsUI, ::oWndUI )
   AAdd( aActivos, Self )
   AAdd( aUIConex, {} )
   if ::lSaveBmp
      //FIMakeThumbNail( cSrcFile, cDstFile, nSize )
      //FIMakeThumbNail( ::cBmpWndUI, cFileNoExt( ::cBmpWndUI )+".PNG", 128 )
      AAdd( aBmpsWndUI, { ::cBmpWndUI, ;
                          if( empty( ::nPosUI ), ::cTitUI, ;
                          (if( empty( ::cTUserUI ), ::cTitUI, ::cTUserUI )))} )
//                          if( empty( ::cTUserUI ), ::cTitUI, ::cTUserUI ) } )
   endif
   ::nPosUI := Len( aWindowsUI )

   if ::lDlgsParent
      if ::nPosUI > 1
         if !::lDlgLeft .and. !::lDlgRight .and. !::lDlgBottom .and. !::lDlgTop
           if empty( ::oWndUI:bMMoved ) .or. Valtype( ::oWndUI:bMMoved ) <> "B"
              if !empty( ::oParentUI )
                 if  !empty( ::oParentUI:oWndUI:bMMoved ) .and. ;
                    Valtype( ::oParentUI:oWndUI:bMMoved ) = "B"
                 //if  !empty( ::oParentUI:bMMoved ) .and. ;
                 //   Valtype( ::oParentUI:bMMoved ) = "B"
                    ::oWndUI:bMMoved  := ::oParentUI:oWndUI:bMMoved
                 //   ::oWndUI:bMMoved  := ::oParentUI:bMMoved
                 endif
              endif
           endif
         else
           if ::lDlgTop  .or. ::lDlgRight .or. ::lDlgBottom
              if !empty( ::oParentUI )
                 bMove := ::oParentUI:oWndUI:bMMoved
                 //bMove := ::oParentUI:bMMoved
                 ::oWndUI:bMMoved  := { | nR, nC, nKF | ::MiMMove( nR, nC, nKF, ;
                                                        bMove, )} //Self
              endif
           else
              ::oWndUI:bMMoved  := ::oParentUI:oWndUI:bMMoved
              //::oWndUI:bMMoved  := ::oParentUI:bMMoved
           endif
         endif
      endif
   else
      /*
      if ::nPosUI > 1
           if ::lDlgTop  .or. ::lDlgRight .or. ::lDlgBottom
              if !empty( ::oParentUI )
                 if Upper( ::oParentUI:oWndUI:ClassName() ) == "TWINDOW"
                 //::oParentUI:oWndUI:Hide()
                 bMove := ::oParentUI:oWndUI:bMMoved
                 //bMove := ::oParentUI:bMMoved
                 ::oWndUI:bMMoved  := { | nR, nC, nKF | ::MiMMove( nR, nC, nKF,;
                                                        bMove, ) } //Self
                 endif
              endif
           else
              //if Upper( ::oParentUI:oWndUI:ClassName() ) == "TWINDOW"
                 //::oParentUI:oWndUI:Hide()
                 ::oWndUI:bMMoved  := ::oParentUI:oWndUI:bMMoved
                 //::oWndUI:bMMoved  := ::oParentUI:bMMoved
              //endif
           endif
      endif
      */
   endif

 // OJO: Si es "NORMAL" y no hay coordenadas, simplemente quitar del alto,
 //      el alto de la barra de tareas para que se pueda ver. Tipo GITHUB

   /*
   ::cBmpWndUI      := Upper( RTrim( ::cFichBmps ) ) + ;
                       StrZero( Len( aWindowsUI ) + 1, 3 )+".BMP"

   AAdd( aWindowsUI, ::oWndUI )
   AAdd( aActivos, Self )
   AAdd( aUIConex, {} )
   if ::lSaveBmp
      //FIMakeThumbNail( cSrcFile, cDstFile, nSize )
      //FIMakeThumbNail( ::cBmpWndUI, cFileNoExt( ::cBmpWndUI )+".PNG", 128 )
      AAdd( aBmpsWndUI, { ::cBmpWndUI, ;
                          if( empty( ::nPosUI ), ::cTitUI, ;
                          (if( empty( ::cTUserUI ), ::cTitUI, ::cTUserUI )))} )
//                          if( empty( ::cTUserUI ), ::cTitUI, ::cTUserUI ) } )
   endif
   ::nPosUI := Len( aWindowsUI )
   */

   ::oWndUI:Activate( cShow , , , , , , , , , , , , , , , , , , )
endif
Return nil

//----------------------------------------------------------------------------//

METHOD End() CLASS TWindowsUI
Local x
Local y
Local nPos
if !empty( ::oWndUI )
   if ::lMdiUI
      For x = 1 to len( ::oWndUI:oWndClient:aWnd )
          ::oWndUI:oWndClient:aWnd[ x ]:End()
      Next x
   else
      if !empty( ::oMainBarL )
         ::oMainBarL:End()
      endif
      if !empty( ::oMainBarR )
         ::oMainBarR:End()
      endif
      if !empty( ::oMainBarT )
         ::oMainBarT:End()
      endif
      if !empty( ::oMainBarB )
         ::oMainBarB:End()
      endif

      //::nPosUI
      nPos := AScan( aWindowsUI,;
                 { | o | ValType( o ) == "O" .and. o:hWnd == ::oWndUI:hWnd } )

      if nPos > 0
         //if nPos <> ::nPosUI      // OJO

         For x = 1 to len( aUIConex )
             For y = 1 to Len( aUIConex[ x ] )
                 if !empty( aUIConex[ x ][ y ] ) .and. ;
                    Valtype( aUIConex[ x ][ y ] ) = "O"
                    aUIConex[ x ][ y ]:Close()
                    aUIConex[ x ][ y ]  := Nil
                 endif
             Next y
         Next x

         ADel( aWindowsUI, nPos )
         ASize( aWindowsUI, Len( aWindowsUI ) - 1 )
         ADel( aBmpsWndUI, nPos )
         ASize( aBmpsWndUI, Len( aBmpsWndUI ) - 1 )
         ADel( aActivos, nPos )
         ASize( aActivos, Len( aActivos ) - 1 )
         ADel( aUIConex, nPos )
         ASize( aUIConex, Len( aUIConex ) - 1 )

         // Borrar Fichero ::cBmpWndUI
         // OJO: sacar fuera del if ?
         if file( ::cBmpWndUI )
            FErase( ::cBmpWndUI )
         endif

      endif

   endif

   ::oWndUI:End()

   if Len( aWindowsUI ) > 0
      For x = 1 to Len( aWindowsUI )
          // Recalcular posiciones de Ventanas ( ::nPosUI ) y Conexiones
      Next x
   else
      /*
      if !Empty( ::oBtnMaxiUI:hBitmap1 )
         DeleteObject( ::oBtnMaxiUI:hBitmap1 )
         ::oBtnMaxiUI:ReleaseDC()
         ::oBtnMaxiUI:End()
      endif
      if !Empty( ::oBtnMiniUI:hBitmap1 )
         DeleteObject( ::oBtnMiniUI:hBitmap1 )
         ::oBtnMiniUI:ReleaseDC()
         ::oBtnMiniUI:End()
      endif
      if !Empty( ::oBtnNormUI:hBitmap1 )
         DeleteObject( ::oBtnNormUI:hBitmap1 )
         ::oBtnNormUI:ReleaseDC()
         ::oBtnNormUI:End()
      endif
      if !Empty( ::oBtnExitUI:hBitmap1 )
         DeleteObject( ::oBtnExitUI:hBitmap1 )
         ::oBtnExitUI:ReleaseDC()
         ::oBtnExitUI:End()
      endif
      */

      while ::oFontTitleUI:nCount > 0
            ::oFontTitleUI:End()
      end
      while ::oFontUserUI:nCount > 0
            ::oFontUserUI:End()
      end
      while ::oFontApliUI:nCount > 0
            ::oFontApliUI:End()
      end
   endif

   ::oFontTitleUI:End()
   ::oFontUserUI:End()
   ::oFontApliUI:End()

   ::oFontTitleUI := Nil
   ::oFontUserUI  := Nil
   ::oFontApliUI  := Nil

endif

   Self    := Nil

Return nil

//----------------------------------------------------------------------------//

METHOD GetListBmps() CLASS TWindowsUI
Return aBmpsWndUI

//----------------------------------------------------------------------------//

METHOD GetListWnds() CLASS TWindowsUI
Return aWindowsUI

//----------------------------------------------------------------------------//

METHOD GetListConex() CLASS TWindowsUI
Return aUIConex

//----------------------------------------------------------------------------//

METHOD GetListActivos() CLASS TWindowsUI
Return aActivos
//----------------------------------------------------------------------------//

METHOD SetListConex( oCn ) CLASS TWindowsUI
if !empty( oCn )
   AAdd( aUIConex, oCn )
endif
Return if( empty( oCn ), 0 , Len( aUIConex ) )

//----------------------------------------------------------------------------//

METHOD GetPosUI( oWnd ) CLASS TWindowsUI
Local nPos
      nPos := AScan( aWindowsUI,;
                  { | o | ValType( o ) == "O" .and. o:hWnd == oWnd:hWnd } )
Return nPos

//----------------------------------------------------------------------------//

METHOD PosWndUI() CLASS TWindowsUI
Return ::nPosUI

//----------------------------------------------------------------------------//

METHOD SetFoco() CLASS TWindowsUI
       ::oWndUI:SetFocus()
Return ::oUIConex

//----------------------------------------------------------------------------//

METHOD PaintUI() CLASS TWindowsUI
local oUI            := if( ::lMdiUI, ::oWndUI:oWndClient, ::oWndUI )
local nAlto          := 0
local x
   //::lBttIconUI .or.
if ::lBttExitUI .or. ::lBttMaxiUI .or. ;
   ::lBttMiniUI .or. ::lBttNormUI .or. ::lTitUI
   //nAlto      := 26
endif

if ::lMdiUI
   For x = 1 to Len( oUI:aWnd )
      oUI:nTop   := nAlto
      WndWidth(  oUI:aWnd[x]:hWnd, Self:oWndUI:nWidth )
      WndHeight( oUI:aWnd[x]:hWnd, Self:oWndUI:nHeight - nAlto )
   Next x
endif

if !::lRepaintUI
   ::IniFunUI( .F. )
   ::lRepaintUI    := .T.
endif
Return nil

//----------------------------------------------------------------------------//

METHOD NoTitle() CLASS TWindowsUI
Return SetWindowLong( ::oWndUI:hWnd, -16, "L" )

//----------------------------------------------------------------------------//

METHOD IniFunUI( lChild ) CLASS TWindowsUI
local  oUI
local  x
local  cBmpIco4      := Nil
local  bBttIco       := Nil //{ || Self:oWndUI:End() }
local  nWdIco        := 0
local  nHgIco        := 0
local  bBttAct       := { || Self:End() }  //Self:oWndUI:End() }
local  bBttMax       := { || IF (!IsZoomed( Self:oWndUI:hWnd ),;
                                ( Self:lRepaintUI := .F., Self:lMax := .T., ;
                                  Self:oWndUI:Maximize() ),; // , Self:IniFunUI() ), ;
                                .T. ) }
local  bBttMin       := { || IF (!IsIconic( Self:oWndUI:hWnd ) ,;
                                ( Self:oWndUI:Minimize() ), ;
                                .T. ) }
local  bBttNor       := { || IF ( ( IsZoomed( Self:oWndUI:hWnd ) .OR. ;
                                    IsIconic( Self:oWndUI:hWnd ) ), ;
                                ( Self:lRepaintUI := .F., Self:lMax := .F. , ;
                                  Self:oWndUI:Restore() ) , ;//, Self:IniFunUI() ), ;
                                .T. ) }

local  nRowBtt       := ::nRowBttUI
local  nColBtt       := ::nColBttUI
local  aBttExit      := ::aBttExitUI
local  nRowBttIco    := ::nRowBttIcoUI
local  nColBttIco    := if( ::nColBttIcoUI < 0, ::oWndUI:nWidth - 26, ::nColBttIcoUI )   // if( ::lWPnelUI, 271, 1 )
local  nSpaceBtt     := 0
local  nSpaceUI      := 32

local  lBlockTit     := IF( ValType( ::bTitUI    ) = "B", .T. , .F. )
local  lBlockPnel    := IF( ValType( ::bPnelUI   ) = "B", .T. , .F. )
local  lWBlockPnel   := IF( ValType( ::bWPnelUI  ) = "B", .T. , .F. )
local  lBlockBar     := IF( ValType( ::bBarUI    ) = "B", .T. , .F. )
local  lBlockRbb     := IF( ValType( ::bRbbUI    ) = "B", .T. , .F. )
local  lBlockFld     := IF( ValType( ::bFldUI    ) = "B", .T. , .F. )
local  lBlockFldEx   := IF( ValType( ::bFldExUI  ) = "B", .T. , .F. )
local  lBlockXBrw    := IF( ValType( ::bXbrwUI   ) = "B", .T. , .F. )
local  lBlockXMnu    := IF( ValType( ::bXMnuUI   ) = "B", .T. , .F. )
local  lBlockUser    := IF( ValType( ::bInitUserUI ) = "B", .T. , .F. )
local  lMover        := IF( ValType( ::aPos ) = "A", .T. , .F. )
local  lControls     := IF( ValType( ::bControls ) = "B", .T. , .F. )
local  oCode

DEFAULT lChild       :=  Self:lMdiUI    //.F.

if empty( ::aWPnels )
   ::aWPnels := {}
endif
if empty( ::aPnels )
   ::aPnels := {}
endif

if ::lDlgLeft .or. ::lDlgRight .or. ::lDlgTop .or. ::lDlgBottom
   if empty( ::oWndUI:bMMoved )
      ::oWndUI:bMMoved  := { | nR, nC, nF | ::VerDlgs( nR, nC, nF, ) }
   endif
endif
       if lMover .and. !::lMax //.and. ::lRepaintUI
          if Len( ::aPos ) > 2
             ::oWndUI:Move( ::aPos[1], ::aPos[2], ::aPos[3], ::aPos[4], ::aPos[5] )
          else
             ::oWndUI:Move( ::aPos[1], ::aPos[2], , )
          endif
       endif

       if lChild
          ::NewUI()
          if ::lRbbUI
             if !Empty( ::oRbbUI )
                SetParent( ::oRbbUI:hWnd, ::oWndUI:oWndClient:aWnd[1]:hWnd )
                ::oWndUI:oWndClient:aWnd[1]:AddControl( ::oRbbUI )
                ::oWndUI:oWndClient:aWnd[1]:cCaption := "PRINCIPAL"
                ::oRbbUI:SetFocus()
             endif
          endif
       else
          if ::lRbbUI
             if !empty( ::oRbbUI )
                //::oRbbUI:ReSize( 0, ::oWndUI:nWidth - 1, ::oRbbUI:nHeight )
                //For  x = 1 to Len( Self:oRbbUI:aPrompts )
                //   WndWidth( ::oRbbUI:aDialogs[x]:hWnd, ;
                //                   IF(Self:lMax, GetSysMetrics(0), ;
                //                                 Self:oWndUI:nWidth ) - 4 )
                //Next x
                //::oRbbUI:Default()
                //::oRbbUI:Refresh()
             endif
          endif
       endif

       if ::lRbbUI .and. !::lTitUI
          if !Empty( ::oRbbUI )
             oUI   := ::oRbbUI
          else
             if !::lMdiUI
                oUI   := ::oWndUI
             else
                oUI   := ::oWndUI:oWndClient:aWnd[1]
             endif
          endif
       else
           if !::lMdiUI
              oUI   := ::oWndUI    //:oWndClient
           else
              oUI   := ::oWndUI:oWndClient:aWnd[1]
           endif
       endif

       if ::lBttIconUI
          if Empty( ::oBtnIconUI )
           if ::lDemoUI
              ::aBttIconUI[1] := ".\Res\guest32.bmp"
              ::aBttIconUI[2] := ".\Res\guest32.bmp"
              ::aBttIconUI[3] := 24
              ::aBttIconUI[4] := 24
           endif
           if !::lBttExitUI
             bBttIco      := { || Self:End() }  // bBttAct
             cBmpIco4     := ::aBttIconUI[ 2 ]
             if len( ::aBttIconUI ) > 2
                nWdIco       := ::aBttIconUI[ 3 ]
                nHgIco       := ::aBttIconUI[ 4 ]
             else
                nWdIco       := 24
                nHgIco       := 24
             endif
           else
             if empty( ::bValIconUI )
                bBttIco      := { || Self:SetMenuUI() }
             else
                bBttIco      := ::bValIconUI
             endif
             cBmpIco4     := ::aBttIconUI[ 2 ]
             if len( ::aBttIconUI ) > 2
                nWdIco       := ::aBttIconUI[ 3 ]
                nHgIco       := ::aBttIconUI[ 4 ]
             else
                nWdIco       := 24
                nHgIco       := 24
             endif
           endif
           if Empty( ::bFilIconUI )        //::oWndUI:oWndClient:nWidth - 20
              ::oBtnIconUI := TBtnBmp():New( nRowBttIco, nColBttIco, ;
                                       nWdIco, nHgIco,,,;
                                       Self:aBttIconUI[ 1 ],,;
                                       bBttIco, oUI,,, .F., .F.,,,,, !.T.,, .F.,, cBmpIco4 ,;
                                       .T.,, !.F.,, .T., .F. )
                              //::oBtnIconUI:cTooltip   := "Salir"
           else
              ::oBtnIconUI  := Eval( ::bFilIconUI, Self )
           endif
          endif
       endif

       if ::lBttExitUI
          if Empty( ::oBtnExitUI )
           if Empty( ::bFilExitUI )
              ::oBtnExitUI := TBtnBmp():New( nRowBtt, ;
                          nColBtt-nSpaceBtt-8*3, aBttExit[3], aBttExit[4],,, ; //Falta calcular dimensiones bmp elegido
                          aBttExit[1], , ;
                          bBttAct, oUI,,,;
                          .F., .F.,,,, , !.T., , .F., , aBttExit[2], ;
                          .T.,, !.F.,, .T., .F. )
              nSpaceBtt   += nSpaceUI+8*2
              ::oBtnExitUI:cTooltip   := "Salir"
           else
             ::oBtnExitUI  := Eval( ::bFilExitUI, Self )
           endif
           //oUI:AddControl( ::oBtnExitUI )
           //SetParent( ::oBtnExitUI:hWnd, oUI:hWnd )
          else
             ::oBtnExitUI:Move( nRowBtt, nColBtt-nSpaceBtt-8*3 )
             nSpaceBtt += nSpaceUI + 8*2
          endif
       endif

       if ::lBttMaxiUI //.and. !::lMax
          if Empty( ::oBtnMaxiUI )
             if Empty( ::bFilMaxiUI )
                ::oBtnMaxiUI := TBtnBmp():New( nRowBtt, ; //::oWndUI:oWndClient:nWidth - 20
                             nColBtt-nSpaceBtt , 24, 20,,, ;
                          ::aFilMaxiUI[ 1 ], , ;
                          bBttMax, oUI,,,;
                          .F., .F.,,,,, !.T.,, .F.,,::aFilMaxiUI[ 2 ], ;
                          .T.,, !.F.,, .T., .F. )
                nSpaceBtt += nSpaceUI-8
                ::oBtnMaxiUI:cTooltip   := "Maximizar"
             else
                ::oBtnMaxiUI  := Eval( ::bFilMaxiUI, Self )
             endif
           else
             ::oBtnMaxiUI:Move( nRowBtt, nColBtt-nSpaceBtt )
             nSpaceBtt += nSpaceUI
           endif
       endif

       if ::lBttMiniUI
          if Empty( ::oBtnMiniUI )
             if Empty( ::bFilMiniUI )
                ::oBtnMiniUI := TBtnBmp():New( nRowBtt, ; //::oWndUI:oWndClient:nWidth - 20
                             nColBtt-nSpaceBtt , 24, 20,,, ;
                          ::aFilMiniUI[ 1 ], , ;
                          bBttMin, oUI,,,;
                          .F., .F.,,,,, !.T.,, .F.,,::aFilMiniUI[ 2 ], ;
                          .T.,, !.F.,, .T., .F. )
                nSpaceBtt += nSpaceUI-8
                ::oBtnMiniUI:cTooltip   := "Minimizar"
             else
                ::oBtnMiniUI  := Eval( ::bFilMiniUI, Self )
             endif
          else
            ::oBtnMiniUI:Move( nRowBtt, nColBtt-nSpaceBtt )
            nSpaceBtt += nSpaceUI
          endif
       endif

       if ::lBttNormUI
          if Empty( ::oBtnNormUI )
           if Empty( ::bFilNormUI )
             ::oBtnNormUI := TBtnBmp():New( nRowBtt, ; //::oWndUI:oWndClient:nWidth - 20
                             nColBtt-nSpaceBtt , 24, 20,,, ;
                          ::aFilNormUI[ 1 ], , ;
                          bBttNor, oUI,,,;
                          .F., .F.,,,,, !.T.,, .F.,,::aFilNormUI[ 2 ], ;
                          .T.,, !.F.,, .T., .F. )
             nSpaceBtt += nSpaceUI //-8
             ::oBtnNormUI:cTooltip   := "Retaurar"
           else
             ::oBtnNormUI  := Eval( ::bFilNormUI, Self )
           endif
          else
             ::oBtnNormUI:Move( nRowBtt, nColBtt-nSpaceBtt )
             nSpaceBtt += nSpaceUI
          endif
       endif

       if ::lPosInit
          if lBlockUser
             Eval( ::bInitUserUI, Self )
          endif
       endif

       if lBlockPnel
          ::lPnelUI    := .T.
          ::oPnelUI    := Eval( ::bPnelUI, Self )
       else
          if ::lPnelUI
             ::bPnelUI := { | oW | Self:SetPnelUI( , , , , , ,) }
             ::oPnelUI    := Eval( ::bPnelUI, Self )
          endif
       endif
       if !empty( ::oPnelUI ) .and. ::lPnelUI
          AAdd( ::aPnels, ::oPnelUI )
       endif

       if lWBlockPnel
          ::lWPnelUI    := .T.
          ::oWPnelUI    := Eval( ::bWPnelUI, Self )
       else
          if ::lWPnelUI
             if empty( ::bWPnelUI )
                ::bWPnelUI := { | oW | Self:WndPnelUI( , , , , , , ,) }
                ::oWPnelUI := Eval( ::bWPnelUI, Self )
             else
                ::oWPnelUI := Eval( ::bWPnelUI, Self )
             endif
             if !empty( ::oWPnelUI )
                if !empty( ::oWndUI:bMMoved )
                   // ::oWPnelUI:bMMoved  := ::oWndUI:bMMoved     // OJO
                endif
             endif
          endif
       endif
       if !empty( ::oWPnelUI ) .and. ::lWPnelUI
          AAdd( ::aWPnels, ::oWPnelUI )
       endif

       if lBlockTit
          ::lTitUI    := .T.
          if Empty( ::oTitUI )
             ::oTitUI    :=  Eval( ::bTitUI, Self )
          endif
       endif

       if lBlockBar
          ::lBarUI    := .T.
          ::oBarUI    := Eval( ::bBarUI, Self )
       else
          if ::lBarUI
             if empty( ::oBarUI )
                ::SetBarUI( , , ,  )
             endif
          endif
       endif

       if lBlockRbb
          ::lRbbUI    := .T.
          ::oRbbUI    := Eval( ::bRbbUI, Self )
          //SetParent( ::oRbbUI:hWnd, ::oWndUI:hWnd )
       endif

       if lBlockFld
          ::lFldUI    := .T.
          ::oFldUI    := Eval( ::bFldUI, Self )
       endif

       if lBlockFldEx
          ::lFldExUI  := .T.
          ::oFldExUI  := Eval( ::bFldExUI, Self )
       endif

       if lBlockXBrw  .and. lChild
          ::lXbrwUI  := .T.
          ::oXbrwUI  := Eval( ::bXbrwUI, Self )
       endif

       if lBlockXMnu .and. lChild
          ::lXMnuUI  := .T.
          ::oXMnuUI  := Eval( ::bXMnuUI, Self )
       endif

       if !::lPosInit
          if lBlockUser
             ::oInitUserUI := Eval( ::bInitUserUI, Self )
          endif
       endif

       if lControls
          Eval( ::bControls, Self )
       endif

       if ::lSaveBmp
          ::oWndUI:SaveToBmp( Self:cBmpWndUI )                  //".PNG"
          //Esto no sirve de nada si no se actualia la variable ::cBmpWndUI
          //FIMakeThumbNail( ::cBmpWndUI, cFileNoExt( ::cBmpWndUI )+".BMP", 128 )
       endif

       if !empty( ::oParentUI )
          if Upper( ::oParentUI:oWndUI:ClassName() ) == "TWINDOW"
             //::oParentUI:oWndUI:Minimize()
          endif
       endif
Return nil

//----------------------------------------------------------------------------//

METHOD NewUI( nR, nC, nH, nW ) CLASS TWindowsUI
local  oBrush
local  oWndChild
local  nAlto          := 0
if ::lBttIconUI .or. ::lBttExitUI .or. ::lBttMaxiUI .or. ;
   ::lBttMiniUI .or. ::lBttNormUI
   if ::lTitUI
      nAlto      := 26
   endif
endif
DEFAULT nR := nAlto
DEFAULT nC := 0
DEFAULT nH := ::oWndUI:nHeight //- nAlto
DEFAULT nW := ::oWndUI:nWidth
  //? nAlto, 0 , Self:oWndUI:nHeight , Self:oWndUI:nWidth
  //? nR, nC, nH, nW
  DEFINE BRUSH oBrush COLOR METRO_WINDOW   //RGB( 240, 240, 240 )
  DEFINE WINDOW oWndChild ;
                MDICHILD  ;
                FROM nR, nC TO nH , nW ;
                BRUSH oBrush ; //Self:oWndUI:oBrush ;  [ ICON <oIco> ] ;
                OF Self:oWndUI ;  // [ <vscroll: VSCROLL, VERTICAL SCROLL> ] ; // [ <hscroll: HSCROLL, HORIZONTAL SCROLL> ] ;// [ <color: COLOR, COLORS> <nClrFore> [,<nClrBack>] ] ;
                PIXEL ;  // [ STYLE <nStyle> ] ;  // [ <HelpId: HELPID, HELP ID> <nHelpId> ] ;
                BORDER NONE ;
                NOSYSMENU ;
                NOCAPTION
                //NOMINIMIZE //;
                //NOMAXIMIZE

   WndWidth(  oWndChild:hWnd, Self:oWndUI:nWidth )
   WndHeight( oWndChild:hWnd, Self:oWndUI:nHeight - nAlto )

   oWndChild:Show()
   oWndChild:SetFocus()

  //? ::oWndUI:oWndClient:aWnd[1]
  //oWndChild:bMMoved   := { | nRow, nCol, nKeyF | Self:SetPanelUI( nRow, nCol, nKeyF ) }
  //::oWndUI:OnMouseMove := { | nRow, nCol, nKeyF | Self:MouseMoveUI( nRow, nCol, nKeyF ) } // Self:SetPanelUI( nRow, nCol, nKeyF ) }
  //::oWndUI:bLClicked   := { | nRow, nCol, nKeyF | Self:LButtonDownUI( nRow, nCol, nKeyF ) }

Return oWndChild

//----------------------------------------------------------------------------//

METHOD SetStyle( nStyl, nSection) CLASS TWindowsUI
Local nColor   := 0
DEFAULT  nStyl := ::nStyleUI
Do Case

   Case nSection = METRO_NAPPWORKSPACE
        DO CASE
           CASE nStyl  =  0
                nColor := METRO_BTNFACE
           CASE nStyl  =  1
                nColor := METRO_16
           CASE nStyl  =  2
                nColor := METRO_14
           CASE nStyl  =  3
                nColor := METRO_APPWORKSPACE
           OTHERWISE
                nColor := METRO_14
        ENDCASE

EndCase
Return nColor

//----------------------------------------------------------------------------//

METHOD SetBrush( nStyl, nSection, nColor ) CLASS TWindowsUI
Local oBrush
DEFAULT nColor    := METRO_APPWORKSPACE

   //nColor           := ::SetStyle(l2007, l2008, l2010, l2013, nSection )
   DEFINE BRUSH oBrush COLOR nColor

Return oBrush

//----------------------------------------------------------------------------//

METHOD SetTitleUI( cTit , nRow, nCol, nPos, oUI, aColors, oFont ) CLASS TWindowsUI
Local  nColor1
Local  nColor2       //::lBttExitUI
Local  nBmpUI   := IF( ::lBttIconUI, 30, 0 )
Local  nHorz    := GetSysMetrics( 0 )
Local  nVert    := GetSysMetrics( 1 )

DEFAULT nRow    := 7
DEFAULT nCol    := 36
DEFAULT nPos    := 0
DEFAULT oUI     := if( ::lMdiUI, ::oWndUI:oWndClient:aWnd[ 1 ], ::oWndUI )
DEFAULT oFont   := ::oFontTitleUI

if !Empty( cTit ) .and. ValType( cTit ) = "C"

   ::lTitUi        := .T.
   if !::lDemoUI
      ::cTitUI        := cTit
   else
      ::cTitUI        := "Class TWindowsUI"
   endif
   ::oTitUI        := Nil

   if nPos > 0
      Do Case
         Case nPos = 1
              nCol := Int( oUI:nWidth/2 ) - ( nBmpUI + ;
                     Int( GetTextWidth( oUI:hDC, ::cTitUI, oFont:hFont ) ) / 2 )
         Case nPos = 2
              nCol := oUI:nWidth - nBmpUI - ;
                      GetTextWidth( oUI:hDC, ::cTitUI, oFont:hFont )
      EndCase
   endif

   if ValType( aColors ) = "A"
      if Len( aColors ) = 2                  //APPWORKSPACE
         nColor1 := IF(Empty(aColors[1]), METRO_ACTIVEBORDER,aColors[1] )
         nColor2 := IF(Empty(aColors[2]), METRO_WINDOW,aColors[2] )
      else
         nColor1 := IF(Empty(aColors[1]), METRO_ACTIVEBORDER,aColors[1] )
         nColor2 := if(nColor1 <> METRO_WINDOW, METRO_WINDOW, METRO_GRIS2 )
      endif
   else
      nColor1    := METRO_APPWORKSPACE //APPWORKSPACE
      nColor2    := METRO_WINDOW
   endif

   @ nRow, nCol SAY Self:oTitUI PROMPT Self:cTitUI OF oUI ; //::oWndUI ;
        FONT oFont ;
        PIXEL COLOR nColor1, nColor2 ;
        SIZE GetTextWidth( oUI:hDC, Self:cTitUI, oFont:hFont ), ;
             (-( oFont:nInpHeight ) + 16 ) ;
        TRANSPARENT

   oUI:AddControl( ::oTitUI )
   SetParent( ::oTitUI:hWnd, oUI:hWnd )

else
   ::oTitUI  := Nil
endif
return ::oTitUI

//----------------------------------------------------------------------------//

METHOD SetTUserUI( cTit , nRow, nCol, nPos, oUI, aColors, oFont ) CLASS TWindowsUI
Local  nColor1
Local  nColor2       //::lBttExitUI
Local  nBmpUI   := IF( ::lBttIconUI, 30, 0 )
Local  nHorz    := GetSysMetrics( 0 )
Local  nVert    := GetSysMetrics( 1 )

DEFAULT nRow    := 7
DEFAULT nCol    := 36
DEFAULT nPos    := 0
DEFAULT oUI     := ::oWndUI  //:oWndClient
DEFAULT oFont   := ::oFontUserUI

if !Empty( cTit ) .and. ValType( cTit ) = "C"

   ::lTUserUi        := .T.
   ::cTUserUI        := IF( !::lDemoUI, cTit, "Vers. DEMO" )
   ::oTUserUI        := Nil

   /*
   if !::lWPnelUI .and. !::lPnelUI
   else
   endif
   */

   if nPos > 0
      Do Case
         Case nPos = 1
              nCol := Int(( ::oWndUI:nWidth) / 2 ) - nBmpUI - ;
                      Int( GetTextWidth(0,::cTitUI, oFont:hFont )/2 )

              //nCol := Int((::oWndUI:nWidth) / 2) - nBmpUI - ;
              //        Int(GetTextWidth(0,::cTitUI, oFont:hFont )/2)
         Case nPos = 2
              nCol := ::oWndUI:nWidth-nBmpUI - ;
                      GetTextWidth(0,::cTitUI, oFont:hFont )
      EndCase
   endif

   if ValType( aColors ) = "A"
      if Len( aColors ) = 2                  //APPWORKSPACE
         nColor1 := IF( Empty(aColors[1]), METRO_ACTIVEBORDER, aColors[1] )
         nColor2 := IF( Empty(aColors[2]), METRO_WINDOW, aColors[2] )
      else
         nColor1 := IF( Empty(aColors[1]), METRO_ACTIVEBORDER, aColors[1] )
         nColor2 := if( nColor1 <> METRO_WINDOW, METRO_WINDOW, METRO_GRIS2 )
      endif
   else
      nColor1    := METRO_APPWORKSPACE //APPWORKSPACE
      nColor2    := METRO_WINDOW
   endif

   @ nRow, nCol SAY Self:oTUserUI PROMPT Self:cTUserUI ; //oF oUI ; //::oWndUI ;
        FONT oFont ;
        PIXEL COLOR nColor1 ; //, nColor2 ;
        SIZE GetTextWidth(0,Self:cTUserUI, oFont:hFont ), (-(oFont:nInpHeight)+14) ;
        TRANSPARENT

        oUI:AddControl( ::oTUserUI )
        SetParent( ::oTUserUI:hWnd, oUI:hWnd )
else
   if Valtype( cTit ) = "B"
      Self:oTUserUI := Eval( cTit, Self, nRow, nCol, aColors )
   endif
endif

return nil


//----------------------------------------------------------------------------//

METHOD SetBarUI( aPromptsUI, nWidthUI, nHeightUI, oUI ) CLASS TWindowsUI
Local    oBar
Local    oWClient
local    nPosH        := if( ::lWPnelUI, ::oWPnelUI:nWidth+1, 0 )
DEFAULT  oUI          := if( ::lMdiUI, ::oWndUI:oWndClient:aWnd[ 1 ], ::oWndUI )
DEFAULT  nWidthUI     := oUI:nWidth - if( ::lWPnelUI, ::oWPnelUI:nWidth+2, 2 )
DEFAULT  nHeightUI    := 28

if !::lPnelUI
   if ::lPnelUI
      nPosH     := ::oPnelUI:nWidth
      nWidthUI  := oUI:nWidth - ::oPnelUI:nWidth - 2
   endif
endif

if ::lBttIconUI .or. ::lBttExitUI .or. ::lBttMaxiUI .or. ;
   ::lBttMiniUI .or. ::lBttNormUI
   if ::lTitUI
      nPosH     := 400
   else
      nPosH     := 26
   endif
endif
             //   TBarC()
  oBar       := TBar():NewAt( 1, nPosH, nWidthUI, nHeightUI, ;
                         nHeightUI-4, nHeightUI-4, oUI, .F.,,,.F.)

  ::oBarUI   := oBar
  ::lBarUI   := .T.

return oBar

//----------------------------------------------------------------------------//


//----------------------------------------------------------------------------//

METHOD SetQuickstartUI() CLASS TWindowsUI


Return nil

//----------------------------------------------------------------------------//

METHOD VerDlgs( nRow, nCol, nKeyFlags, oWnd ) CLASS TWindowsUI
Local oSay
Local lSi    := .F.
Local nTp    := 0

DEFAULT oWnd := Nil  //::oWndUI

   if nCol <= ::nDlgWidth*2 //.and. nCol > 0
      if nRow >= 120 .and. nRow < GetSysMetrics( 1 ) - 100    //.and. nRow <= 40
         if ::lDlgLeft
            lSi  := .T.
            if !::lDialogoL
               //::SLeftUpUI(  , , , , )
            else
               ::oMainBarL:Show()
            endif
         endif
      /*
      else
         if ::lDlgBottom
            if nRow > GetSysMetrics( 1 ) - 100
               lSi  := .T.
               if !::lDialogoB
                  //::SBottUpUI( oWnd )
               else
                  ::oMainBarB:Show()
               endif
            endif
         endif
         if ::lDlgTop
            if nRow < 20
               lSi  := .T.
               if !::lDialogoT
                  //::STopUpUI( oWnd )
               else
                  ::oMainBarT:Show()
               endif
            endif
         endif
         */
      endif
   endif

   if nCol >= GetSysMetrics(0) - ::nDlgWidth*2
      /*
      if nRow > GetSysMetrics( 1 ) - 100
         if ::lDlgBottom
            lSi  := .T.
            if !::lDialogoB
               //::SBottUpUI( oWnd )
            else
               ::oMainBarB:Show()
            endif
         endif
      else
         if nRow < 20
            if ::lDlgTop
               lSi := .T.
               if !::lDialogoT
                  //::STopUpUI( oWnd )
               else
                  ::oMainBarT:Show()
               endif
            endif
         else
      */
            if ::lDlgRight
               if nRow >= 120 .and. nRow <= GetSysMetrics(1) - 40  // Esto hay que cambiarlo
                  lSi := .T.
                  if !::lDialogoR
                     //::SRightUpUI( oWnd )
                  else
                     ::oMainBarR:Show()
                  endif
               endif
            endif
         //endif
      //endif
   endif
                     //*2                                           //*2
   if nCol > ::nDlgWidth*2 .and. nCol < ( GetSysMetrics(0) - ::nDlgWidth*2 )
      if nRow > GetSysMetrics( 1 ) - 40    //100        //1
         if ::lDlgBottom
            lSi  := .T.
            if !::lDialogoB
               //::SBottUpUI( oWnd )
            else
               ::oMainBarB:Show()
            endif
         endif
      else
         if nRow < 40
            if ::lDlgTop
               lSi := .T.
               if !::lDialogoT
                  //::STopUpUI( oWnd )
               else
                  ::oMainBarT:Show()
               endif
            endif
         endif
      endif
   endif

   if !lSi

      if ::lDlgLeft
         if ::lDialogoL
            ::oMainBarL:Hide()
         endif
      endif

      if ::lDlgBottom
         if ::lDialogoB
            ::oMainBarB:Hide()
         endif
      endif

      if ::lDlgTop
         if ::lDialogoT
            ::oMainBarT:Hide()
         endif
      endif

      if ::lDlgRight
         if ::lDialogoR
            ::oMainBarR:Hide()
         endif
      endif

   endif

Return lSi //0

//----------------------------------------------------------------------------//

METHOD SetXBrwUI( oUI, aPrompt, aSizes, aDatos, aBmps ) CLASS TWindowsUI
Local oXBrw
Local x
Local oBrush1
Local oWClient      := ::oWndUI:oWndClient
Local nWidth        := oWClient:nWidth - 4
Local nHeight       := oWClient:nHeight - 2
local nAlto         := 0
Local aBitmaps    := {}
Local aBmpPal
Local aBmps1      := { ;
                       ".\Res\M_AGREGADIR.BMP",;
                       ".\Res\M_AGREGAUNO.BMP",;
                       ".\Res\M_ARBOL.BMP",;
                       ".\Res\M_COLOR.BMP",;
                       ".\Res\M_EXPLORA.BMP",;
                       ".\Res\Run.bmp" ;
                     }

   For x = 1 to Len( aBmps1 )
       if !empty( aBmps1[x] )
          AAdd( aBitmaps, Array(6) )
          aBmpPal   := PalBmpLoad( aBmps1[x] )
          aBitmaps[ Len(aBitmaps), 1 ]   :=  aBmpPal[ 1 ]
          aBitmaps[ Len(aBitmaps), 2 ]   :=  aBmpPal[ 2 ]
          if x = 1
          aBitmaps[ Len(aBitmaps), 3 ]   :=  16   //20
          aBitmaps[ Len(aBitmaps), 4 ]   :=  16   //20
          else
          aBitmaps[ Len(aBitmaps), 3 ]   :=  16
          aBitmaps[ Len(aBitmaps), 4 ]   :=  16
          endif
          aBitmaps[ Len(aBitmaps), 5 ]   :=  0
          aBitmaps[ Len(aBitmaps), 6 ]   :=  .T.
          aBmpPal   := Nil
       endif
   Next x

if ::lBttIconUI .or. ::lBttExitUI .or. ::lBttMaxiUI .or. ;
   ::lBttMiniUI .or. ::lBttNormUI
   if ::lTitUI
      nAlto      := 26
   endif
endif
nHeight       := oWClient:nHeight - nAlto

/*
      DATA   bXbrwUI
*/

DEFAULT oUI           := oWClient
DEFAULT aPrompt       := {}
DEFAULT aDatos        := {}
DEFAULT aSizes        := {}
DEFAULT aBmps         := {}

   ::lXbrwUI    := .T.
   oXBrw        := TXCBrowse():New( oUI )

   WITH OBJECT oXBrw

   //:oBrush          := oBrush1
   :nStyle            -= WS_BORDER
   //:lTransparent   := .t.

   :cCaption          := ""    //"Prueba de Browse"
   :l2007             := .f.
   //:l2010    := .t.      // No implementado -- 18/02/2013 -- FWh - 31/03/2012

   :nTop              := 1
   :nLeft             := 1 + Int( nWidth/5 )
   :nBottom           := nHeight
   :nRight            := nWidth

   :lDesign           := .f.
   //:oFont             := oFont1

   :lHeader           := .T.
   :lHeadSelec        := .F.   //.t.
   :nHeaderHeight     :=  18
                         // Azul Cobalto  Rgb(  0, 71, 171)
                         // Marron claro titulo ventanas W8 { 209, 168, 129 }
   :bClrHeader        := {|| { Rgb( 255, 255, 255), Rgb(  0, 25, 64 )} }
   :lFooter           := .F.    //.T.
   //oBrw:nFooterHeight     :=  2     //14

   :lLinBorder        := .f.
   :lLinDataSep       := .f.
   :lLinHeadVSep      := .f.
   :lLinHeadHSep      := .f.
   :lLinFootVSep      := .f.
   :lLinFootHSep      := .f.
   :lHeadBtton        := .T.  //.F.
   :lRecordSelector   := .F.
   :nWRecordSel       :=  24

   :nRowDividerStyle  := LINESTYLE_NOLINES        //4
   :nColDividerStyle  := LINESTYLE_NOLINES        //4

   //:nRecSelColor      := Rgb( 202, 162, 126 )

   :nDataLines        :=  1

   :lAllowColSwapping := .f.
   :lVScroll          := .T.
   :lHScroll          := .f.
   :nRowHeight        :=  18
   :nFreeze           :=  1
   :nMarqueeStyle     :=  MARQSTYLE_HIGHLWIN7//MARQSTYLE_HIGHLROW //CELL  //LROWMS // 3     // 5

   //:nSpRecordSel      :=  0

   :bClrSel           := { || { METRO_APPWORKSPACE , Rgb( 210, 210, 204 ) } }
   :bClrStd           := { || { METRO_APPWORKSPACE, METRO_WINDOW } }
   :bClrSelFocus      := { || { METRO_WINDOW, Rgb( 153, 180, 209 ) } }
   :bClrRowFocus      := { || { METRO_APPWORKSPACE , Rgb( 210, 210, 204 ) } }

   //:bKeyChar                := {|x,y,nF,nCol| XBrw1Elige( oBrw:nArrayAt, oBrw, x, y, nF, nCol ) }
   //? ::nAt(), LDblClick( nRow, nCol, nKeyFlags ),

   :lAutoSort         := .F.

   :SetArray( aDatos )

   For x = 1 to Len( aPrompt )
      :aCols[x]:nWidth         := Int( :nWidth/5 )-4
      if x = 1
        :aCols[x]:aBitmaps       := aBitmaps
        :aCols[x]:bBmpData       := {|| 1 } //oXBrw:nArrayAt + 1 }
        :aCols[x]:nHeadBmpNo     := 1
        :aCols[x]:lSetBmpCol     := .T.   // Poner a .t. para poner el icono de la columna 1 en la del Selector
      endif
      :aCols[x]:cHeader        := "Nombre"
      :aCols[x]:nDataStrAlign  := 0
      :aCols[x]:bClrHeader  := { || { METRO_APPWORKSPACE, METRO_WINDOW } }

      if :aCols[x]:lSetBmpCol
         :aCols[x]:nHeadStrAlign  := 2
      else
         :aCols[x]:nHeadStrAlign  := 0
      endif
      :aCols[x]:lAllowSizing   := .f.
      :aCols[x]:nEditType       := 0
   Next x


/*
      :lHScroll               := .F.

      //:s:SetCheck( nil, .T. )
      :bClrSel          := { || { CLR_WHITE, Rgb(128,128,128) } }
      :bClrSelFocus     := { || { CLR_WHITE, Rgb(051,051,051) } }
//      :bClrRowFocus     := { || { CLR_BLACK, Rgb(128,128,128) } }
*/
      :CreateFromCode()
      aBitmaps := Nil
   END

   //oDlg:SetControl( oXBrw )
   //oDlg:AddControl( oXBrw )
   //oFont1   := Nil
   oBrush1  := Nil

   ::oXbrwUI   := oXBrw

Return nil

//----------------------------------------------------------------------------//

METHOD SetSpltUI( oUI ) CLASS TWindowsUI
Local oSplit

/*
      @ oPnel:nTop, oPnel:nWidth+118 SPLITTER oSplit ;
              VERTICAL ;
              PREVIOUS CONTROLS oPnel ;
              HINDS CONTROLS oWinUI:oWPnelUI ; //LEFT MARGIN 10 ; // RIGHT MARGIN 10 ;
              SIZE 0.5, oPnel:nHeight-1 PIXEL ;         // - 74
              OF oWinUI:oWndUI ;
              COLOR METRO_GRIS2 //BTNFACE //WINDOW //METRO_AZUL3    //3
              //_3DLOOK ;
              //UPDATE
*/

Return nil

//----------------------------------------------------------------------------//

METHOD MnuXBrwUI( oUI, aPrompt, aSizes, aDatos, aBmps ) CLASS TWindowsUI
Local oXBrw
Local x
Local oBrush1
Local oWClient      := ::oWndUI:oWndClient
Local nWidth        := oWClient:nWidth - 4
Local nHeight       := oWClient:nHeight - 2
local nAlto         := 0
Local aData       := {{" Abandonar Aplicacion"},;
                      {" Fichero Articulos"},;      // {"Seleccionar Usuario 8066"},;
                      {" Proveedores"},;
                      {" Facturas de Compra"},;
                      {" Clientes"},;
                      {" Presupuestos"},;
                      {" Albaranes de Venta"},;
                      {" Facturas de Venta"},;
                      {" Plan Contable"},;
                      {" Introducir Apuntes"},;
                      {" Balances Oficiales" }}
Local aTipos      := { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 }
/*
Local aBitmaps    := {}
Local aBmpPal
Local aBmps1      := { ;
                       ".\Res\Bmps\M_AGREGADIR.BMP",;
                       ".\Res\Bmps\M_AGREGAUNO.BMP",;
                       ".\Res\Bmps\M_ARBOL.BMP",;
                       ".\Res\Bmps\M_COLOR.BMP",;
                       ".\Res\Bmps\M_EXPLORA.BMP",;
                       ".\Res\Bmps\Run.bmp" ;
                     }

   For x = 1 to Len( aBmps1 )
       if !empty( aBmps1[x] )
          AAdd( aBitmaps, Array(6) )
          aBmpPal   := PalBmpLoad( aBmps1[x] )
          aBitmaps[ Len(aBitmaps), 1 ]   :=  aBmpPal[ 1 ]
          aBitmaps[ Len(aBitmaps), 2 ]   :=  aBmpPal[ 2 ]
          if x = 1
          aBitmaps[ Len(aBitmaps), 3 ]   :=  16   //20
          aBitmaps[ Len(aBitmaps), 4 ]   :=  16   //20
          else
          aBitmaps[ Len(aBitmaps), 3 ]   :=  16
          aBitmaps[ Len(aBitmaps), 4 ]   :=  16
          endif
          aBitmaps[ Len(aBitmaps), 5 ]   :=  0
          aBitmaps[ Len(aBitmaps), 6 ]   :=  .T.
          aBmpPal   := Nil
       endif
   Next x
*/



if ::lBttIconUI .or. ::lBttExitUI .or. ::lBttMaxiUI .or. ;
   ::lBttMiniUI .or. ::lBttNormUI
   if ::lTitUI
      nAlto      := 26
   endif
endif
nHeight       := oWClient:nHeight - nAlto

/*
      DATA   bXbrwUI
*/

DEFAULT oUI           := oWClient
DEFAULT aPrompt       := {}
DEFAULT aDatos        := {}
DEFAULT aSizes        := {}
DEFAULT aBmps         := {}

   ::lXbrwUI    := .T.
   oXBrw        := TXCBrowse():New( oUI )

   WITH OBJECT oXBrw

   //:oBrush          := oBrush1
   :nStyle            -= WS_BORDER
   //:lTransparent   := .t.

   :cCaption          := ""    //"Prueba de Browse"
   :l2007             := .f.
   //:l2010    := .t.      // No implementado -- 18/02/2013 -- FWh - 31/03/2012

   :nTop              := 1
   :nLeft             := 1 + Int( nWidth/5 )
   :nBottom           := nHeight
   :nRight            := nWidth

   :lDesign           := .f.
   //:oFont             := oFont1

   :lHeader           := .T.
   :lHeadSelec        := .F.   //.t.
   :nHeaderHeight     :=  18
                         // Azul Cobalto  Rgb(  0, 71, 171)
                         // Marron claro titulo ventanas W8 { 209, 168, 129 }
   :bClrHeader        := {|| { Rgb( 255, 255, 255), Rgb(  0, 25, 64 )} }
   :lFooter           := .F.    //.T.
   //oBrw:nFooterHeight     :=  2     //14

   :lLinBorder        := .f.
   :lLinDataSep       := .f.
   :lLinHeadVSep      := .f.
   :lLinHeadHSep      := .f.
   :lLinFootVSep      := .f.
   :lLinFootHSep      := .f.
   :lHeadBtton        := .T.  //.F.
   :lRecordSelector   := .F.
   :nWRecordSel       :=  24

   :nRowDividerStyle  := LINESTYLE_NOLINES        //4
   :nColDividerStyle  := LINESTYLE_NOLINES        //4

   //:nRecSelColor      := Rgb( 202, 162, 126 )

   :nDataLines        :=  1

   :lAllowColSwapping := .f.
   :lVScroll          := .T.
   :lHScroll          := .f.
   :nRowHeight        :=  18
   :nFreeze           :=  1
   :nMarqueeStyle     :=  MARQSTYLE_HIGHLWIN7//MARQSTYLE_HIGHLROW //CELL  //LROWMS // 3     // 5

   //:nSpRecordSel      :=  0

   :bClrSel           := { || { METRO_APPWORKSPACE , Rgb( 210, 210, 204 ) } }
   :bClrStd           := { || { METRO_APPWORKSPACE, METRO_WINDOW } }
   :bClrSelFocus      := { || { METRO_WINDOW, Rgb( 153, 180, 209 ) } }
   :bClrRowFocus      := { || { METRO_APPWORKSPACE , Rgb( 210, 210, 204 ) } }

   //:bKeyChar                := {|x,y,nF,nCol| XBrw1Elige( oBrw:nArrayAt, oBrw, x, y, nF, nCol ) }
   //? ::nAt(), LDblClick( nRow, nCol, nKeyFlags ),

   :lAutoSort         := .F.

   :SetArray( aData )
      x := 1
      :aCols[x]:nWidth         := Int( :nWidth/5 )-4
      :aCols[x]:lAllowSizing   := .f.
      :aCols[x]:nEditType       := 0

      :CreateFromCode()
   END

   //oDlg:SetControl( oXBrw )
   //oDlg:AddControl( oXBrw )
   //oFont1   := Nil
   oBrush1  := Nil

   ::oXbrwUI   := oXBrw

Return nil

//----------------------------------------------------------------------------//

METHOD SetPnelUI( oUI, nRow1, nCol1, nCol2, nHt, oBrh, nColor ) CLASS TWindowsUI
Local oPanel        := Nil
Local oWClient      := if( ::lMdiUI, ::oWndUI:oWndClient, ::oWndUI )
Local nWidth        := Int( oWClient:nWidth / 4 ) - 4
Local nHeight       := oWClient:nHeight - 2
local nAlto         := 0
Local oBrush

DEFAULT nColor      := METRO_ACTIVEBORDER

DEFINE BRUSH oBrush COLOR nColor

if ::lBttIconUI .or. ::lBttExitUI .or. ::lBttMaxiUI .or. ;
   ::lBttMiniUI .or. ::lBttNormUI
   if ::lTitUI
      nAlto         := 26
   endif
endif

DEFAULT nRow1         := nAlto
DEFAULT nCol1         := 1
DEFAULT nCol2         := nWidth
DEFAULT nHt           := nHeight  //- nRow1
DEFAULT oUI           := oWClient
DEFAULT oBrh          := oBrush      //::oWndUI:oBrush

  oPanel := TPanel():New( nRow1, nCol1, nHt , nCol2 , oUI ) //oWndClient
  ::oPnelUI  := oPanel

  oPanel:SetBrush( oBrh )

  SetParent( oPanel:hWnd, oUI:hWnd )

  if !::lPnelUI
      ::lPnelUI  := .T.
  endif

  oBrush:End()
  oBrush  := Nil
  oBrh    := Nil

Return oPanel

//----------------------------------------------------------------------------//

METHOD WndPnelUI( oUI, nRow1, nCol1, nCol2, nHt, oBrh, nColor, cBmp ) CLASS TWindowsUI
Local oPanel        := Nil
Local oWClient      := if( ::lMdiUI, ::oWndUI:oWndClient:aWnd[1], ::oWndUI )
Local nWidth        := Int( oWClient:nWidth / 5 ) - 4 // Int( ::oWndUI:nWidth/5 ) - 4
Local nHeight       := oWClient:nHeight - 2  // ::oWndUI:nHeight-2
local nAlto         := 0
Local oBrush
Local bBttPnel
Local oBttPnel

DEFAULT nRow1         := nAlto //+ 2
DEFAULT nCol1         := 0
DEFAULT nCol2         := nWidth //* 2
DEFAULT nHt           := nHeight //* 2 ) //- nRow1
DEFAULT oUI           := oWClient
DEFAULT nColor        := METRO_APPWORKSPACE   //ACTIVEBORDER

DEFINE BRUSH oBrush COLOR nColor

DEFAULT oBrh          := oBrush //::oWndUI:oBrush
DEFAULT cBmp          := ::cBmpPnel

  if !empty( nRow1 )
     ::nRowWPnelUI    := nRow1
  else
     if !empty( ::nRowWPnelUI )
        nRow1 := ::nRowWPnelUI
     else
        ::nRowWPnelUI    := nRow1
     endif
  endif
  if !empty( nCol1 )
     ::nColWPnelUI    := nCol1
  else
     if !empty( ::nColWPnelUI )
        nCol1 := ::nColWPnelUI
     else
        ::nColWPnelUI    := nCol1
     endif
  endif
  if !Empty( cBmp )             //::lBttIconUI
     if !::lBttExitUI
        bBttPnel     := { || Self:End() }
     else
        bBttPnel     := { || .T. } // { || Self:End() }
     endif
  else
     bBttPnel     := { || .T. } //Self:SetMenuUI() }
  endif

  oPanel := TPanel():New( nRow1, nCol1, nHt , nCol2 , oUI )
  //oPanel:nClrPane := nRgb( 255, 255, 255 )
  ::oWPnelUI   := oPanel

  oPanel:SetBrush( oBrh )
  SetParent( oPanel:hWnd, oUI:hWnd )

  if !Empty( cBmp )
     oBttPnel := TBtnBmp():New( 0, 6, 32, 32,,, ; //nColBtt
                          cBmp,, bBttPnel, oPanel,,,;
                          .F., .F.,,,,, !.T.,, .F.,, , ;
                          .T.,, !.F.,, .T., .F. )
     if !::lBttExitUI
        oBttPnel:cTooltip   := "Salir"
     endif
  endif

  if !::lWPnelUI
      ::lWPnelUI  := .T.
  endif

  oBrush:End()
  oBrush  := Nil
  oBrh    := Nil

Return oPanel

//----------------------------------------------------------------------------//

METHOD SetMenuUI() CLASS TWindowsUI
Local oMn
Local x

      MENU POPUP oMn 2010

           if !::lBttExitUI
            MENUITEM " Salir " ; //FILE ".\16x16\build1r1.bmp" ;
                    ACTION ::oWndUI:End()
           endif
           if !::lBttMaxiUI .and. !::lMax
            SEPARATOR
            MENUITEM " Maximizar " ; //FILE ".\16x16\organicer1.bmp" ;
                    ACTION ::oWndUI:Maximize()
           endif
           if !::lBttMiniUI
            if (!::lBttMaxiUI .and. !::lMax) .or. !::lBttExitUI
               SEPARATOR
            endif
            MENUITEM " Minimizar " ; //FILE ".\16x16\organicer1.bmp" ;
                    ACTION ::oWndUI:Minimize()

           endif
           if !::lBttNormUI .and. !::lMax  //!::lBttMaxiUI
            SEPARATOR
            MENUITEM " Restaurar " ; //FILE ".\16x16\organicer1.bmp" ;
                    ACTION ( Self:lRepaintUI := .F., ::oWndUI:Restore() )
           endif

           if ::lMdiUI

            if !Empty( ::oWndUI:oWndClient:aWnd )
              For x = 1 to Len( ::oWndUI:oWndClient:aWnd )
                  if !::lBttNormUI .and. !::lMax
                     SEPARATOR
                  endif
                  MENUITEM ::oWndUI:oWndClient:aWnd[x]:cCaption //;
                    //ACTION Self:oWndUI:oWndClient:aWnd[x]:SetFocus()

                  if x < Len( ::oWndUI:oWndClient:aWnd )
                   SEPARATOR
                  endif
              Next x
            endif

           endif

      ENDMENU
      ACTIVATE POPUP oMn WINDOW ::oWndUI AT ::oBtnIconUI:nHeight+2,;
                         ::oBtnIconUI:nWidth + ::oBtnIconUI:nLeft + 2  //26, 0

Return oMn

//----------------------------------------------------------------------------//

//METHOD SetxMnuUI() CLASS TWindowsUI

//Return oBr

//----------------------------------------------------------------------------//

METHOD SetWndMnu( oMnu ) CLASS TWindowsUI


Return Nil

//----------------------------------------------------------------------------//
/*
METHOD SetPanelUI( nRow, nCol, nKeyF ) CLASS TWindowsUI
Local oUI     := ::oWndUI:oWndClient:aWnd[1]

//? ::oWndUI:nWidth, ::oWndUI:oWndClient:nWidth, ::oWndUI:nHeight, oUI:nWidth, GetSysMetrics( 0 ), GetSysMetrics( 1 )
if nCol > ::oWndUI:nWidth-5 .and. nRow < 5
   //@ 7, 260 SAY STRZero( nCol, 5 ) OF oUI PIXEL COLOR CLR_BLACK, CLR_WHITE
else

endif

Return 0
*/


//----------------------------------------------------------------------------//

METHOD LButtonDownUI( nRow, nCol, nFlags, o ) CLASS TWindowsUI
Local oCrs
/*
   if !::lDialogoT
      if nRow <= 10
         if !::lDrag
            ::lDrag     := .T.
            CursorHand()
            //CursorDrag()
            //DEFINE CURSOR oCrs DRAG
            //::oWndUI:oDragCursor  := oCrs

            //::oWndUI:nDragRow  = nRow
            //::oWndUI:nOldCol   = nCol
         else

         endif
      else

      endif
   else
      ::lDrag  := .F.
   endif
*/
return nil

//----------------------------------------------------------------------------//

METHOD LButtonUpUI( nRow, nCol, nFlags ) CLASS TWindowsUI

   ::lDrag     = .F.
   //::oWndUI:oDragCursor  := Nil

   //CursorNS()

   //::oWndUI:nDragRow  = nil

return nil

//----------------------------------------------------------------------------//


//----------------------------------------------------------------------------//

METHOD MouseMoveUI( o, nRow, nCol, nKeyF ) CLASS TWindowsUI

  if ::lDrag

     //::oWndUI:SetSize( GetSysMetrics( 0 ) - nRow*10, GetSysMetrics( 1 ) - nRow*10 )
     //::oWndUI:Move( nRow, nRow )

  else
     //if ::oWndUI:bLClicked != nil
       // return Eval( Self:oWndUI:bLClicked, nRow, nCol, nKeyFlags, Self )
     //endif
     //::oWndUI:MouseMove( nRow, nCol, nKeyF )
  endif

Return nil

//----------------------------------------------------------------------------//

METHOD SetImgUI( aBmps, nH, nW, oButton, nPos )  CLASS TWindowsUI
Local x
Local aBitmaps  := {}
Local aBmpPal
Local cExt      := ""
Local uFile     := ""

DEFAULT aBmps   := {}
DEFAULT nH      := 32
DEFAULT nW      := 32
DEFAULT oButton := Nil
DEFAULT nPos    := 0

if !empty( aBmps )
   Do Case
      Case Valtype( aBmps ) = "C"

           uFile         := aBmps
           cExt          :=  Right( Upper( uFile ), 3 )
           Do Case
              Case Upper( cExt ) = "BMP"

                    AAdd( aBitmaps, Array(6) )
                    aBmpPal   := PalBmpRead( , aBmps )
                    aBitmaps[ x, 1 ]   :=  aBmpPal[ 1 ]
                    aBitmaps[ x, 2 ]   :=  aBmpPal[ 2 ]
                    aBitmaps[ x, 3 ]   :=  nH   //64
                    aBitmaps[ x, 4 ]   :=  nW   //108
                    aBitmaps[ x, 5 ]   :=  0
                    aBitmaps[ x, 6 ]   :=  .F.
                    aBmpPal   := Nil

              Case Upper( cExt ) = "PNG"

                    AAdd( aBitmaps, Array(6) )
                    aBmpPal := FWOpenPngFile( aBmps )
                    aBitmaps[ x, 1 ]   :=  aBmpPal
                    aBitmaps[ x, 2 ]   :=  0
                    aBitmaps[ x, 3 ]   :=  nH   //64
                    aBitmaps[ x, 4 ]   :=  nW   //108
                    aBitmaps[ x, 5 ]   :=  0
                    aBitmaps[ x, 6 ]   :=  .F.
                    aBmpPal   := Nil


           EndCase

      Case Valtype( aBmps ) = "A"

        For x = 1 to Len( aBmps )
            uFile        := aBmps[ x ]
            cExt         :=  Right( Upper( uFile ), 3 )
            Do Case
               Case Upper( cExt ) = "BMP"

                    AAdd( aBitmaps, Array(6) )
                    aBmpPal   := PalBmpRead( , aBmps[x] )
                    aBitmaps[ x, 1 ]   :=  aBmpPal[ 1 ]
                    aBitmaps[ x, 2 ]   :=  aBmpPal[ 2 ]
                    aBitmaps[ x, 3 ]   :=  nH   //64
                    aBitmaps[ x, 4 ]   :=  nW   //108
                    aBitmaps[ x, 5 ]   :=  0
                    aBitmaps[ x, 6 ]   :=  .F.
                    aBmpPal   := Nil

               Case Upper( cExt ) = "PNG"

                    AAdd( aBitmaps, Array(6) )
                    aBmpPal := FWOpenPngFile( aBmps[ x ] )
                    //if Valtype( aBmpPal ) = "A"
                    aBitmaps[ x, 1 ]   :=  aBmpPal
                    aBitmaps[ x, 2 ]   :=  0
                    aBitmaps[ x, 3 ]   :=  nH   //64
                    aBitmaps[ x, 4 ]   :=  nW   //108
                    aBitmaps[ x, 5 ]   :=  0
                    aBitmaps[ x, 6 ]   :=  .F.
                    //endif
                    aBmpPal   := Nil
                Otherwise
                    if empty( aBmps[x] )// .and. aBmps[x] <> Nil
                       AAdd( aBitmaps, Array(6) )
                    endif

            EndCase
            uFile        := ""
            cExt         := ""
        Next x

   EndCase
endif
/*
              oButton:HasAlpha( oButton:hBitmap1, BTN_UP )
              oButton:nAlphaLevel( nLevelAlpha )
              oButton:Refresh()
*/
return aBitmaps

//----------------------------------------------------------------------------//

METHOD SRightUpUI( oWnd, bControls, nWd, nColor, oFont1 ) CLASS TWindowsUI
Local x
Local oXBrw
Local oBrus
Local oBtt1
Local aP
Local aBtns   := Array( 10 )
Local oDlgR
DEFAULT bControls  := { || .T. }
DEFAULT nWd        := Int( ::nDlgWidth*1.65 )
DEFAULT nColor     := METRO_GRIS1
DEFAULT oWnd       := Nil           //::oWndUI
//DEFAULT oWnd       := GetWndDefault()

if !::lDialogoR
  if empty( ::oMainBarR )
     if Empty( oFont1 )
        DEFINE FONT oFont1 NAME "Segoe UI" SIZE 0, -10
     endif
     DEFINE BRUSH oBrus COLOR nColor
     ::lDialogoR    := .T.
     ::nDlgWidth    := nWd

     if empty( oWnd )
        DEFINE DIALOG oDlgR FROM 0, GetSysMetrics(0)-nWd ;
            TO GetSysMetrics(1), GetSysMetrics(0) ;
            PIXEL STYLE nOr( WS_POPUP, WS_VISIBLE )
     else
        DEFINE DIALOG oDlgR FROM 0, GetSysMetrics(0)-nWd ;
            TO GetSysMetrics(1), GetSysMetrics(0) OF oWnd ;
            PIXEL STYLE nOr( WS_POPUP, WS_VISIBLE )
     endif
     oDlgR:SetBrush( oBrus )
     oDlgR:SetFont( oFont1 )

     ACTIVATE DIALOG oDlgR NOWAIT ;
              ON INIT ( oDlgR:Hide(), Eval( bControls, Self ) )
     if !empty( oBrus )
        oBrus:End()
        oBrus   := Nil
     endif
     oFont1:End()
     oFont1   := Nil
     ::oMainBarR := oDlgR
  endif
endif
Return Self:oMainBarR

//----------------------------------------------------------------------------//

METHOD SLeftUpUI( oWnd, bControls, nWd, nColor, oFont1 ) CLASS TWindowsUI
Local x
Local oXBrw
Local oBrus
Local oBtt1
Local aP
Local aBtns   := Array( 10 )
Local oDlgL
DEFAULT bControls  := { || .T. }
DEFAULT nWd        := 128*1.6  //Int( ::nDlgWidth*3 ) + 8
DEFAULT nColor     := METRO_GRIS1
DEFAULT oWnd       := Nil   //::oWndUI

if !::lDialogoL
  if empty( ::oMainBarL )
     if Empty( oFont1 )
        DEFINE FONT oFont1 NAME "Segoe UI" SIZE 0, -10
     endif
     DEFINE BRUSH oBrus COLOR nColor
     ::lDialogoL    := .T.

     if empty( oWnd )
     DEFINE DIALOG oDlgL FROM 0, 0 TO GetSysMetrics(1), nWd ;
          PIXEL STYLE nOr( WS_POPUP ) //, WS_VISIBLE )
     else
     DEFINE DIALOG oDlgL FROM 0, 0 TO GetSysMetrics(1), nWd ;
          OF oWnd PIXEL STYLE nOr( WS_POPUP ) //, WS_VISIBLE )
     endif
     oDlgL:SetBrush( oBrus )
     oDlgL:SetFont( oFont1 )

     if ::lSaveBmp
        oXBrw := ::LoadMdiUI( , nWd )
        oDlgL:bPainted := { |oD| oXBrw:SetArray( Self:GetListBmps() ),;   //208
        oXBrw:aCols[1]:aBitmaps := LoadImgUI( Self:GetListBmps(), ;
                   oXBrw:aCols[1]:nWidth, oXBrw:nRowHeight-2,,) }

        ACTIVATE DIALOG oDlgL NOWAIT ;
              ON INIT ( SetParent( oXBrw:hWnd, oDlgL:hWnd ), ;
                        oDlgL:Hide(), Eval( bControls, Self ) )
     else
        ACTIVATE DIALOG oDlgL NOWAIT ;
              ON INIT ( oDlgL:Hide(), Eval( bControls, Self ) )
     endif

     ::oMainBarL := oDlgL

     if !empty( oBrus )
        oBrus:End()
        oBrus   := Nil
     endif
     oFont1:End()
     oFont1   := Nil
  endif
else
  if ::lSaveBmp
     if empty( ::oMainBarL:bPainted )
       oXBrw := ::LoadMdiUI( , nWd )
       ::oMainBarL:bPainted := { |oD| oXBrw:SetArray( Self:GetListBmps() ),;
       oXBrw:aCols[1]:aBitmaps := LoadImgUI( Self:GetListBmps(), ;
                   oXBrw:aCols[1]:nWidth, oXBrw:nRowHeight-2,,) }
     endif
  endif
endif
Return Self:oMainBarL

//----------------------------------------------------------------------------//

METHOD SBottUpUI( oWnd, bControls, nWd, nColor, oFont1 ) CLASS TWindowsUI
Local x
Local oXBrw
Local oBrus
Local oBtt1
Local aP
Local aBtns   := Array( 10 )
Local oDlgB
DEFAULT bControls  := { || .T. }
DEFAULT nWd        := ::nDlgWidth
DEFAULT nColor     := METRO_WINDOW //METRO_GRAYTEXT
DEFAULT oWnd       := Nil  //::oWndUI

if !::lDialogoB
  if empty( ::oMainBarB )
     if Empty( oFont1 )
        DEFINE FONT oFont1 NAME "Segoe UI" SIZE 0, -10
     endif
     DEFINE BRUSH oBrus COLOR nColor
     ::lDialogoB    := .T.

     if empty( oWnd )
        DEFINE DIALOG oDlgB FROM GetSysMetrics(1)-nWd, 0 ;
             TO GetSysMetrics(1), GetSysMetrics(0) ;
             PIXEL STYLE nOr( WS_POPUP, WS_VISIBLE )
     else
        DEFINE DIALOG oDlgB FROM GetSysMetrics(1)-nWd, 0 ;
             TO GetSysMetrics(1), GetSysMetrics(0) ;
             OF oWnd PIXEL STYLE nOr( WS_POPUP, WS_VISIBLE )
     endif

     oDlgB:SetBrush( oBrus )
     oDlgB:SetFont( oFont1 )

     ACTIVATE DIALOG oDlgB NOWAIT ;
              ON INIT ( oDlgB:Hide(), Eval( bControls, Self ) )
     if !empty( oBrus )
        oBrus:End()
        oBrus   := Nil
     endif
     oFont1:End()
     oFont1   := Nil
     ::oMainBarB := oDlgB
  endif
endif

Return nil

//----------------------------------------------------------------------------//

METHOD STopUpUI( oWnd, bControls, nWd, nColor, oFont1 ) CLASS TWindowsUI
Local x
Local oXBrw
Local oBrus
Local oBtt1
Local aP
Local aBtns   := Array( 10 )
Local oDlgT
DEFAULT bControls  := { || .T. }
DEFAULT nWd        := ::nDlgWidth
DEFAULT nColor     := METRO_GRAYTEXT
DEFAULT oWnd       := Nil   //::oWndUI

if !::lDialogoT
  if empty( ::oMainBarT )
     if Empty( oFont1 )
        DEFINE FONT oFont1 NAME "Segoe UI" SIZE 0, -14
     endif
     DEFINE BRUSH oBrus COLOR nColor
     ::lDialogoT    := .T.
     if empty( oWnd )
        DEFINE DIALOG oDlgT FROM 0,0 TO nWd, GetSysMetrics(0) ;
               PIXEL STYLE nOr( WS_POPUP, WS_VISIBLE )
     else
        DEFINE DIALOG oDlgT FROM 0, 0 TO nWd, GetSysMetrics(0) ;
               OF oWnd PIXEL STYLE nOr( WS_POPUP, WS_VISIBLE )
     endif

     oDlgT:SetBrush( oBrus )
     oDlgT:SetFont( oFont1 )

     ACTIVATE DIALOG oDlgT NOWAIT ;
              ON INIT ( oDlgT:Hide(), Eval( bControls, Self ) )
     if !empty( oBrus )
        oBrus:End()
        oBrus   := Nil
     endif
     oFont1:End()
     oFont1   := Nil
     ::oMainBarT := oDlgT
  endif
endif

Return Self:oMainBarT

//----------------------------------------------------------------------------//

METHOD LoadMdiUI( lTxt, nWd ) CLASS TWindowsUI
Local oXbrw
Local x
Local oBrush1
Local oFont1
Local oDlg
Local aP          := {}
Local aBitmaps1   := {}
Local aBmps1      := {}
//Local oWClient    := oWinUI:oWndUI

   DEFAULT lTxt   := .T.

   aP             := Array( Len( aBitmaps1 ), 2 )

   DEFINE FONT oFont1 NAME "Segoe UI" BOLD SIZE 0, -20

          //TXCBrowse
   oXBrw := TXBrowse():New( ::oMainBarL )

   WITH OBJECT oXBrw

   :nStyle            -= WS_BORDER
   :lTransparent      := .F.
   :cCaption          := ""
   :l2007             := .F.
   :nTop              := 0
   :nLeft             := 4 //3
   :nBottom           := 383 * ::CalculaRes( 1 )
   :nRight            := 100 //118  //75
   :lDesign           := .F.

   :SetFont( oFont1 )
   :SetColor( METRO_WINDOWTEXT, METRO_GRIS1 ) //WINDOW )

   :lHeader           := .F.
   :nHeaderHeight     :=  0

   :lFooter           := .F.    //.T.
   :nFooterHeight     :=  0     //14

   // DATAs del XCBrowse
   if Upper( oXbrw:ClassName() ) == "TXCBROWSE"
      oXbrw:lHeadSelec          := .F.     //
      oXbrw:lLinBorder          := .F.     // Eliminar lineas
      oXbrw:lLinDataSep         := .F.     // Eliminar lineas
      oXbrw:lLinHeadVSep        := .F.     // Eliminar lineas Vertical Titulos
      oXbrw:lLinHeadHSep        := .F.     // Eliminar lineas Horizontal Titulos
      oXbrw:lLinFootVSep        := .F.     // Eliminar lineas Vertical Pie
      oXbrw:lLinFootHSep        := .F.     // Eliminar lineas Horizontal Pie
      oXbrw:lHeadBtton          := .F.     //
      oXbrw:lRowDividerComplete := .F.     // Eliminar lineas o poner todas
      oXbrw:lNoEmpty            := .T.     // Saltar columnas sin BITMAP a la siguiente
      //   :nSpRecordSel      :=  1
   endif
   :lRecordSelector   := .F.
   //:nRecSelWidth      := 12
   //:nWRecordSel       := 12

   :nRowDividerStyle  := LINESTYLE_NOLINES        //4
   :nColDividerStyle  := LINESTYLE_NOLINES        //4

   :nRecSelColor      := METRO_APPWORKSPACE   //RGB( 20,20,20 )

   :lAllowColSwapping := .F.
   :lVScroll          := .F. //if( Len( aP ) > 6, .T., .F. )
   :lHScroll          := .F.
   :nRowHeight        := Int((:nRight - :nLeft) * 2 * 0.5625) //+ 24//0.5625 * 1.778 ) //104 //124 //98
   //:nDataLines        := 2
   :nMarqueeStyle     := 3

   :bClrSel           := { || { METRO_GRIS1, METRO_GRIS1 } }//APPWORKSPACE } }  //GRIS1
   :bClrStd           := { || { METRO_GRIS1, METRO_APPWORKSPACE } }  //METRO_WINDOWFRAME
   //:bClrStds          := { || { , METRO_APPWORKSPACE } }
   :bClrSelFocus      := { || { METRO_GRIS1, METRO_GRIS4 } }//GRIS2 }}//METRO_AZUL8 } }

   :bKeyChar          := {|x,y,nF,oCol| Self:XBrwElige( oXBrw:nArrayAt, oXBrw,;
                                x, y, oXBrw:nRowSel, oXBrw:nColSel, nF, oCol ) }

   //:bLDblClick        := {|x,y,nF,oCol| XBrwElige( oXBrw:nArrayAt, oXBrw, ;
                              //x, y, oXBrw:nRowSel, oXBrw:nColSel, nF, oCol ) }
   //:lAutoSort         := .F.
   //:nStretchCol       := 1

   :bRClicked           := { |x,y,nF| Self:SetMnuLeft( oXBrw, x, y ) }

   :SetArray( Self:GetListBmps() )

   WITH OBJECT :aCols[ 1 ]
   //:cDataType      := 'F'
   :nWidth         := if( oXBrw:lVScroll, ;
                          2*Int((oXBrw:nRight - oXBrw:nLeft)) - 10, ;
                          2*Int((oXBrw:nRight - oXBrw:nLeft)) )
   :aBitmaps       := LoadImgUI( Self:GetListBmps(), :nWidth, oXBrw:nRowHeight-2,,)
   :bStrData       := { || Nil }
   :bBmpData       := { | oB | oXbrw:nArrayAt }
   :nDataStrAlign  := 2  //1
                         //AL_TOP   4 //AL_BOTTOM 8
   :nDataBmpAlign  := 2  //AL_LEFT  0 //AL_RIGHT  1 //AL_CENTER  2
   :bClrStd        := { || { , METRO_GRIS1 } } //AZUL3 } }  //METRO_WINDOWFRAME
   :lAllowSizing   := .F.
   :nEditType      := 0
   :bLDClickData   := {|x,y,nF,oCol| Self:XBrwElige( oXBrw:nArrayAt, ;
                                     oXBrw, x, y, oXBrw:nRowSel, ;
                                     oXBrw:nColSel, nF, oCol ) }
   //:lBmpStretch    := .T.
   END
   /*
   if lTxt
      oXBrw:AddCol()
      WITH OBJECT :aCols[ 2 ]
         :bClrStd        := oXBrw:aCols[ 1 ]:bClrStd
         :lAllowSizing   := .F.
         :nEditType      := 0
         :bStrData       := { || if( !empty( Self:GetListBmps() ),;
                                 Self:GetListBmps()[ oXBrw:nArrayAt ][ 2 ], "" ) }
         :oDataFont     := oFont1
         :nWidth        := oXBrw:aCols[ 1 ]:nWidth
         :nDataStrAlign := 0  //1
         :bPaintText    := { | oCol, hDC, cData, aRect, aColors, lHighLite | ;
                    DrawHText( oCol, hDC, cData, aRect, aColors, lHighLite ) }

      END
      :aCols[ 1 ]:SetColsAsRows( 1, 2 )
   endif
   */
/*
   for nCol := 1 to 5 step 2
      WITH OBJECT oBrw:aCols[ nCol + 1 ]
         :nWidth        := 120
         :cDataType     := 'F'
         :nDataBmpAlign := AL_CENTER
      END
      WITH OBJECT oBrw:aCols[ nCol ]
         :oDataFont     := oVert
         :nWidth        := oBrw:aCols[ nCol + 1 ]:nWidth //24

         :bPaintText    := { | oCol, hDC, cData, aRect, aColors, lHighLite | ;
                           DrawHText( oCol, hDC, cData, aRect, aColors, lHighLite ) }
      END
      oBrw:aCols[ nCol ]:SetColsAsRows( nCol, nCol+1 )
   next  nCol
*/
   :CreateFromCode()

   END
   aBitmaps1 := Nil
   aBmps1    := Nil
   //while oBrush1:nCount > 0
   //      oBrush1:End()
   //end
   //oBrush1:End()
   oBrush1   := Nil
   while oFont1:nCount > 0
         oFont1:End()
   end
   //oFont1:End()
   oFont1    := Nil

Return oXBrw

//----------------------------------------------------------------------------//

METHOD UIXPnel( oDlg, aBmps, aTGrps, nSzBmp, aTxtBmps, bActions, nWidth, nColor ) CLASS TWindowsUI
Local oXBrw
Local n       := 0
Local z       := 0
Local aT      := {}
Local x
Local oWClient
Local nWidth1       := 0
Local nHeight
Local oBrush1
Local oFont1
Local oFont2
Local oFont3
Local oRect
Local y             := 0
Local nColumnas     := 0
Local nFilas        := 0
Local nColVacias    := 0
Local nNumFilas     := 0
Local nMonitor      := ::TipoMonitor()
Local aGruposHeader := {}
Local nBmp          := 3
Local aBitmaps      := {}
Local aBmpPal
Local nPonBorde     := 0
Local hOldWhite
Local hOldShadow
Local nClrTits

DEFAULT aBmps     := {}
DEFAULT aTGrps    := {}
DEFAULT nSzBmp    := 118
DEFAULT aTxtBmps  := {}
DEFAULT bActions  := ""  //{|| .T. }
DEFAULT nWidth    := 0
DEFAULT nColor    := METRO_AZUL3

   Do Case
      Case nMonitor = 0
           nFilas     := 4
           nColumnas  := 90   //9
           nColVacias := 3

      Case nMonitor = 1
           nFilas     := 5
           nColumnas  := 110
           nColVacias := 4

      Case nMonitor = 2
           nFilas     := 6
           nColumnas  := 130
           nColVacias := 5

   EndCase

   For n := 1 to Len( aBmps )
       if !Empty( aBmps[ n ] )
          z := Max( z , Len( aBmps[ n ] ) )
       endif
   Next n
   nNumFilas := z
   //z   := Min( z , nColumnas )
   For n := 1 to z
       AAdd( aT, n )
   Next n

   z := 0
   y := 0
    For n := 1 to Len( aBmps )
       if !Empty( aBmps[ n ] )
          if Empty( y )
             y := n
          endif
          if n <= nColumnas + nColVacias
             AAdd( aBitmaps , ::SetImgUI( aBmps[ n ], nSzBmp, nSzBmp, , ) )
          endif
       else
          AAdd( aGruposHeader, { "Grupo: "+Strzero(y,2)+" a "+Strzero(n-1,2), y, n-1, nil } )
          y := 0
       endif
    Next n
    if y > 0
      AAdd( aGruposHeader, { "Grupo: "+Strzero(y,2)+" a "+Strzero(n-1,2), y, n-1, nil } )
    endif

    For n := 1 to Len( aTGrps )
        if Len( aGruposHeader ) >= n
           aGruposHeader[ n ][ 1 ] := aTGrps[ n ]
        endif
    Next n

   y := 0
   nHeight       := if( ::lMdiUI, oWClient:oWndClient:nHeight - 28,;
                                  GetSysMetrics(1) - 68 )  // ScreenHeight()

   DEFINE BRUSH oBrush1 COLOR nColor

   DEFINE FONT oFont1 NAME "Segoe UI LIGHT" SIZE 0, -22  //-12
   DEFINE FONT oFont2 NAME "Segoe UI LIGHT" SIZE 0, -8
   oFont3 := ::oFontUserUI

   oWClient  := ::oWndUI
   oDlg      := if( ::lMdiUI, oWClient:oWndClient:aWnd[1], oWClient )

   //oXBrw  := TXBrowse():New( oDlg )
   oXBrw  := TXCBrowse():New( oDlg )

   WITH OBJECT oXBrw

   if Upper( oXBrw:ClassName() ) == "TXCBROWSE"
      if !:lBorder
         :nStyle            -= WS_BORDER
      else
         if !lAnd( :nStyle, WS_BORDER )
            :nStyle         += WS_BORDER
         endif
      endif
   else
      if !lAnd( :nStyle, WS_BORDER )

      else
         :nStyle         -= WS_BORDER
      endif
   endif

   if lAnd( :nStyle, WS_BORDER )
      nPonBorde       := ( COL_EXTRAWIDTH ) / 2  //3
   endif

   :lTransparent      := .F.
   :cCaption          := ""    //"Prueba de Browse"
   :l2007             := .F.
   :lAllowColHiding   := .T.
   :lAllowColSwapping := .F.
   :lVScroll          := .F.
   :lHScroll          := .F.   // OJO - Ver si hace falta
   :nRowHeight        :=  124
   //:nFreeze           :=  1
   :nMarqueeStyle     :=  3  //4     //MARQSTYLE_HIGHLCELL   3

   nWidth1   := 0
   For n = 1 to Len( aBmps )
       if !Empty( aBmps[ n ] )
          nWidth1 += ( oXBrw:nRowHeight ) //+ 4
       else
          nWidth1 += INT( oXBrw:nRowHeight / 3 ) //+ 4
       endif
   Next n
   nWidth1 += 2*Len( aBmps )
   if empty( nWidth )
      nWidth  := Min( nWidth1, GetSysMetrics( 0 )  )  //Modificado 06/04/2014
      //nWidth  := nWidth1      // Habia antes
   endif
   //nWidth    := Min( GetSysmetrics(0), nWidth )

   :lHeader        := .T.
   :nHeaderHeight  :=  32
   :lFooter        := .F.
   :nFooterHeight  :=  0 //32
   :nTop           := IF( ::lRbbUI, ::oRbbUI:nHeight+1, ;
                          168 - IF( :lHeader, :nHeaderHeight, 0 ) )
   :nLeft          := Min( (100 - IF( !:lVScroll, 0, 18 ) - IF( Empty( nPonBorde ), -3 , nPonBorde )),;
                           (GetSysMetrics( 0 ) - nWidth - IF( !:lVScroll, 0, 18 ) - nPonBorde ))
   :nLeft          := Max( :nLeft, 100 )                 // A�adido el 06/04/14

   nHeight  := :nRowHeight * nNumFilas + :nTop + 1

   nWidth += :nLeft + nPonBorde - IF( Empty( nPonBorde ) , 0 , 1 )
     // :nRight
   if nWidth > GetSysMetrics( 0 ) - 100       // A�adido el 06/04/14
       :lHScroll  := .T.
   endif
   nWidth := Min( nWidth, GetSysMetrics( 0 ) - IF( !:lVScroll, 0 , 18 ) - 1 ) //- :nLeft )
   :nRight         := nWidth  + IF( !:lVScroll, 0 , 18 )
   //? nWidth
   :nBottom        := nHeight + IF( !:lHScroll, 0 , 18 ) + ;
                      IF( :lFooter, :nFooterHeight, 0 )  + ;                 //+20
                      IF( !Empty( aGruposHeader ), :nHeaderHeight*IF(:lHeader,2,1), 0 )
                      //IF( :lHeader, :nHeaderHeight + 4, 0 ) + ;

   if :lHScroll                                             // OJO A�adido 06/04/14
   :nBottom        := Max( :nBottom, GetSysMetrics( 1 )  )  // OJO A�adido 06/04/14
   endif

   :oBrush         := oBrush1
   :lDesign        := .F.
   :SetFont( oFont1 )

   // DATAs de XCBrowse
   if Upper( :ClassName() ) == "TXCBROWSE"
      :lHeadSelec          := .F.
      :lLinBorder          := .F.
      :lLinDataSep         := .F.
      :lLinHeadVSep        := .F.
      :lLinHeadHSep        := .F.
      :lLinFootVSep        := .F.
      :lLinFootHSep        := .F.
      :lHeadBtton          := .F.
      :lRowDividerComplete := .F.
      :lNoEmpty            := .T.
      //:nWRecordSel              :=  24
      //:nSpRecordSel             :=  0
      :lWheelH             := if( :lHScroll, .T., .F. )
   else
      hOldWhite    := :hWhitePen
      hOldShadow   := :hBtnShadowPen

      :hWhitePen      := CreatePen( PS_SOLID, 1, nColor )  //METRO_AZUL3 )
      :hBtnShadowPen  := CreatePen( PS_SOLID, 1, nColor )  //METRO_AZUL3 )
   endif

   :lRecordSelector   := .F.
   :nRowDividerStyle  := LINESTYLE_NOLINES        //4
   :nColDividerStyle  := LINESTYLE_NOLINES        //4

   if nColor <> METRO_AZUL3
      :nRecSelColor      := METRO_APPWORKSPACE   //Rgb( 202, 162, 126 )
      if nColor <> METRO_WINDOW   //GRIS2
         nClrTits           := METRO_WINDOW
      else
         nClrTits           := METRO_GRIS2
      endif
   else
      :nRecSelColor      := METRO_AZUL8  //APPWORKSPACE   //Rgb( 202, 162, 126 )
       nClrTits          := METRO_WINDOW
   endif
   //:nStretchCol       := STRETCHCOL_LAST
   :nDataLines        :=  1

   :bClrHeader  := { || { nClrTits, nColor, nColor } }
   :bClrFooter  := { || { nClrTits, nColor, nColor } }

   :bClrSel           := { || { METRO_GRIS2, oXBrw:nRecSelColor } }
   :bClrStd           := { || { METRO_GRIS2, nColor } }
   :bClrStds          := { || { METRO_GRIS2, nColor } }
   :bClrSelFocus      := { || { METRO_GRIS2, ;
                              if( len( oXBrw:aCols[ oXBrw:nColSel ]:aBitmaps ) >= oXBrw:nRowSel,;
                                  oXBrw:nRecSelColor, nColor ) } }
                                         //8
   :SetColor( METRO_GRIS2, nColor )

   :SetArray( aT )  //aT
   :lAutoSort         := .F.
   /*
   :bKeyChar          := {| x,y,nF,oCol| XBrw1Elige( oCol:oBrw, x, y, ;
                                   oXBrw:nRowSel, oXBrw:nColSel, nF, oCol ) }
   */
   n := 0
   For n := 1 to Len( aBmps )
       if oXBrw:lHeader
          oXBrw:aCols[ n ]:bClrHeader  := { || { nClrTits, nColor } }
          //oXBrw:aCols[ n ]:nHeadStrAlign := 0
          if !Empty( aBmps[ n ] )
             oXBrw:aCols[ n ]:cHeader     := "" //"Columna"
          endif
       endif
       if oXBrw:lFooter
          oXBrw:aCols[ n ]:bClrFooter  := { || { nClrTits, nColor } }
          if !Empty( aBmps[ n ] )
             oXBrw:aCols[ n ]:cFooter     := "" //"Final"
          endif
       endif
       if !Empty( aBmps[ n ] )
          z++
          oXBrw:aCols[n]:nWidth          := oXBrw:nRowHeight
          oXBrw:aCols[n]:aBitmaps        := aBitmaps[z]
          oXBrw:aCols[n]:bStrData        := { || Nil }
          oXBrw:aCols[n]:bBmpData        := { | oB | oXBrw:nArrayAt }
          oXBrw:aCols[n]:nDataStrAlign   := 1
          oXBrw:aCols[n]:lBmpStretch     := .F. // Rellenar toda la celda con el BitMap
          oXBrw:aCols[n]:lAllowSizing    := .F.
          oXBrw:aCols[n]:nEditType       := 0
          //oXBrw:aCols[n]:bEditValue      := nil
          oXBrw:aCols[n]:lColTransparent := .F. // No funciona si el del Browse SI es TRANSPARENT
          oXBrw:aCols[n]:lBmpTransparent := .F.
          oXBrw:aCols[n]:bClrSel         := { || { , nColor } }
          //oXBrw:aCols[n]:bClrStd         := { || { CLR_BLACK, METRO_AZUL3 } }
          //oXBrw:aCols[n]:bClrSelFocus    := { || { CLR_BLACK, METRO_AZUL3 } }
          if !empty( bActions ) .and. Valtype( bActions ) = "B"
             oXBrw:aCols[n]:bLDClickData    := bActions
          endif

          /*
          oXBrw:aCols[n]:bRClickData     := {|nRow, nCol, nKeyFlags, oCol| MsgInfo( oCol:nPos ) }
          */

          if n < Len( aBmps )
             oXBrw:AddCol()
          endif
       else
          if n < Len( aBmps )
             oXBrw:AddCol()
             oXBrw:aCols[n]:nWidth            := Int( oXBrw:nRowHeight / 3 ) //+1
             oXBrw:aCols[n]:bClrSel           := { || { , nColor } }
             oXBrw:aCols[n]:bClrStd           := { || { , nColor } }
             oXBrw:aCols[n]:bClrSelFocus      := { || { , nColor } }
          endif
       endif
   Next n
                             //Self, nRow, nCol, nkeyFlags
   //oXBrw:aCols[ 1 ]:bToolTip := { | o, nR, nC, nK | Trim("hola")+"/"+StrZero( nR, 3 )+"/"+Str( nC, 3) }

   For n = 1 to Len( oXBrw:aCols )
      if !Empty( oXBrw:aCols[ n ]:Value() ) .or. !Empty( oXBrw:aCols[ n ]:aBitmaps )
   oXBrw:aCols[ n ]:bPaintText = { | oCol, hDC, cText, aCoors, aColors, lHighlight | ;
         DrawTxtRow( oCol, hDC, cText, aCoors , aTxtBmps, , , .F. ,.F., .F., .F., .F., .F. ) }
      endif
   Next n

   For n = 1 to Len( aGruposHeader )
   :SetGroupHeader( aGruposHeader[n][1], aGruposHeader[n][2], aGruposHeader[n][3], oFont3 )
   Next n

   :CreateFromCode()

   END

   aBitmaps  := Nil
   aBmps     := Nil

   oFont1:End()
   oFont2:End()
   /*
   while oFont3:nCount > 0
         oFont3:End()
   enddo
   */
   oFont3:End()

   oFont1    := Nil
   oFont2    := Nil
   oFont3    := Nil

Return oXBrw

//----------------------------------------------------------------------------//

//----------------------------------------------------------------------------//

//----------------------------------------------------------------------------//

//----------------------------------------------------------------------------//

METHOD TipoMonitor() CLASS TWindowsUI
local nAncho
local nTipoMonitor
      nAncho := GetSysMetrics( 0 )
      Do Case
      Case nAncho <= 1400   // 1366
           nTipoMonitor := 0
      Case nAncho > 1400 .and. nAncho < 1700   // 1600
           nTipoMonitor := 1
      Case nAncho >= 1700   //1920
           nTipoMonitor := 2
      EndCase
      ::nTpMonitor   := nTipoMonitor
Return nTipoMonitor

//----------------------------------------------------------------------------//

METHOD CalculaRes( nTp )  CLASS TWindowsUI // nTp -> 0 Ancho   nTp -> 1 Alto
local nAncho
local nAlto
local nPorcAnc
local nPorcAlt
DEFAULT nTp   := 0
      nAncho  := GetSysMetrics( 0 )  //ScreenWidth()  //GetSysmetrics( 4 )
      nAlto   := GetSysMetrics( 1 )  //ScreenHeight() //GetSysmetrics( 3 )
      nPorcAnc   := Round( ( nAncho/1366 ) , 4 )
      nPorcAlt   := Round( ( nAlto/768 ) , 4 )

      ::nPorWidth   := nPorcAnc
      ::nPorHeight  := nPorcAlt
Return IF( Empty( nTp ), nPorcAnc, nPorcAlt )

//----------------------------------------------------------------------------//

METHOD XBrwElige( nPos, oBrw, x, y, nRow, nCol, nF, oCol ) CLASS TWindowsUI
Local oMainBar
Local n
//? nRow, Len( ::GetListWnds() )
if nRow = 1 .and. Len( ::GetListWnds() ) = 1
   ::oMainBarL:Hide()
else
   ::oMainBarL:Hide()
   if IsIconic( ::GetListWnds()[ nRow ]:hWnd )
      //GetListWnds()[ nRow ]:lRepaintUI := .F.
      ::GetListWnds()[ nRow ]:Restore()
   else
      ::GetListWnds()[ nRow ]:Show()
      For n = 1 to Len( ::GetListWnds()[ nRow ]:aControls )
          ::GetListWnds()[ nRow ]:aControls[ n ]:Refresh()
      Next n
   endif
endif

Return nil

//----------------------------------------------------------------------------//

METHOD MiMMove( nRow, nCol, nKeyFlags, bMove, oWnd ) CLASS TWindowsUI
Local lSw  := .F.
oWnd  := Nil
   lSw := ::VerDlgs( nRow, nCol, nKeyFlags, )

   if !empty( bMove )
      Eval( bMove, nRow, nCol, nKeyFlags )
   endif

Return nil

//----------------------------------------------------------------------------//

METHOD SetMnuLeft( oP, x, y ) CLASS TWindowsUI
Local oMn

      MENU POPUP oMn 2010
            MENUITEM " Cerrar " FILE ".\Res\g37116.bmp" ;
                    ACTION ( ::GetListActivos()[ oP:nArrayAt ]:End(),;
                             ::oMainBarL:Refresh() )
                    //ACTION ( ::GetListWnds()[ oP:nArrayAt ]:End() )
      ENDMENU
      ACTIVATE POPUP oMn WINDOW oP ;
      AT ( oP:nRowHeight * oP:nArrayAt ) - 28, Min( 12, Int(oP:nRight/0.6) )
      //AT Min( x, ( oP:nRowHeight * oP:nArrayAt ) - 36 ), Int(oP:nRight/0.6)

Return oMn

//----------------------------------------------------------------------------//

DLL FUNCTION keybd_event( bVk as _INT, ;
                          bScan as _INT, ;
                          dwFlags as LONG, ;
                          dwExtraInfo as LONG ) AS VOID PASCAL ;
                          FROM "keybd_event" LIB "user32.dll"

//keybd_event(VK_F3, 0, 0, 0)   // Press F3 key
//keybd_event(VK_F3, 0, 2, 0)   // Release F3 key
//----------------------------------------------------------------------------//

Function LoadImgUI( aBmps, nH, nW, oButton, nPos )
Local x
Local aBitmaps  := {}
Local aBmpPal
Local cExt      := ""
Local uFile     := ""

DEFAULT aBmps   := {}
DEFAULT nH      := 32
DEFAULT nW      := 32
DEFAULT oButton := Nil
DEFAULT nPos    := 1

if !empty( aBmps )
   Do Case
      Case Valtype( aBmps ) = "C"

           uFile         := aBmps
           cExt          := Right( Upper( uFile ), 3 )
           Do Case
              Case Upper( cExt ) = "BMP"

                    AAdd( aBitmaps, Array(6) )
                    aBmpPal   := PalBmpRead( , aBmps )
                    aBitmaps[ x, 1 ]   :=  aBmpPal[ 1 ]
                    aBitmaps[ x, 2 ]   :=  aBmpPal[ 2 ]
                    aBitmaps[ x, 3 ]   :=  nH   //64
                    aBitmaps[ x, 4 ]   :=  nW   //108
                    aBitmaps[ x, 5 ]   :=  0
                    aBitmaps[ x, 6 ]   :=  .F.
                    aBmpPal   := Nil

              Case Upper( cExt ) = "PNG"

                    AAdd( aBitmaps, Array(6) )
                    aBmpPal := FWOpenPngFile( aBmps )
                    aBitmaps[ x, 1 ]   :=  aBmpPal
                    aBitmaps[ x, 2 ]   :=  0
                    aBitmaps[ x, 3 ]   :=  nH   //64
                    aBitmaps[ x, 4 ]   :=  nW   //108
                    aBitmaps[ x, 5 ]   :=  0
                    aBitmaps[ x, 6 ]   :=  .F.
                    aBmpPal   := Nil


           EndCase

      Case Valtype( aBmps ) = "A"

        For x = 1 to Len( aBmps )
            if Valtype( aBmps[ x ] ) = "A"
               uFile        := aBmps[ x ][ nPos ]
            else
               uFile        := aBmps[ x ]
            endif
            cExt         :=  Right( Upper( uFile ), 3 )
            Do Case
               Case Upper( cExt ) = "BMP"

                    AAdd( aBitmaps, Array(6) )
                    if Valtype( aBmps[ x ] ) = "A"
                    aBmpPal   := PalBmpRead( , aBmps[ x ][ nPos ] )
                    else
                    aBmpPal   := PalBmpRead( , aBmps[x] )
                    endif
                    aBitmaps[ x, 1 ]   :=  aBmpPal[ 1 ]
                    aBitmaps[ x, 2 ]   :=  aBmpPal[ 2 ]
                    aBitmaps[ x, 3 ]   :=  nH   //64
                    aBitmaps[ x, 4 ]   :=  nW   //108
                    aBitmaps[ x, 5 ]   :=  0
                    aBitmaps[ x, 6 ]   :=  .F.
                    aBmpPal   := Nil

               Case Upper( cExt ) = "PNG"

                    AAdd( aBitmaps, Array(6) )
                    if Valtype( aBmps[ x ] ) = "A"
                    aBmpPal   := PalBmpRead( , aBmps[ x ][ nPos ] )
                    else
                    aBmpPal := FWOpenPngFile( aBmps[ x ] )
                    endif
                    aBitmaps[ x, 1 ]   :=  aBmpPal
                    aBitmaps[ x, 2 ]   :=  0
                    aBitmaps[ x, 3 ]   :=  nH   //64
                    aBitmaps[ x, 4 ]   :=  nW   //108
                    aBitmaps[ x, 5 ]   :=  0
                    aBitmaps[ x, 6 ]   :=  .F.
                    aBmpPal   := Nil

            EndCase
        Next x

   EndCase
endif
/*
              oButton:HasAlpha( oButton:hBitmap1, BTN_UP )
              oButton:nAlphaLevel( nLevelAlpha )
              oButton:Refresh()
*/
return aBitmaps

//----------------------------------------------------------------------------//

Function CargaImgBtt( uFile , nPos , oButton )
Local  lSw    := .F.
Local  cExt   := ""
Local  x      := 0

if !empty( uFile )
   Do Case
      Case ValType( uFile ) = "C"
           cExt          :=  Right( Upper( uFile ), 3 )
           if !Empty( oButton )
              if  cExt  =  "BMP"
                 oButton:LoadBitmaps( ,, uFile )
                 lSw    := .T.
              else
                 oButton:hBitmap1  := FWOpenPngFile( uFile )
                 oButton:hPalette1 := 0
                 lSw    := .T.
     /*
     if IsGdiObject( hPng )
         if IsGdiObject( oBmp:hBitmap )
            DeleteObject( oBmp:hBitmap )
         endif
         oBmp:hBitmap = hPng
      */
              endif
              //oButton:aLevel[1] := 80
           endif
           if lSw
              oButton:HasAlpha( oButton:hBitmap1, BTN_UP )
              oButton:nAlphaLevel( nLevelAlpha )
              oButton:Refresh()
           endif
      Case ValType( uFile ) = "A"

         For x = 3 to 6

           cExt          :=  Right( Upper( uFile[ nPos , x ] ), 3 )

           if !Empty( oButton )
             if  cExt  =  "BMP"
                 oButton:LoadBitmaps( ,, uFile[ nPos , x ], uFile[ nPos , x++ ] )
                 lSw    := .T.
                 //if lSw
                 //   oButton:HasAlpha( oButton:hBitmap1, BTN_UP )
                 //   oButton:Refresh()
                 //endif
                 x  := 7
             else
               if !Empty( uFile[ nPos , x ] )
                 Do Case
                    Case x = 3
                         oButton:hBitmap1  := FWOpenPngFile( uFile[ nPos , x ] )
                         oButton:hPalette1 := 0
                         if !Empty( oButton:hBitmap1 )
                            lSw    := .T.
                         endif
                         if lSw
                            oButton:HasAlpha( oButton:hBitmap1, BTN_UP )
                            oButton:Refresh()
                         endif
                    Case x = 4
                         oButton:hBitmap2  := FWOpenPngFile( uFile[ nPos , x ] )
                         oButton:hPalette2 := 0
                         if !Empty( oButton:hBitmap2 )
                            lSw    := .T.
                         endif
                         if lSw
                            oButton:HasAlpha( oButton:hBitmap2, BTN_UP )
                            oButton:Refresh()
                         endif

                    Case x = 5
                         oButton:hBitmap3  := FWOpenPngFile( uFile[ nPos , x ] )
                         oButton:hPalette3 := 0
                         if !Empty( oButton:hBitmap3 )
                            lSw    := .T.
                         endif
                         if lSw
                            oButton:HasAlpha( oButton:hBitmap3, BTN_UP )
                            oButton:Refresh()
                         endif

                    Case x = 6
                         oButton:hBitmap4  := FWOpenPngFile( uFile[ nPos , x ] )
                         oButton:hPalette4 := 0
                         if !Empty( oButton:hBitmap4 )
                            lSw    := .T.
                         endif
                         if lSw
                            oButton:HasAlpha( oButton:hBitmap4, BTN_UP )
                            oButton:Refresh()
                         endif

                  EndCase
               endif
             endif
           endif

         Next x

   EndCase
endif
Return lSw

//----------------------------------------------------------------------------//

static function DrawHText( oCol, hDC, cData, aRect, aColors, lHighLite )

   local oBrw        := oCol:oBrw
   local nTop        := aRect[ 1 ]
   local nLeft       := aRect[ 2 ]
   local nBottom     := aRect[ 3 ]
   local nRight      := aRect[ 4 ]
   local nTxtWidth   := oBrw:GetWidth( cData, oCol:oDataFont )
   local nTxtHeight  := GetTextHeight( oBrw:hWnd, hDC )

   nBottom           := ( nTop + nBottom + nTxtHeight + 12 ) // /2 //
   //nLeft             := ( nRight - nTxtWidth ) / 2

   DrawTextEx( hDC, cData, { nTop, nLeft, nBottom, nRight }, DT_LEFT+DT_VCENTER )

return nil

//----------------------------------------------------------------------------//

Function DrawTxtRow( oCol, hDC, cText, aCoors, aItems, cFont, nHFont, lVert,;
                     lEnBmp, lLinHT, lLinHB, lLinVL, lLinVR )
   local hOldFont
   Local aColor  := { 16777215, 12632256, 8421504, 4210752, 0 }
   Local aCRgb   := { 0, 64, 128, 192 , 255 }
   Local nCSel   := 0
   Local n
   Local oFontt
   Local hFontt
   Local nEscpe
   Local nOrient
   Local nAncho  := 0
   Local nColor
   Local hPen
   Local nVacio     := 0
   Local nDesp      := 20  //24    // 12

   DEFAULT lEnBmp   := .F.
   DEFAULT lLinHT   := .F.
   DEFAULT lLinHB   := .F.
   DEFAULT lLinVL   := .F.
   DEFAULT lLinVR   := .F.
   DEFAULT cFont    := "Segoe UI"  //SemiLight"  // Si es esta poner BOLD
   DEFAULT nHFont   := -11
   DEFAULT aItems   := {}
   DEFAULT lVert    := .F.

//if Len( oCol:aBitmaps ) >= oCol:oBrw:nArrayAt  //!Empty( Eval( oCol:bBmpData ) ) .and. ;
   if lVert
   DEFINE FONT oFontt NAME cFont SIZE 0, nHFont NESCAPEMENT 900 //BOLD
   else
   DEFINE FONT oFontt NAME cFont SIZE 0, nHFont //BOLD
   endif


   if lLinHT .or. lLinHB
    For n = 1 to oCol:nPos-1
       if Empty( oCol:aBitmaps )     //!Empty( Eval( oCol:bBmpData ) )
          nAncho  += oCol:oBrw:aCols[ n ]:nWidth
          nVacio  := nVacio + 1
       else
          nAncho  += oCol:oBrw:aCols[ n ]:nWidth
       endif
    Next n
   endif

   nColor  := GetPixel( hDC, aCoors[ 2 ] + 4 , aCoors[ 1 ] + 4 )
   if lEnBmp
      For n = Len( aColor ) to 1 step -1
          if nColor <= aColor[ n ]
             nCSel  := aCRgb[ n ]
             n := 0
          endif
      Next n
   else         //((aColor[ 3 ] + aColor[ 4 ])/2)
      if nColor < ( ( aColor[ 1 ] + aColor[ 2 ] ) / 3  )
         nCSel := aColor[ 1 ]
         hPen  := oCol:oBrw:hWhitePen
      else
         nCSel := aColor[ 5 ]
         hPen  := oCol:oBrw:hRowPen
      endif
   endif

   SetTextColor( hDC, nCSel )
   SetBkColor( hDC, nColor  )

   if !Empty( cText )
      DrawText( hDC, cText, aCoors )
      //DrawTextEx( hDC, cText, aCoors, DT_LEFT + DT_VCENTER )
   endif

   if Len( aItems ) > 0
                           // nPos
      if !Empty( aItems[ oCol:nCreationOrder ] ) //[ oCol:oBrw:KeyNo ] )
                        //  nPos
       if Len( aItems[ oCol:nCreationOrder ] ) >= oCol:oBrw:KeyNo
                             // nPos
        if !Empty( aItems[ oCol:nCreationOrder ][ oCol:oBrw:KeyNo ] )
         hOldFont = SelectObject( hDC, oFontt:hFont )
         if !lVert                  // nPos
           DrawText( hDC, aItems[ oCol:nCreationOrder ][ oCol:oBrw:KeyNo ],;
              { aCoors[1] + oCol:nWidth-24, ;
                aCoors[2] + 12, ;              // Abs( nHFont )+1, ;  // 26
                aCoors[3], aCoors[4] } )
         else
      //DrawTextEx( hDC, cData, { nBottom, nLeft, nTop, nRight }, DT_LEFT + DT_VCENTER )
                                             // nPos
           DrawTextEx( hDC, Upper( aItems[ oCol:nCreationOrder ][ oCol:oBrw:KeyNo ] ),;
              { aCoors[3] - IF( lLinHB,2*( Abs( nHFont )+1 ),Abs( nHFont )+1),;
                aCoors[2], aCoors[1], ;       //DT_LEFT + DT_VCENTER
                aCoors[2] + (Abs(nHFont)+1)*2 }, DT_LEFT + DT_VCENTER )
                                     //26  //No funciona la alineacion
         endif
         SelectObject( hDC, hOldFont )                // 12
        endif
       endif
      endif
   endif

   if !Empty( oCol:aBitmaps ) //.or. !oCol:oBrw:lNoEmpty
      //aCoors[ 1 ] += oCol:nWidth - 23
      if lLinHT
       //DrawHorz( hDC, aCoors[ 1 ]+nDesp, aCoors[ 2 ], nAncho, hPen )
       MoveTo( hDC, aCoors[ 2 ], aCoors[ 1 ]+nDesp )
       LineTo( hDC, aCoors[ 4 ]+oCol:nWidth-IF(lLinVR,0,1), aCoors[ 1 ]+nDesp, hPen )
      endif
      if lLinHB
       //DrawHorz( hDC, aCoors[ 3 ]-nDesp, aCoors[ 2 ], nAncho, hPen )
       MoveTo( hDC, aCoors[ 2 ], aCoors[ 3 ]-nDesp )
       LineTo( hDC, aCoors[ 4 ]+oCol:nWidth-IF(lLinVR,0,1), aCoors[ 3 ]-nDesp, hPen )
      endif
      //aCoors[ 2 ] += 12   //5
      if lLinVL
       //DrawVert( hDC, aCoors[ 2 ]+nDesp, aCoors[ 1 ], aCoors[ 3 ], hPen )
       MoveTo( hDC, aCoors[ 2 ]+nDesp, aCoors[ 1 ]+IF( Empty( nDesp), 0, 1 ) )
       LineTo( hDC, aCoors[ 2 ]+nDesp, aCoors[ 1 ]+oCol:oBrw:nRowHeight-4-IF( Empty( nDesp), 0, 1 ), hPen )
      endif
      if lLinVR
       MoveTo( hDC, aCoors[ 2 ]-nDesp+oCol:nWidth-6, aCoors[ 1 ]+IF( Empty( nDesp), 0, 1 ) )
       LineTo( hDC, aCoors[ 2 ]-nDesp+oCol:nWidth-6, aCoors[ 1 ]+oCol:oBrw:nRowHeight-4-+IF( Empty( nDesp), 0, 1 ), hPen )
//       DrawVert( hDC, oCol:nWidth-3-nDesp, aCoors[ 1 ], aCoors[ 3 ], hPen )
      endif
   endif

   oFontt:End()
//endif
   oFontt    := nil
   hPen      := nil

return nColor

//----------------------------------------------------------------------------//

Function SinB( cP )
Local cTmp1  := ""
Local cTmp2  := ""
Local nL
Local nPos
nL     := Len( RTrim( cP ) )
nPos   := Rat( "\", RTrim( cP ) )
if nL = nPos
   cTmp1  := Left( cP, nPos-1 )
   cTmp2  := " "
else
   cTmp1  := cP
endif
cP        := cTmp1 + cTmp2
//? nL, nPos, cTmp1, cTmp2, cTmp1+cTmp2
Return cP

//----------------------------------------------------------------------------//
/*
#define GWL_STYLE -16

SetWindowLong(oWnd:hWnd, GWL_STYLE, nOr( GetWindowLong(oWnd:hWnd, GWL_STYLE), WS_MAXIMIZEBOX ))
*/

DLL32 function setwindowlong( hwnd as LONG, index as LONG, newlog as LONG ) ;
      AS LONG PASCAL FROM "SetWindowLongA" lib "user32.dll"


DLL32 function getwindowlong( hwnd as LONG, index as LONG ) AS LONG PASCAL ;
                              FROM "GetWindowLongA" lib "user32.dll"

/*
hWnd: identifica la ventana e, indirectamente, la clase a la que pertenece.
nIndex: especifica el desplazamiento, empezando en cero, del valor a recuperar.
        Los valores v�lidos est�n en el rango desde cero hasta el n�mero de
        bytes de memoria extra de la ventana, menos cuatro;
        por ejemplo, si se especifican 12 o m�s bytes de memoria extra,
        un valor de 8 ser� un �ndice para el tercer entero de 32 bits.
        Para recuperar cualquier otro valor, especificar uno de los valores
        siguientes:

Valor	Accion
GWL_EXSTYLE	Recupera los estilos extendidos de la ventana.
GWL_STYLE	Recupera los estilos de ventana.
GWL_WNDPROC	Recupera la direcci�n del procedimiento de ventana, o un manipulador
            que representa la direcci�n del procedimiento de ventana.
            Se debe usar la funci�n CallWindowProc para invocar al procedimiento
            de ventana.
GWL_HINSTANCE	Recupera el manipulador de la instancia de la aplicaci�n.
GWL_HWNDPARENT	Recupera el manipulador de la ventana padre, si existe.
GWL_ID	      Recupera el identificador de la ventana.
GWL_USERDATA	Recupera el valor de 32 bits asociado con la ventana.
               Cada ventana tiene su correspondiente valor de 32 bits destinado
               al uso por la aplicaci�n que cre� la ventana.

Los siguientes valores tambi�n est�n disponibles cuando el par�metro hWnd identifica un cuadro de di�logo:
Valor	Accion
DWL_DLGPROC  	Recupera la direcci�n del procedimiento del cuadro de di�logo,
               o un manipulador que representa la direcci�n del procedimiento
               del cuadro de di�logo. Se debe usar la funci�n CallWindowProc
               para invocar el procedimiento del cuadro de di�logo.
DWL_MSGRESULT	Recupera el valor de retorno de un mensaje procesado en el
               procedimiento de cuadro de di�logo.
DWL_USER	      Recupera informaci�n privada extra para la aplicaci�n,
               como son manipuladores o punteros.
*/

//----------------------------------------------------------------------------//
// Establece el grado de opacidad de una ventana (o Di�logo, o control, imagino)

//DLL32 function setlayeredwindowattributes( hwnd as LONG, crkey as LONG, ;
//      alpha as WORD, flag as LONG ) AS LONG PASCAL ;
//      FROM "SetLayeredWindowAttributes" lib "user32.dll"

//FUNCTION TranspColor( oWnd, nRGB )
//SetWindowLong( oWnd:hWnd, GWL_EXSTYLE, nOr( GetWindowLong( oWnd:hWnd, GWL_EXSTYLE ), WS_EX_LAYERED ) )
//SetLayeredWindowAttributes( oWnd:hWnd, nRgb, 0, LWA_COLORKEY )
//RETURN NIL
/*

//----------------------------------------------------------------------------//

/*
#define MF_BYPOSITION 1024 // 0x0400
#define MF_DISABLED   2
FUNCTION RemoveMItems(oWin, lDisable)

LOCAL hMenu  := 0
LOCAL nCount := 0

IF lDisable
   hMenu  = GetSystemMenu(oWin:hWnd, .F.)
   nCount = GetMItemCount(hMenu)
   IF oWin:ClassName() = "TDIALOG"
      RemoveMenu(hMenu, 1, nOR( MF_BYPOSITION, MF_DISABLED) )
   ELSE
      RemoveMenu(hMenu, nCount - 3, nOR( MF_BYPOSITION, MF_DISABLED) )
      RemoveMenu(hMenu, nCount - 7, nOR( MF_BYPOSITION, MF_DISABLED) )
   ENDIF
   DrawMenuBar( oWin:hWnd )
ELSE
   GetSystemMenu( oWin:hWnd, .T. )
   DrawMenuBar( oWin:hWnd )
ENDIF

RETURN nil
*/

//----------------------------------------------------------------------------//

//----------------------------------------------------------------------------//

//----------------------------------------------------------------------------//

//----------------------------------------------------------------------------//

//----------------------------------------------------------------------------//

//----------------------------------------------------------------------------//

//----------------------------------------------------------------------------//

//----------------------------------------------------------------------------//

//----------------------------------------------------------------------------//

