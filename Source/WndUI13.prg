
/*
 *
 * Proyecto: FiveUI
 * Fichero:  WndUI13.prg
 * Descripci�n: Test Clase WindowsUI, y clases heredadas
 *              Definicion de Ventana:
 *              - Entorno UI
 *              - Paneles laterales
 *              - Imagenes de en cuadro izquierdo
 *              - Panel Principal UI
 * Autor: Cristobal Navarro Lopez (C)
 * Fecha: 03/10/2013
 *
 */


#include "Fivewin.ch"

#include "colores.ch"


Static   oWinUI
Static   nRefresh          := 0

Function Main()
Local lMax    := .T.
Local lEsMdi  := .F.
Local nColor  := METRO_AZUL3
Local cTit    := "Inicio"
Local cUser   := WNetGetUser()
//Local aBtt    := { ".\Res\Guest32.bmp", ".\Res\Guest32.bmp" }
Local aBtt    := { { ".\Res\Guest32.bmp", ".\Res\Guest32.bmp" }, ;
                   { ".\Res\OnOff32.bmp", ".\Res\OnOff32.bmp" }, ;
                   { ".\Res\Busca32.bmp", ".\Res\Busca32.bmp" } }

Local oDlgs
Local oDlgR
Local oDlgL
Local oDlgT
Local oDlgB

Local bActions
Local aBmps1      := { ;
                       ".\inicio.bmp" ,;
                       ".\inicio.bmp" ,;
                       ".\inicio.bmp" ,;
                       ".\inicio.bmp"      ;
                     }
Local aBmps2      := { ;
                       ".\inicio.bmp" ,;
                       ".\inicio.bmp" ,;
                       ".\inicio.bmp" ;
                       }
Local aBmps3      := { ;
                       ".\inicio.bmp",;
                       ".\inicio.bmp",;
                       ".\inicio.bmp",;
                       ".\inicio.bmp";
                     }

Local aBmps4      := { ;
                       ".\inicio.bmp",;
                       ".\inicio.bmp"  ;
                     }

Local aBmps5      := { ;
                       ".\inicio.bmp",;
                       ".\inicio.bmp",;
                       ".\inicio.bmp",;
                       ".\inicio.bmp" ;
                       }
Local aBmps6      := { ;
                       ".\inicio.bmp" ,;
                       ".\inicio.bmp" ,;
                       ".\inicio.bmp"  ;
                     }
Local aBmps7      := { ;
                       ".\inicio.bmp" ,;
                       ".\inicio.bmp" ,;
                       ".\inicio.bmp"  ;
                     }

Local aBmps8      := { ;
                       ".\inicio.bmp",;
                       ".\inicio.bmp",;
                       ".\inicio.bmp",;
                       ".\inicio.bmp";
                     }

Local aBmps9      := { ;
                       ".\inicio.bmp",;
                       ".\inicio.bmp",;
                       ".\inicio.bmp";
                       }

Local aItems      := { ;// Deberia ser: {"Texto",cFnt,nHFnt,lBold,lItal,lLHT,lHHB,lLVL,lLVR}
                       {"Double Click","Opcion 12","Opcion 13","Opcion 14",,},;
                       {"Opcion 21","Opcion 22","Opcion 23"},;
                       {},;
                       {"Opcion 31","Opcion 32","Opcion 33"},;
                       {"Opcion 41","Opcion 42",,},;
                       {},;
                       {"Opcion 51","Opcion 52","Opcion 53","Opcion 54"},;
                       {"Opcion 61","Opcion 62","Opcion 63"},;
                       {},;
                       {"Opcion 71","Opcion 72","Opcion 73"},;
                       {"Opcion 81","Opcion 82","Opcion 83","Opcion 84"},;
                       {"Opcion 91","Opcion 92","Opcion 93"},;
                       {},;
                       {,,,};
                       }
                    /*

                       {},;
                       {,,,},;
                       {,,,},;
                       {,,,};
                      }
                    */

Local aBmps     := { aBmps1, aBmps2, , aBmps3, aBmps4, , ;
                       aBmps5, aBmps6, , aBmps7, aBmps8 , aBmps9 }
Local aTitGrps  := { "Grupo I", "Grupo II", "Grupo III", ;
                     "Grupo IV ( Click Right )" }



   // Sets generales---------------------------------------------------------
   SET EPOCH TO 1990               // Admite los a�os desde el 1990 en adelante
   SET CENTURY ON                  // 4 d�gitos a�o
   SET DELETED ON                  // Impedir ver registros marcados borrar
   //SetCancel( .F. )              // Inutiliza ALT + C para abortar programa
   //SetDialogEsc( .F. )           // Impide salir Di�logos con Escape
   SET DATE FORMAT "DD/MM/YYYY"
   SET DECIMALS TO 2
   XBrNumFormat( "E", .T.)

   SetResDebug( .T. )

   //---------------------------     -----------------------------------------//
   // Modelo 002:  Ventana: Panel principal
   //                       En ventana principal: NO MDI (No es necesario MDI)
   //              Definicion de cuadros de di�logos laterales
   //---------------------------     -----------------------------------------//

   oWinUI := TWindowsUI():PnelPPal( .T., lMax, lEsMdi, nColor, cTit, cUser, aBtt)

   // Activa la visualizacion de ventanas en panel izquierdo
   oWinUI:lSaveBmp    := .T.

   oWinUI:lDlgRight   := .T.
   oWinUI:SRightUpUI( , , , , )

   oWinUI:lDlgLeft    := .T.
   oWinUI:SLeftUpUI( , , , , )

   oWinUI:lDlgBottom  := .F.
   oWinUI:lDlgTop     := .F.

   bActions := { | x, y, nF, oCol | XBrwMnu( oCol:oBrw:nRowSel, ;
                                             oCol:oBrw:nColSel, oCol )}

   oWinUI:UIXPnel( oWinUI, aBmps, aTitGrps, 118, aItems, bActions )


   oWinUI:ActivaUI()

   Hb_GCall(.t.)
   CLEAR MEMORY

   if File( "checkres.txt" )
      FErase( "checkres.txt" )
   endif
   CheckRes()

Return nil

//----------------------------------------------------------------------------//
//----------------------------------------------------------------------------//
//----------------------------------------------------------------------------//

//----------------------------------------------------------------------------//
Function XBrwMnu( nRow, nCol, oCol )
Local nVentanas  :=  Len( oWinUI:GetListWnds() )

Do Case
   Case nCol = 1
        Do Case
           Case nRow = 1
               //? nVentanas
               //cTitle, cMsg, oWnd, nColor, oFont1, lFlat, nWd, nHg
               TDialogUI():DlgMsgUI( "N� de Ventanas", Str( nVentanas ), , , , , ,)

           Case nRow = 2

           Case nRow = 3

           Case nRow = 4

        EndCase

   Case nCol = 2
        Do Case
           Case nRow = 1

           Case nRow = 2
                //ApliPPalUI()
                //oWinUIApp:ActivaUI()

           Case nRow = 3

           Case nRow = 4

        EndCase

   Case nCol = 4
        Do Case
           Case nRow = 1

           Case nRow = 2

           Case nRow = 3

           Case nRow = 4

        EndCase

   Case nCol = 5
        Do Case
           Case nRow = 1

           Case nRow = 2

           Case nRow = 3

           Case nRow = 4

        EndCase


   Case nCol = 7
        Do Case
           Case nRow = 1
//                Editor()
                //? Len( oWinUI:HazListWnds() )

           Case nRow = 2
//                TextoVertical()
           Case nRow = 3
//                TextoHorizontal()
           Case nRow = 4
//                ObjectCells()
       EndCase

   Case nCol = 8
        Do Case
           Case nRow = 1
//                MetroUI02()
           Case nRow = 2
//                MultiLineCells()
                //ApliPPalUI()
                //oWinUIApp:ActivaUI()
           Case nRow = 3
//                UsarAcces()
           Case nRow = 4
        EndCase

   Case nCol = 9
        Do Case
           Case nRow = 1
//                MetroUI02()
           Case nRow = 2
//                MultiLineCells()
                //ApliPPalUI()
                //oWinUIApp:ActivaUI()
           Case nRow = 3
//                UsarAcces()
           Case nRow = 4
        EndCase

   Otherwise
        if Empty( nCol ) .or. nCol > Len( oCol:oBrw:aCols )
           ? "Columna no definida"
        endif
EndCase

Return nil


//----------------------------------------------------------------------------//

//----------------------------------------------------------------------------//

//----------------------------------------------------------------------------//

//----------------------------------------------------------------------------//
