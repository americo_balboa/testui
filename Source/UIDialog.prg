/*
* Aplicacion  :
* Clase       : TDialogUI
* Descripcion : Panel Contenedor dependiente de la ventana inicial
*
*
*/

//----------------------------------------------------------------------------//
#include "colores.ch"
//----------------------------------------------------------------------------//
#include "fivewin.ch"
//----------------------------------------------------------------------------//
#define COL_EXTRAWIDTH        6
//----------------------------------------------------------------------------//

CLASS TDialogUI FROM TWindowsUI

      DATA   lOkUI
      //DATA   nDlgWidth

      METHOD NewDlg( lMax, lMdi, nTop, nLeft, nWidth, nHeight, nColor ) CONSTRUCTOR
      METHOD ActivaDlgUI()
      METHOD DlgMsgUI(  cTitle, cMsg, oWnd, nColor, oFont1, lFlat, nWd, nHg  ) CONSTRUCTOR
      //oWnd, nWd, nHg, nColor, oFont1, cMsg ) CONSTRUCTOR


ENDCLASS

//----------------------------------------------------------------------------//

METHOD NewDlg( lMax , lMdi, oParent, nTop, nLeft, nWidth, nHeight, nColor ) CLASS TDialogUI

local oMnu
local oBrush

DEFAULT lMax          := .F.
DEFAULT lMdi          := .F.
DEFAULT nColor        := METRO_GRIS2  //APPWORKSPACE
DEFAULT nTop          := 0
DEFAULT nLeft         := 0
DEFAULT nWidth        := GetSysMetrics( 0 )
DEFAULT nHeight       := GetSysMetrics( 1 )

DEFAULT oParent       := GetWndDefault()

   ::lOKUI          := .F.

   ::TipoMonitor()
   ::CalculaRes()

   ::lDemoUI        := .F.
   ::lRepaintUI     := .T.
   ::lMax           := lMax
   ::lMdiUI         := lMdi
   //::cBmpWndUI      := "WndUI"+StrZero( Len( aWindowsUI ) + 1, 3)+".bmp"

   ::Initiate(  .F., lMax , lMdi, nColor, , oParent,  )

   oBrush           := ::SetBrush( , , nColor )

   DEFINE DIALOG ::oWndUI ; // MENU oMnu //;
          STYLE nOr( WS_CHILD, WS_POPUP ) ; //WS_POPUP ;
          SIZE nWidth, nHeight ;
          BRUSH oBrush ;        //          OF oParent ;
          PIXEL

//   ::oWndUI:SetColor( , METRO_GRIS3 )
   ::oWndUI:SetBrush( oBrush )

   ::oWndUI:nTop    := nTop
   ::oWndUI:nLeft   := nLeft

   ::lPosInit       := .F.

   ::lBttIconUI     := .T.
   ::aBttIconUI     := {,}
   ::lBttExitUI     := .F.
   ::bValExitUI     := { || .T. }
   ::lBttMaxiUI     := .F.
   ::aFilMaxiUI     := { ".\Res\path70901n.bmp", ".\Res\path70902.bmp" }
   ::lBttMiniUI     := .F.
   ::aFilMiniUI     := { ".\Res\path70911n.bmp", ".\Res\path70912.bmp" }
   ::lBttNormUI     := .F.
   ::aFilNormUI     := { ".\Res\g71112n.bmp", ".\Res\g71122.bmp" }
   ::nRowBttUI      := 1
   ::nColBttUI      := GetSysMetrics(0) - 26
   ::nRowBttIcoUI   := 1
   ::nColBttIcoUI   := 1

   ::aBttExitUI     := {".\Res\g60645n.bmp", ".\Res\g60646.bmp"}

   ::lBarUI         := .F.
   ::lFldExUI       := .F.
   ::lFldUI         := .F.
   ::lRbbUI         := .F.
   ::lTitUI         := .F.
   ::lPnelUI        := .F.
   ::lWPnelUI       := .F.
                                        //SemiLight //BOLD
   ::oFontTitleUI   := TFont():New("Segoe UI Light", 0 , -54 )    //-12
   ::oFontUserUI    := TFont():New("Segoe UI SemiLight", 0 , -24 )
   ::oFontApliUI    := TFont():New("Segoe UI Light", 0 , -22 )

   ::aPos           := { 0, 0 } //, 0, 0 , .F. )

   oBrush:End()
   oBrush           := Nil
   oMnu             := Nil

Return Self

//----------------------------------------------------------------------------//

METHOD ActivaDlgUI( lParent, lModal ) CLASS TDialogUI
local oUI         := if( ::lMdiUI, ::oWndUI:oWndClient, ::oWndUI )
local cShow       := IF( ::lMax, "CENTERED", "NORMAL" )
local bIni
local bLClick
DEFAULT lParent   := .T.
DEFAULT lModal    := .F.

if Empty( ::oWndUI:bInit )
   bIni     := { | o | Self:IniFunUI( Self:lMdiUI ) } //, ;
                       //Self:oWndUI:Move( 100, 100 ) }
                       //Self:oWndUI:Move( ::oWndUI:nTop, ::oWndUI:nLeft ) }
else
   bIni     :=  ::oWndUI:bInit
endif
if !empty( ::oWndUI:bLClicked )
   bLClick  := ::oWndUI:bLClicked
endif
                      // ::lMax                      // lParent
::oWndUI:Activate( bLClick, , , ::lMax, , lModal, bIni , , , , lParent )

 //ACTIVATE DIALOG ::oWndUI CENTERED
Return nil

//----------------------------------------------------------------------------//
//             ( oWnd, nWd, nHg, nColor, oFont1, cMsg, lFlat, cTitle )
METHOD DlgMsgUI( cTitle, cMsg, oWnd, nColor, oFont1, lFlat, nWd, nHg )  CLASS TDialogUI
Local oDlgUI
Local oBtn1
Local oBtn2
Local oFont2    := TFont():New("Segoe UI Light", 0 , -20 )
Local nLin      := 1 //Max( 1, nCuantas( cMsg, CRLF ) )
Local x
Local cTemp     := ""
DEFAULT oWnd    := Nil
DEFAULT nWd     := ScreenWidth()
DEFAULT nHg     := Int( ScreenHeight() / 4 ) + 32
DEFAULT nColor  := METRO_AZUL4 //Rgb( 123, 140, 223 )  //METRO_AZUL0
DEFAULT cMsg    := ""
DEFAULT oFont1  := TFont():New("Segoe UI SemiLight", 0 , -24 )
DEFAULT lFlat   := .T.
DEFAULT cTitle  := "Atencion"

  if Valtype( cMsg ) = "C"
     cTemp   := cMsg
  endif
  if Valtype( cMsg ) = "N"
     cTemp   := LTrim( Str( cMsg ) )
  endif
  if Valtype( cMsg ) = "O"
     cTemp   := "OBJECT"
  endif
  if Valtype( cMsg ) = "D"
     cTemp   := DToc( cMsg )
  endif
  if Valtype( cMsg ) = "A"
     //cMsg   := "ARRAY"
     For x = 1 to Len( cMsg )
         if Valtype( cMsg[ x ] ) = "C"
            cTemp += cMsg[ x ] + if( x < Len( cMsg ), CRLF, "" )
         endif
         if Valtype( cMsg[ x ] ) = "D"
            cTemp += DToc(cMsg[ x ]) + if( x < Len( cMsg ), CRLF, "" )
         endif
         if Valtype( cMsg[ x ] ) = "N"
            cTemp += LTrim( Str( cMsg[ x ] )) + if( x < Len( cMsg ), CRLF, "" )
         endif
         if Valtype( cMsg[ x ] ) = "O"
            cTemp += "OBJECT" + if( x < Len( cMsg ), CRLF, "" )
         endif
         // Ojo, Aqui habr�a que hacer una rutina de tipo recursivo
         if Valtype( cMsg[ x ] ) = "A"
            cTemp += "ARRAY" + if( x < Len( cMsg ), CRLF, "" )
         endif


     Next x

  endif
  nLin      := Max( 1, nCuantas( cTemp, CRLF ) )

  ::lOkUI  := .F.
              //::oWndUI
  DEFINE DIALOG oDlgUI STYLE nOr( WS_CHILD, WS_POPUP ) ;
      SIZE nWd, nHg ;
      PIXEL COLOR METRO_WINDOW, nColor
      //Int( nWd / 4 ) - Int( len( cTitle ) / 2 )

                //6
  @ 4, Int( nWd / 6 ) SAY cTitle ;
                    OF oDlgUI FONT oFont1 PIXEL TRANSPARENT
                                   //- Int( len( cMsg ) / 2 )
  if !empty( cTemp )
     @ Int( nHg/6 )-10, Int( nWd / 6 )  SAY cTemp ;
                     SIZE 300, 24*nLin ;
                     OF oDlgUI FONT oFont2 PIXEL TRANSPARENT
//GetTextWidth( cMsg, oFont2:nWidth ), GetTextHeight( cMsg, -oFont2:nHeight )*nLin ;
  endif

  if lFlat
   @ (nHg/2) - 24, 2*Int( nWd/6) + 50 FLATBTN oBtn1 ;
      PROMPT "Aceptar" OF oDlgUI ;
      SIZE 50, 18 ; //20 ;
      COLOR nColor, METRO_WINDOW ;
      ACTION ( oDlgUI:End() ) FONT oFont1 // NOBORDER
                        //Self:lOkUI := .T.,
   @ (nHg/2) - 24, 2*Int( nWd/6) + 130 FLATBTN oBtn2 ;
      PROMPT "Cancelar" OF oDlgUI ;
      COLOR nColor, METRO_WINDOW ;
      SIZE 50, 18 ; //20 ;
      ACTION ( oDlgUI:End() ) FONT oFont1

  else
   @ ( nHg/2 ) - 24, 2*Int( nWd/6) + 50 BUTTON oBtn1 PROMPT "Aceptar" ;
      SIZE 50, 16 PIXEL OF oDlgUI ;
      FONT oFont2 ;
      ACTION ( Self:lOkUI := .T., iif( Self:lOkUI, oDlgUI:End(), .F. ) )
                                 //150
   @ ( nHg/2 ) - 24, 2*Int( nWd/6) + 130 BUTTON oBtn2 PROMPT "Cancelar" ;
      SIZE 50, 16 PIXEL OF oDlgUI ;
      FONT oFont2 ;
      ACTION ( oDlgUI:End() )

 endif

 ACTIVATE DIALOG oDlgUI CENTERED VALID ( oFont1:End(), oFont1 := Nil, ;
                                         oFont2:End(), oFont2 := Nil, .T. )

Return Self

//----------------------------------------------------------------------------//
Function nCuantas( cTxt, cCad )
Local nVeces  := 0
Local nLen    := Len( cTxt )
Local cString := ""
Local nPos    := 0
nPos  := At( cCad, cTxt )
if nPos > 0
   nVeces++
   if nPos < nLen
      cString := Right( cTxt, nLen - nPos )
      nLen    := Len( cString )
      Do While nLen > 0
         nPos  := At( cCad, cString )
         if nPos > 0
            nVeces++
            cString := Right( cTxt, nLen - nPos )
            nLen    := Len( cString )
         else
            exit
         endif
      Enddo
   endif
endif
Return nVeces

//----------------------------------------------------------------------------//

//----------------------------------------------------------------------------//

//----------------------------------------------------------------------------//

//----------------------------------------------------------------------------//

//----------------------------------------------------------------------------//

//----------------------------------------------------------------------------//

