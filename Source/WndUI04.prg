
/*
 *
 * Proyecto: FiveUI
 * Fichero:  WndUI04.prg
 * Descripci�n: Test Clase WindowsUI, y clases heredadas
 *              Definicion de Ventana:
 *              - Maximizar
 *              - Minimizar
 *              - Titulo ventana
 *              - Paneles
 * Autor: Cristobal Navarro Lopez (C)
 * Fecha: 03/10/2013
 *
 */


#include "Fivewin.ch"

#include "colores.ch"


Static   oWinUI
Static   oMainBar
Static   nRefresh          := 0

Function Main()
Local    lEsMdi

   // Sets generales---------------------------------------------------------
   SET EPOCH TO 1990               // Admite los a�os desde el 1990 en adelante
   SET CENTURY ON                  // 4 d�gitos a�o
   SET DELETED ON                  // Impedir ver registros marcados borrar
   //SetCancel( .F. )                // Inutiliza ALT + C para abortar programa
   //SetDialogEsc( .F. )             // Impide salir Di�logos con Escape
   SET DATE FORMAT "DD/MM/YYYY"
   SET DECIMALS TO 2
   XBrNumFormat( "E", .T.)

   SetResDebug( .T. )

   lEsMdi   := .F.

   // 1.- Definicion de Ventana
   //     La definicion de ventanas MDI est� ajust�ndose
   //                        (lMax , lMdi, nColor, nStyl, oWnd, bWnd )
   //
   //     Probar el ejemplo cambiando la variable lEsMdi

   oWinUI := TWindowsUI():New( .T., .T., lEsMdi,       ,      ,     ,      )

   // 2.- Definicion de Icono de Barra de Titulo y acciones de ventanas

   oWinUI:lDemoUI       := .F.
   //oWinUI:lBttIconUI    := .T.
   oWinUI:lBttExitUI    := .T.
   oWinUI:lBttMaxiUI    := .T.      // Si la ventana es lMax, lo desactiva
   oWinUI:lBttMiniUI    := .T.
   oWinUI:lBttNormUI    := .T.      // Si la ventana es lMax, lo desactiva

   oWinUI:aBttIconUI     := { ".\Res\IconUI22.bmp", ".\Res\IconUI221.bmp" }
   oWinUI:aFilMaxiUI     := { ".\Res\path70901n.bmp", ".\Res\path70902.bmp" }
   oWinUI:aFilMiniUI     := { ".\Res\path70911n.bmp", ".\Res\path70912.bmp" }
   oWinUI:aFilNormUI     := { ".\Res\g71112n.bmp", ".\Res\g71122.bmp" }
   oWinUI:aBttExitUI     := { ".\Res\g60645n.bmp", ".\Res\g60646.bmp", 48, 20}

   // 3.- Definicion de Paneles:
   //                  lWPnelUI:  con Boton, sin icono de Aplicacion
   //                  lPnelUI :  Panel sin boton

   oWinUI:lWPnelUI       := .T.
   //oWinUI:bWPnelUI       := { | oW | MiPnel(,,,,,) }
   oWinUI:cBmpPnel       := ".\Res\back24.bmp"

   oWinUI:bTitUI         := { | oW | ;
          oWinUI:SetTitleUI( "PANEL DE CONTROL" , -1, 0, 1, oWinUI:oWPnelUI, ;
                    { METRO_WINDOW, METRO_ACTIVEBORDER }, oWinUI:oFontApliUI) }

   oWinUI:ActivaUI()


   Hb_GCall(.t.)
   CLEAR MEMORY

   if File( "checkres.txt" )
      FErase( "checkres.txt" )
   endif
   CheckRes()

Return nil

//----------------------------------------------------------------------------//

//----------------------------------------------------------------------------//

//----------------------------------------------------------------------------//

//----------------------------------------------------------------------------//

//----------------------------------------------------------------------------//
