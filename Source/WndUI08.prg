
/*
 *
 * Proyecto: FiveUI
 * Fichero:  WndUI08.prg
 * Descripci�n: Test Clase WindowsUI, y clases heredadas
 *              Definicion de Ventana:
 *              - Maximizar
 *              - Minimizar
 *              - Titulo ventana
 *              - Paneles
 *              - Ribbon Bar
 * Autor: Cristobal Navarro Lopez (C)
 * Fecha: 03/10/2013
 *
 */


#include "Fivewin.ch"

#include "ribbon.ch"

#include "colores.ch"


Static   oWinUI
Static   oMainBar
Static   nRefresh          := 0

Function Main()
Local    lEsMdi

   // Sets generales---------------------------------------------------------
   SET EPOCH TO 1990               // Admite los a�os desde el 1990 en adelante
   SET CENTURY ON                  // 4 d�gitos a�o
   SET DELETED ON                  // Impedir ver registros marcados borrar
   //SetCancel( .F. )                // Inutiliza ALT + C para abortar programa
   //SetDialogEsc( .F. )             // Impide salir Di�logos con Escape
   SET DATE FORMAT "DD/MM/YYYY"
   SET DECIMALS TO 2
   XBrNumFormat( "E", .T.)

   SetResDebug( .T. )

   lEsMdi   := .T.

   // 1.- Definicion de Ventana
   //     La definicion de ventanas MDI est� ajust�ndose
   //                        (lMax , lMdi, nColor, nStyl, oWnd, bWnd )
   //
   //     Probar el ejemplo cambiando la variable lEsMdi

   oWinUI := TWindowsUI():New( .T., .T., lEsMdi,       ,      ,     ,      )

   // 2.- Definicion de Icono de Barra de Titulo y acciones de ventanas
   //     Si no existe lBttExit, se asigna al bAction del icono el :End()

   oWinUI:lDemoUI       := .F.
   oWinUI:lBttIconUI    := .T.
   oWinUI:aBttIconUI    := { ".\Res\IconUI22.bmp", ".\Res\IconUI221.bmp" }

   // Posibilidad de definir posicion del IconoBmp
   //oWinUI:nRowBttIcoUI  := 1
   //oWinUI:nColBttIcoUI  := 1

   // 5.- Definir RibbonBar

   oWinUI:lRbbUI      := .T.
   oWinUI:bRbbUI      := { | oW | MiRibbon( lEsMdi ) }

   oWinUI:ActivaUI()


   Hb_GCall(.t.)
   CLEAR MEMORY

   if File( "checkres.txt" )
      FErase( "checkres.txt" )
   endif
   CheckRes()

Return nil

//----------------------------------------------------------------------------//

Function MiRibbon( lChild )
Local   x
Local   oRBar
Local   oGrp       := {}
Local   oWClient   := oWinUI:oWndUI
Local   nWidth     := if( lChild, oWClient:oWndClient:nWidth - 3, 0 )
Local   nHeight    := if( lChild, Int( oWClient:oWndClient:nHeight / 6 ), 0 )
Local   oDlg                                  //:aWnd[1]
DEFAULT lChild     := .T.

   oDlg  := if( lChild, oWClient:oWndClient, oWClient )

   if !lChild
      nWidth     := oWClient:nWidth - 3
      nHeight    := Int( oWClient:nHeight / 6 ) + 20
   endif

   oWinUI:lRbbUI := .T.
   oRBar := TRibbonBar():New( oDlg,;
                              { "VISUAL MAKE","EDITOR","UTILIDADES" },;
                              ,, nWidth , nHeight ,,,,,,,,,,,.F.,,.T.,)
//                              ,,  , ,,,,,,,,,,,.F.,,.T.,)
   WITH OBJECT oRBar

        :nTop             = 26
        :nLeft            = 1
        :nHeightFld       = 24
        //:nBottom          = nHeight
        //:nRight           = nWidth + :nLeft
        :nWidth           = nWidth // ( :nLeft * 10 )
        //:nHeight          = nHeight

        //:lFillFld         = .F.

   ENDWITH

   oRBar:CalcPos()


   ASize( oGrp, 0 )

   For  x = 1 to Len( oRBar:aPrompts )
        AAdd( oGrp, nil )

        WndWidth(  oRBar:aDialogs[x]:hWnd, ;
                  IF(oWinUI:lMax, GetSysMetrics(0)-1, oWinUI:oWndUI:nWidth ) - 4 )

        @ 0 , 0 RBGROUP oGrp[1] OF oRBar:aDialogs[x] ;
                PROMPT oRBar:aPrompts[x] ;
                SIZE oRBar:aDialogs[x]:nWidth-1 , oRBar:aDialogs[x]:nHeight-1 ; //  FONT oFontFld ; //Edt  ;
                LINECOLORS CLR_WHITE , CLR_WHITE  ;
                GRADIANT { { 1, CLR_WHITE, CLR_WHITE } ,;
                           { 0, RGB( 100, 100, 100 ), RGB( 100, 100, 100 ) } } ;
                CAPTIONGRAD { { 1, CLR_WHITE, CLR_WHITE } ,;
                              {   0, RGB( 100, 100, 100 ), RGB( 100, 100, 100 ) } } ;
                TEXTCOLOR METRO_APPWORKSPACE

         //oRBar:aClrTabTxt[ x ]  := {|| METRO_APPWORKSPACE }
   Next x

   oRBar:CalcPos()

   SetParent( oRBar:hWnd, oDlg:hWnd )

   if oWinUI:lBttIconUI .and. !lChild
      if oWinUI:nRowBttIcoUI >= oRBar:nTop
         SetParent( oWinUI:oBtnIconUI:hWnd, oRBar:hWnd )
      endif
   endif

Return oRBar

//----------------------------------------------------------------------------//


//----------------------------------------------------------------------------//

//----------------------------------------------------------------------------//

//----------------------------------------------------------------------------//

//----------------------------------------------------------------------------//
