
#include "FiveWin.ch"

//----------------------------------------------------------------------------//

CLASS TCMru FROM TMru

   METHOD New( cIniFile, cSection, nMaxItems, cMsg, bAction,;
               oMenu, nMenuPos, uBmpFile, uResName, nSp ) CONSTRUCTOR

ENDCLASS

//----------------------------------------------------------------------------//

METHOD New( cIniFile, cSection, nMaxItems, cMsg, bAction, oMenu, nMenuPos, ;
            uBmpFile, uResName, nSp ) CLASS TCMru

   local oIni, n, cValue, oItem, lExist := .f.
   local cCad
   local cBmp

   DEFAULT nMaxItems := 10, cMsg := "Open this file",;
           oMenu := LastMenu(), nMenuPos  := LastItemPos(), ;
           uBmpFile := "", uResName := "", nSp := 0

   ::aItems     = {}
   ::nMaxItems  = nMaxItems
   ::cIniFile   = cIniFile
   ::cSection   = cSection
   ::bAction    = bAction
   ::oMenu      = oMenu
   ::nMenuPos   = nMenuPos
   ::cMsg       = cMsg

   cCad         = Space( nSp )

   INI oIni FILENAME cIniFile

      for n = 1 to nMaxItems

         GET cValue SECTION cSection ENTRY AllTrim( Str( n ) ) OF oIni ;
            DEFAULT ""

         if ! Empty( cValue )

            if ! lExist
               lExist = .t.
               if Len( oMenu:aItems ) > 0
                  SEPARATOR ::oSeparator
               endif
            endif
            if bAction != nil
               if !empty( uBmpFile )
                  cBmp  := uBmpFile
                  if Valtype( uBmpFile ) <> "C"
                    if Valtype( uBmpFile ) = "A"
                       if Len( uBmpFile ) >= n
                          cBmp  := uBmpFile[ n ]
                       else
                          cBmp  := uBmpFile[ 1 ]
                       endif
                    else    // CodeBlock
                       cBmp  := Eval( uBmpFile, n, cValue, oMenu )
                    endif
                  endif
                  MENUITEM oItem ;
                     PROMPT cCad+"&" + Str( n, If( n < 10, 1, 2 ) ) + " " + ;
                     cValue MESSAGE cMsg ;
                     BLOCK { | oItem | Eval( bAction, ;
                                       NoNumber( oItem:cPrompt ), oItem ) } ;
                     FILE cBmp
               else
                  if !empty( uResName )
                     cBmp  := uResName
                     if Valtype( uResName ) <> "C"
                        if Valtype( uResName ) = "A"
                           if Len( uResName ) >= n
                              cBmp  := uResName[ n ]
                           else
                              cBmp  := uResName[ 1 ]
                           endif
                        else    // CodeBlock
                           cBmp  := Eval( uResName, n, cValue, oMenu )
                        endif
                     endif
                     MENUITEM oItem ;
                        PROMPT cCad+"&" + Str( n, If( n < 10, 1, 2 ) ) + " " + ;
                        cValue MESSAGE cMsg ;
                        BLOCK { | oItem | Eval( bAction, ;
                                       NoNumber( oItem:cPrompt ), oItem ) } ;
                        RESNAME cBmp
                  else
                  MENUITEM oItem ;
                     PROMPT cCad+"&" + Str( n, If( n < 10, 1, 2 ) ) + " " + ;
                     cValue MESSAGE cMsg ;
                     BLOCK { | oItem | Eval( bAction, ;
                                       NoNumber( oItem:cPrompt ), oItem ) }
                  endif
               endif
            else
               if !empty( uBmpFile )
                  cBmp  := uBmpFile
                  if Valtype( uBmpFile ) <> "C"
                     if Valtype( uBmpFile ) = "A"
                       if Len( uBmpFile ) >= n
                          cBmp  := uBmpFile[ n ]
                       else
                          cBmp  := uBmpFile[ 1 ]
                       endif
                     else    // CodeBlock
                       cBmp  := Eval( uBmpFile, n, cValue, oMenu )
                     endif
                  endif
                  MENUITEM oItem ;
                  PROMPT cCad+"&" + Str( n, If( n < 10, 1, 2 ) ) + " " + ;
                  cValue MESSAGE cMsg ;
                  FILE cBmp
               else
                  if !empty( uResName )
                     cBmp  := uResName
                     if Valtype( uResName ) <> "C"
                        if Valtype( uResName ) = "A"
                           if Len( uResName ) >= n
                              cBmp  := uResName[ n ]
                           else
                              cBmp  := uResName[ 1 ]
                           endif
                        else    // CodeBlock
                           cBmp  := Eval( uResName, n, cValue, oMenu )
                        endif
                     endif
                     MENUITEM oItem ;
                        PROMPT cCad+"&" + Str( n, If( n < 10, 1, 2 ) ) + " " + ;
                        cValue MESSAGE cMsg ;
                        RESNAME cBmp
                  else
                     MENUITEM oItem ;
                     PROMPT cCad+"&" + Str( n, If( n < 10, 1, 2 ) ) + " " + ;
                     cValue MESSAGE cMsg
                  endif
               endif
            endif

            AAdd( ::aItems, oItem )
         endif

      next
   ENDINI

return Self

//----------------------------------------------------------------------------//

static function NoNumber( cPrompt )

return SubStr( cPrompt, At( " ", cPrompt ) + 1 )

//----------------------------------------------------------------------------//
