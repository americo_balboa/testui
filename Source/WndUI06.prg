
/*
 *
 * Proyecto: FiveUI
 * Fichero:  WndUI06.prg
 * Descripci�n: Test Clase WindowsUI, y clases heredadas
 *              Definicion de Ventana:
 *              - Maximizar
 *              - Minimizar
 *              - Titulo ventana
 *              - Paneles
 * Autor: Cristobal Navarro Lopez (C)
 * Fecha: 03/10/2013
 *
 */


#include "Fivewin.ch"

#include "colores.ch"


Static   oWinUI
Static   oMainBar
Static   nRefresh          := 0

Function Main()
Local    lEsMdi

   // Sets generales---------------------------------------------------------
   SET EPOCH TO 1990               // Admite los a�os desde el 1990 en adelante
   SET CENTURY ON                  // 4 d�gitos a�o
   SET DELETED ON                  // Impedir ver registros marcados borrar
   //SetCancel( .F. )                // Inutiliza ALT + C para abortar programa
   //SetDialogEsc( .F. )             // Impide salir Di�logos con Escape
   SET DATE FORMAT "DD/MM/YYYY"
   SET DECIMALS TO 2
   XBrNumFormat( "E", .T.)

   SetResDebug( .T. )

   lEsMdi   := .T.

   // 1.- Definicion de Ventana
   //     La definicion de ventanas MDI est� ajust�ndose
   //                        (lMax , lMdi, nColor, nStyl, oWnd, bWnd )
   //
   //     Probar el ejemplo cambiando la variable lEsMdi

   oWinUI := TWindowsUI():New( .T., .T., lEsMdi,       ,      ,     ,      )

   // 2.- Definicion de Icono de Barra de Titulo y acciones de ventanas

   oWinUI:lDemoUI       := .F.
   oWinUI:lBttIconUI    := .F.

   // 3.- Definicion de Paneles:
   //                  lWPnelUI:  con Boton, sin icono de Aplicacion
   //                  lPnelUI :  Panel sin boton

   oWinUI:lWPnelUI       := .T.
   //oWinUI:bWPnelUI       := { | oW | MiPnel(,,,,,) }
   oWinUI:cBmpPnel       := ".\Res\back321.bmp"

   oWinUI:bTitUI         := { | oW | ;
          oWinUI:SetTitleUI( "Panel de Control" , -1, 0, 1, oWinUI:oWPnelUI, ;
                    { METRO_WINDOW, METRO_APPWORKSPACE }, oWinUI:oFontApliUI ) }

   oWinUI:ActivaUI()


   Hb_GCall(.t.)
   CLEAR MEMORY

   if File( "checkres.txt" )
      FErase( "checkres.txt" )
   endif
   CheckRes()

Return nil

//----------------------------------------------------------------------------//

//----------------------------------------------------------------------------//

//----------------------------------------------------------------------------//

//----------------------------------------------------------------------------//

//----------------------------------------------------------------------------//
