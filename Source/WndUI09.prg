
/*
 *
 * Proyecto: FiveUI
 * Fichero:  WndUI09.prg
 * Descripci�n: Test Clase WindowsUI, y clases heredadas
 *              Definicion de Ventana:
 *              - Maximizar
 *              - Minimizar
 *              - Titulo ventana
 *              - Paneles
 *              - Ribbon Bar y ejemplos
 * Autor: Cristobal Navarro Lopez (C)
 * Fecha: 03/10/2013
 *
 */


#include "Fivewin.ch"

#include "ribbon.ch"

#include "colores.ch"


Static   oWinUI
Static   oMainBar
Static   nRefresh          := 0

Function Main()
Local    lEsMdi

   // Sets generales---------------------------------------------------------
   SET EPOCH TO 1990               // Admite los a�os desde el 1990 en adelante
   SET CENTURY ON                  // 4 d�gitos a�o
   SET DELETED ON                  // Impedir ver registros marcados borrar
   //SetCancel( .F. )                // Inutiliza ALT + C para abortar programa
   //SetDialogEsc( .F. )             // Impide salir Di�logos con Escape
   SET DATE FORMAT "DD/MM/YYYY"
   SET DECIMALS TO 2
   XBrNumFormat( "E", .T.)

   SetResDebug( .T. )

   lEsMdi   := .T.

   // 1.- Definicion de Ventana
   //     La definicion de ventanas MDI est� ajust�ndose
   //                        (lMax , lMdi, nColor, nStyl, oWnd, bWnd )
   //
   //     Probar el ejemplo cambiando la variable lEsMdi

   oWinUI := TWindowsUI():New( .T., .T., lEsMdi,       ,      ,     ,      )

   // 2.- Definicion de Icono de Barra de Titulo y acciones de ventanas
   //     Si no existe lBttExit, se asigna al bAction del icono el :End()

   oWinUI:lDemoUI       := .F.
   oWinUI:lBttIconUI    := .T.
   oWinUI:aBttIconUI     := { ".\Res\IconUI22.bmp", ".\Res\IconUI221.bmp" }

   // Posibilidad de definir posicion del IconoBmp
   //oWinUI:nRowBttIcoUI  := 1
   //oWinUI:nColBttIcoUI  := 1

    // 5.- Definir RibbonBar

   oWinUI:lRbbUI      := .T.
   oWinUI:bRbbUI      := { | oW | MiRibbon() }

   // 6.- Definir XBrowse
   // Probar haciendo click en un fichero y mover la rueda del mouse
   oWinUI:bXbrwUI         := { | oW | UIXBrowse() }


   oWinUI:bXMnuUI         := { | oW | UIXMnu() }


   oWinUI:ActivaUI()


   Hb_GCall(.t.)
   CLEAR MEMORY

   if File( "checkres.txt" )
      FErase( "checkres.txt" )
   endif
   CheckRes()

Return nil

//----------------------------------------------------------------------------//

Function MiRibbon()
Local   x
Local   oGrp          := {}
Local   aOBttBar      := {}
Local   oBackStage
Local   oBtnBack
Local   aGradBack
Local nFBtt       := 4
Local nB          := 82

Local oRBar
Local nSz1        := 82
Local nSz2        := 60

Local   cPrompt       := ""
Local   oDlg
Local   bSalir        := { || oWinUI:End() }

Local nRowUI      := 0 //26
Local nColUI      := IF( oWinUI:lWPnelUI, oWinUI:oWPnelUI:nWidth+1, 1 ) //300
Local aPromptsUI  := { "PROGRAMAS","EDITOR","UTILIDADES" }
//Local oUI         := if( oWinUI:lMdiUI, oWinUI:oWndUI:oWndClient:aWnd[ 1 ], oWinUI:oWndUI:oClient )
Local oUI         := if( oWinUI:lMdiUI, oWinUI:oWndUI:oWndClient, oWinUI:oWndUI)
Local nWidthUI    := GetSysMetrics(0) - ( nColUI + 2 )//GetSysMetrics(0) //oUI:nWidth
Local nHeightUI   := Int( GetSysMetrics(1) / 6 ) - 8   // oUI:nHeight

   if oWinUI:lPnelUI
      nWidthUI   := oUI:nWidth - 4 - oWinUI:oPnelUI:nWidth
   endif

   oWinUI:lRbbUI := .T.
   oRBar := TRibbonBar():New( oUI, aPromptsUI,;
                              ,,nWidthUI ,nHeightUI ,,,,,,,,,,,.F.,,.T.,)

   WITH OBJECT oRBar
        //:oFont         = oWinUI:oFontTitleUI

        :nClrPaneRB       = METRO_WINDOW
        :nClrBoxOut       = METRO_WINDOW
        :nClrBoxIn        = METRO_ACTIVEBORDER     //APPWORKSPACE
        :nClrBoxSelOut    = METRO_WINDOW
        :nClrBoxSelIn     = METRO_ACTIVEBORDER     //APPWORKSPACE

        :nTop             = nRowUI
        :nLeft            = nColUI

        //:lFillFld         = .F.

        :nLeftMargin      = 110     //110
        //:nHeightFld       = 24

        :nTopMargin       = :nHeightFld + 2 //Margen desde punto superior
        //:nType            = 4

   ENDWITH

   oRBar:CalcPos()

//--------------
  /*
   aGradBack             = { {  0, METRO_ACTIVEBORDER, METRO_ACTIVEBORDER },;
                             {100, METRO_WINDOW, METRO_WINDOW } }

                                      //"./../bitmaps/rbnmenu.bmp"
   oBtnBack = TRBtn():New( 0, 2, 84, 22, ".\Res\g483n1.bmp", { || oRBar:Backstage() }, oRBar,;
        ,,,,,, .T., .F.,,,,,, "SAYBUTTON" , oBackStage,,aGradBack,,,,,,,,,,,;
        aGradBack , METRO_WINDOW, METRO_WINDOW ) //ACTIVEBORDER )
        //CargaImgBtt( ".\Res\g483n1.png" ,  , oBtnBack )

   oBtnBack:nRound   := 0
   oBtnBack:aClrGradOver  := aGradBack
   oBtnBack:aClrGradUnder := aGradBack

                                          //oRBar
   oBackStage := TBackStage():New( 26, , , ,  , 106 )

   oBackStage:AddOption( "Test1", , ,;
                  1, , , , ,;
                  , , , ;
                  , { || MsgInfo("Test 1") } )

   oBackStage:AddOption( "Test2", , ,;
                  1, , , , ,;
                  , , , ;
                  , { || MsgInfo("Test 2") } )

   oRBar:SetBackStage( oBackStage )
 */

//------------------------

   ASize( oGrp, 0 )
   ASize( aOBttBar , 0 )
   For  x = 1 to Len( oRBar:aPrompts )    //APPWORKSPACE
        oRBar:aClrTabTxt[x]  := {|| {{METRO_ACTIVEBORDER, METRO_WINDOW},;
                                           {METRO_AZUL8, METRO_WINDOW},;
                                           {METRO_AZUL8, METRO_WINDOW},;
                                           {METRO_AZUL8, METRO_WINDOW}} }
        AAdd( oGrp, Nil )
        AAdd( aOBttBar , {} )
        //WndWidth(  oRBar:aDialogs[x]:hWnd, ;
        //           IF(oWinUI:lMax, GetSysMetrics(0), oWinUI:oWndUI:nWidth ) - 4 - nColUI - 2 )
        oDlg       := oRBar:aDialogs[x]
        nHeightUI  := oRBar:aDialogs[x]:nHeight-1
        nWidthUI   := oRBar:aDialogs[x]:nWidth-1
        cPrompt    := "EXPLORADOR DE ARCHIVOS"  //Upper( oRBar:aPrompts[x] )
        oGrp[x] := TRBGroup():New( oDlg, 0, 0, nHeightUI, nWidthUI, , ;
                                   cPrompt,, METRO_WINDOW, METRO_WINDOW, ;
                                   { { 1, METRO_WINDOW, METRO_WINDOW } , ;
                                     { 0, RGB( 100, 100, 100 ), ;
                                          RGB( 100, 100, 100 ) } }, ;
                                   , { { 1, METRO_WINDOW, METRO_WINDOW } , ;
                                     { 0, RGB( 100, 100, 100 ), ;
                                          RGB( 100, 100, 100 ) } }, ;
                                   , , ,,,, METRO_ACTIVEBORDER ) //APPWORKSPACE )
        oGrp[x]:bAction := { || MsgInfo( "Action Grupo - 1" ) }
   Next x

   For x = 1 to 1 //Len( oGrp )

   AAdd( aOBttBar[x] , Nil )

   @ nFBtt, 2+nB*(x-1) ADD BUTTON aOBttBar[x][1] PROMPT "Salir" ;
          GROUP oGrp[x] ;
          SAYBUTTON ;
          SIZE nSz1, nSz2 TOP ;
          BITMAP ".\Res\g821n1.bmp" ;
          LINECOLORS METRO_WINDOW, METRO_WINDOW ;
          TOOLTIP "TOOLTIP BOTON 1"
          aOBttBar[x][1]:bAction := bSalir
          //aOBttBar[x][1]:oFont := oFontFld
          //CargaImgBtt( ".\Res\g821n1.png" ,  , aOBttBar[x][1] )

   AAdd( aOBttBar[x] , Nil )

   @ nFBtt, 2+nB*(x) ADD BUTTON aOBttBar[x][2] PROMPT "Boton 2" ;
          GROUP oGrp[x] ;
          SAYBUTTON ;
          ACTION MsgInfo("Boton - 2","Atencion") ; //CANCEL
          SIZE nSz1, nSz2 TOP ;
          BITMAP ".\Res\g679n1.bmp" ;
          LINECOLORS METRO_WINDOW, METRO_WINDOW ;
          TOOLTIP "TOOLTIP BOTON 2"
          //aOBttBar[x][1]:oFont := oFontFld
          //CargaImgBtt( ".\Res\g679n1.png" ,  , aOBttBar[x][2] )

   AAdd( aOBttBar[x] , Nil )

   @ nFBtt, 2+nB*(x)*2 ADD BUTTON aOBttBar[x][3] PROMPT "Boton 3" ;
          GROUP oGrp[x] ;
          SAYBUTTON ;
          ACTION MsgInfo("Boton - 3","Atencion") ; //CANCEL
          SIZE nSz1, nSz2 TOP ;
          BITMAP ".\Res\g3749n1.bmp" ;
          LINECOLORS METRO_WINDOW, METRO_WINDOW ;
          TOOLTIP "TOOLTIP BOTON 3"
          //aOBttBar[x][1]:oFont := oFontFld
          //CargaImgBtt( ".\Res\g3749n1.png" ,  , aOBttBar[x][3] )

   AAdd( aOBttBar[x] , Nil )

   @ nFBtt, 2+nB*(x)*3 ADD BUTTON aOBttBar[x][4] PROMPT "Boton 4" ;
          GROUP oGrp[x] ;
          SAYBUTTON ;
          ACTION MsgInfo("Boton - 4","Atencion") ; //CANCEL
          SIZE nSz1, nSz2 TOP ;
          BITMAP ".\Res\g653.bmp" ;
          LINECOLORS METRO_WINDOW, METRO_WINDOW ;
          TOOLTIP "TOOLTIP BOTON 4"
          //aOBttBar[x][1]:oFont := oFontFld
          //CargaImgBtt( ".\Res\g653.png" ,  , aOBttBar[x][4] )

   Next x

   SetParent( oRBar:hWnd, if( oWinUI:lMdiUI, oUI:hWnd, oWinUI:oWndUI:hWnd ) )

   if oWinUI:lBttIconUI //.and. !oWinUI:lMdiUI
      if oWinUI:nRowBttIcoUI >= oRBar:nTop
         SetParent( oWinUI:oBtnIconUI:hWnd, oRBar:hWnd )
      endif
   endif

Return oRBar

//----------------------------------------------------------------------------//

Function UIXBrowse()
Local oXBrw
Local aT      := {}
Local aN      := {}
Local aB      := {}
Local aF      := {}
Local aH      := {}
Local aG      := {}

Local aN1     := {}
Local aB1     := {}
Local aF1     := {}
Local aH1     := {}
Local aG1     := {}

Local oFld
Local nDlg
Local aTabla
Local nLen
Local nLen1
Local x
Local oWClient      := oWinUI:oWndUI  //:oWndClient
Local nWidth        := oWClient:oWndClient:nWidth - 4
Local nHeight       := oWClient:oWndClient:nHeight - 22  //28
Local oDlg
Local oBrush1
Local oFont1
Local y             := 0
Local nBmp        := 3
Local aBitmaps    := {}
Local aBmpPal
Local aBmps1      := { ;
                       ".\Res\Bmps\new.bmp",;
                       ".\Res\Bmps\addprgg1.bmp",;
                       ".\Res\Bmps\M_AGREGADIR.BMP",;
                       ".\Res\Bmps\M_AGREGAUNO.BMP",;
                       ".\Res\Bmps\open.bmp",;
                       ".\Res\Bmps\M_ARBOL.BMP",;
                       ".\Res\Bmps\M_COLOR.BMP",;
                       ".\Res\Bmps\M_EXPLORA.BMP",;
                       ".\Res\Bmps\Run.bmp",;
                       ".\Res\Bmps\Copy.bmp" ;
                     }

   For x = 1 to Len( aBmps1 )
       if !empty( aBmps1[x] )
          AAdd( aBitmaps, Array(6) )             // hBmp := ReadBitmap( , file )
          aBmpPal   := PalBmpRead( , aBmps1[x] ) //PalBmpLoad( aBmps1[x] )
          aBitmaps[ Len(aBitmaps), 1 ]   :=  aBmpPal[ 1 ]
          aBitmaps[ Len(aBitmaps), 2 ]   :=  aBmpPal[ 2 ]
          if x = 1
          aBitmaps[ Len(aBitmaps), 3 ]   :=  16   //20
          aBitmaps[ Len(aBitmaps), 4 ]   :=  16   //20
          else
          aBitmaps[ Len(aBitmaps), 3 ]   :=  16
          aBitmaps[ Len(aBitmaps), 4 ]   :=  16
          endif
          aBitmaps[ Len(aBitmaps), 5 ]   :=  0
          aBitmaps[ Len(aBitmaps), 6 ]   :=  .F.
          aBmpPal   := Nil
       endif
   Next x

   nLen1  := 0

   nLen  := ADIR(".\*.*")
   ASize( aN , nLen )
   ASize( aB , nLen )
   ASize( aF , nLen )
   ASize( aH , nLen )
   ASize( aG , nLen )

   ADIR( ".\*.*", aN, aB, aF, aH, aG )

   For x = nLen1+1 to nLen+nLen1
       AAdd( aT , {,,,,} )
       aT[x][1]  := aN[x-nLen1]
       if aB[x-nLen1] > 0                     //   2
          aT[x][2]  := Str( Round( aB[x-nLen1]/1000 , 0 ) )+" KB"
       else
          aT[x][2]  := " " //" 0 KB"
       endif
       aT[x][3]  := aF[x-nLen1]
       aT[x][4]  := aH[x-nLen1]
       aT[x][5]  := aG[x-nLen1]
   Next x


   //DEFINE BRUSH oBrush1 COLOR CLR_WHITE
   DEFINE FONT oFont1 NAME "Segoe UI LIGHT" SIZE 0, -12
   oDlg  := oWClient:oWndClient:aWnd[1]
            //TXCBrowse
   oXBrw := TXBrowse():New( oDlg )

   WITH OBJECT oXBrw

   //:oBrush          := oBrush1

   :nStyle          -= WS_BORDER
   :lTransparent   := .F.

   :cCaption        := ""

   :l2007             := .f.

   :nTop              := IF( oWinUI:lRbbUI, oWinUI:oRbbUI:nHeight+oWinUI:oRbbUI:nTop,  1 )
   :nLeft             := 1 + Int( nWidth/5 )
   :nBottom           := nHeight
   :nRight            := nWidth

   :lDesign           := .f.
   :SetFont( oFont1 )

   :lHeader           := .T.

   :nHeaderHeight     :=  24
                         // Azul Cobalto  Rgb(  0, 71, 171)
                         // Marron claro titulo ventanas W8 { 209, 168, 129 }
   :bClrHeader        := {|| { Rgb( 255, 255, 255), Rgb(  0, 25, 64 )} }

   :lFooter           := .F.    //.T.
   :nFooterHeight     :=  0     //14

   // DATAs de XCBrowse
   if Upper( oXBrw:ClassName() ) == "TXCBROWSE"

     :lHeadSelec        := .F.
     :lLinBorder        := .f.
     :lLinDataSep       := .f.
     :lLinHeadVSep      := .f.
     :lLinHeadHSep      := .f.
     :lLinFootVSep      := .f.
     :lLinFootHSep      := .f.
     :lHeadBtton        := .F.  //.F.
     :nWRecordSel       :=  24
     :lNoEmpty          := .F.

   endif

   :lRecordSelector   := .F.
   :bClrHeader        := { || { METRO_WINDOW, METRO_GRIS4 } }
   :nRowDividerStyle  := LINESTYLE_NOLINES        //4
   :nColDividerStyle  := LINESTYLE_NOLINES        //4

   :nRecSelColor      := METRO_APPWORKSPACE   //Rgb( 202, 162, 126 )

   //:nDataLines        :=  1

   :lAllowColSwapping := .f.
   :lVScroll          := .T.
   :lHScroll          := .f.
   :nRowHeight        :=  24
   :nFreeze           :=  1
   :nMarqueeStyle     :=  MARQSTYLE_HIGHLWIN7 //MARQSTYLE_HIGHLROW //CELL  //LROWMS // 3     // 5

   //:nSpRecordSel      :=  0

   :bClrSel           := { || { METRO_APPWORKSPACE, Rgb( 210, 210, 204 ) } }
   :bClrStd           := { || { METRO_APPWORKSPACE, CLR_WHITE } }
   :bClrSelFocus      := { || { CLR_WHITE, Rgb( 153, 180, 209 ) } }
   //:bClrRowFocus      := { || { METRO_APPWORKSPACE , CLR_WHITE } }


   :lAutoSort         := .F.
   :SetArray( aT )

   :aCols[1]:nWidth         := Int( :nWidth/5 ) + 112
   :aCols[1]:aBitmaps       := aBitmaps
   :aCols[1]:bBmpData       := {| oB | IF( ( oXBrw:nArrayAt % 10  = 0 ), 1 , (oXBrw:nArrayAt % 10)+1 ) }
   :aCols[1]:cHeader        := "Nombre"
   :aCols[1]:nDataStrAlign  := 0
   :aCols[1]:bClrHeader     := { || { METRO_APPWORKSPACE, CLR_WHITE } }
   :aCols[1]:nHeadBmpNo     := 1      // 0 -> Quita icono cabecera
   //:aCols[1]:cOrder         := "A"

   /*
   :aCols[1]:lSetBmpCol     := .F.   // Poner a .t. para poner el icono de la columna 1 en la del Selector
   if :aCols[1]:lSetBmpCol
      :aCols[1]:nHeadStrAlign  := 2
   else
      :aCols[1]:nHeadStrAlign  := 2
   endif
   */

   :aCols[1]:lAllowSizing   := .f.
   :aCols[1]:nEditType       := 0

   :aCols[2]:nWidth         := Int( :nWidth/5 )-60
   //:aCols[2]:aBitmaps       := aBitmaps
   //:aCols[2]:bBmpData       := {|| oxBrw:nArrayAt + 1 }
   :aCols[2]:cHeader        := "Tama�o"
   :aCols[2]:bClrHeader  := { || { METRO_APPWORKSPACE, CLR_WHITE } }
   :aCols[2]:nDataStrAlign  := 1
   //:aCols[2]:nHeadBmpNo     := 1
   /*
   :aCols[2]:lSetBmpCol     := .f.   // Poner a .t. para poner el icono de la columna 1 en la del Selector
   if :aCols[2]:lSetBmpCol
      :aCols[2]:nHeadStrAlign  := 2
   else
      :aCols[2]:nHeadStrAlign  := 2
   endif
   */
   :aCols[2]:lAllowSizing   := .f.
   :aCols[2]:nEditType       := 0

   :aCols[3]:nWidth         := Int( :nWidth/5 )-4
                                 //oBrw:nRight - if( oBrw:lRecordSelector , Max( oBrw:nWRecordSel, 24 ), 0 )-if( oBrw:lVScroll, 10, 0 )
   //:aCols[3]:aBitmaps       := aBitmaps
   //:aCols[3]:bBmpData       := {|| oxBrw:nArrayAt + 1 }
   :aCols[3]:cHeader        := "Fecha"
   :aCols[3]:bClrHeader  := { || { METRO_APPWORKSPACE, CLR_WHITE } }
   :aCols[3]:nDataStrAlign  := 0
   //:aCols[3]:nHeadBmpNo     := 1
   /*
   :aCols[3]:lSetBmpCol     := .f.   // Poner a .t. para poner el icono de la columna 1 en la del Selector
   if :aCols[3]:lSetBmpCol
      :aCols[3]:nHeadStrAlign  := 2
   else
      :aCols[3]:nHeadStrAlign  := 2
   endif
   */
   :aCols[3]:lAllowSizing   := .f.
   :aCols[3]:nEditType       := 0

   :aCols[4]:nWidth         := Int( :nWidth/5 )-4
   //:aCols[1]:aBitmaps       := aBitmaps
   //:aCols[1]:bBmpData       := {|| oxBrw:nArrayAt + 1 }
   :aCols[4]:cHeader        := "Hora"
   :aCols[4]:bClrHeader  := { || { METRO_APPWORKSPACE, CLR_WHITE } }
   :aCols[4]:nDataStrAlign  := 0
   //:aCols[4]:nHeadBmpNo     := 1
   /*
   :aCols[4]:lSetBmpCol     := .f.   // Poner a .t. para poner el icono de la columna 1 en la del Selector
   if :aCols[4]:lSetBmpCol
      :aCols[4]:nHeadStrAlign  := 2
   else
      :aCols[4]:nHeadStrAlign  := 2
   endif
   */
   :aCols[4]:lAllowSizing   := .f.
   :aCols[4]:nEditType       := 0

   :aCols[5]:nWidth         := Int( :nWidth/5 ) - 70   // 72 - // Si tiene borde
   //:aCols[5]:aBitmaps       := aBitmaps
   //:aCols[5]:bBmpData       := {|| oxBrw:nArrayAt + 1 }
   :aCols[5]:cHeader        := "Tipo"
   :aCols[5]:bClrHeader  := { || { METRO_APPWORKSPACE, CLR_WHITE } }
   :aCols[5]:nDataStrAlign  := 2
   //:aCols[5]:nHeadBmpNo     := 1
   /*
   :aCols[5]:lSetBmpCol     := .f.   // Poner a .t. para poner el icono de la columna 1 en la del Selector
   if :aCols[5]:lSetBmpCol
      :aCols[5]:nHeadStrAlign  := 2
   else
      :aCols[5]:nHeadStrAlign  := 0
   endif
   */
   :aCols[5]:lAllowSizing   := .f.
   :aCols[5]:nEditType       := 0

   :CreateFromCode()
   aBitmaps := Nil

   END

   //oDlg:SetControl( oXBrw )
   //oDlg:AddControl( oXBrw )
   //while oFont1:nCount > 0
         oFont1:End()
   //end
   oFont1   := Nil
   oBrush1  := Nil

Return oXBrw

//----------------------------------------------------------------------------//

Function UIXMnu()
Local oXBrw
Local aT      := {}
Local aN      := {}
Local aB      := {}
Local aF      := {}
Local aH      := {}
Local aG      := {}

Local aN1     := {}
Local aB1     := {}
Local aF1     := {}
Local aH1     := {}
Local aG1     := {}

Local oFld
Local nDlg
Local aTabla
Local nLen
Local nLen1
Local x
Local oWClient      := oWinUI:oWndUI  //:oWndClient
Local nWidth        //:= oWClient:oWndClient:nWidth - 4
Local nHeight       := oWClient:oWndClient:nHeight - 24
Local oDlg
Local oBrush1
Local oBrush2
Local oFont1
Local lSw           := .T.
Local y             := 0
Local aData       := {{"      Aplicacion "},;
                      {"      Ficheros   "},; // {"Seleccionar Usuario 8066"},;
                      {"      Carpetas   "},;
                      {"      Abrir      "},;
                      {"      Nuevos     "},;
                      {"      Utilidades "},;
                      {"      Copiar     "},;
                      {"      Documentos "},;
                      {"      Utiles     "},;
                      {"      Listados   "},;
                      {"      Setup      " }}
Local aTipos      := { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 }

if oWinUI:lWPnelUI
   oDlg  := oWinUI:oWPnelUI   //       oWClient:oWndClient:aWnd[1]
else
   oDlg  :=  oWClient:oWndClient:aWnd[1]
endif
   //? oWinUI:oWPnelUI
   DEFINE BRUSH oBrush1 COLOR METRO_ACTIVEBORDER
   DEFINE BRUSH oBrush2 COLOR METRO_WINDOW
   DEFINE FONT oFont1 NAME "Segoe UI Light" SIZE 0, -24

   oXBrw := TXCBrowse():New( oDlg )

   WITH OBJECT oXBrw

   //:oBrush          := oBrush1

   :nStyle          -= WS_BORDER
   :lTransparent   := .F.

   :cCaption        := ""    //"Prueba de Browse"

   :l2007             := .F.
   //:l2010    := .t.      // No implementado -- 18/02/2013 -- FWh - 31/03/2012

   :nTop              := IF( oWinUI:lRbbUI, oWinUI:oRbbUI:nHeight+oWinUI:oRbbUI:nTop,  1 )
   //:nTop              := 132 //IF( oWinUI:lRbbUI, oWinUI:oRbbUI:nHeight+1,  1 )
   :nLeft             := 2
   :nBottom           := nHeight    //736
   :nRight            := 268   //nWidth

   :lDesign           := .f.
   //:oFont             := oFont1
   :SetFont( oFont1 )

   :lHeader           := .F.
   :lHeadSelec        := .F.   //.t.
   :nHeaderHeight     :=  0
   //:bClrHeader        := {|| { Rgb( 255, 255, 255), Rgb(  0, 25, 64 )} }

   :lFooter           := .F.    //.T.
   :nFooterHeight     :=  0     //14

   // DATAs de XCBrowse
   if Upper( oXBrw:ClassName() ) == "TXCBROWSE"

   :lLinBorder        := .f.
   :lLinDataSep       := .f.
   :lLinHeadVSep      := .f.
   :lLinHeadHSep      := .f.
   :lLinFootVSep      := .f.
   :lLinFootHSep      := .f.
   :lHeadBtton        := .T.  //.F.
   :nWRecordSel       :=  24

   endif

   :lRecordSelector   := .F.
   :nRowDividerStyle  := LINESTYLE_NOLINES        //4
   :nColDividerStyle  := LINESTYLE_NOLINES        //4

   :nDataLines        :=  1

   :lAllowColSwapping := .F.
   :lVScroll          := .F.
   :lHScroll          := .F.
   :nRowHeight        :=  56
   :nFreeze           :=  1
   :nMarqueeStyle     :=  3 //MARQSTYLE_HIGHLROW //CELL  //LROWMS // 3     // 5

   //:nSpRecordSel      :=  0

   if !lSw
   :nRecSelColor      := METRO_ACTIVEBORDER   //METRO_APPWORKSPACE
   :bClrSel           := { || { METRO_WINDOW , METRO_APPWORKSPACE } }
   :bClrStd           := { || { METRO_WINDOW , METRO_ACTIVEBORDER } }
   :bClrSelFocus      := { || { METRO_APPWORKSPACE, METRO_WINDOW  } }
   :bClrRowFocus      := { || { METRO_APPWORKSPACE , METRO_WINDOW }  }
   :SetBrush( oBrush1 )
   else
   :nRecSelColor      := METRO_APPWORKSPACE
   :bClrSel           := { || { METRO_APPWORKSPACE, METRO_WINDOW } }
   :bClrStd           := { || { METRO_ACTIVEBORDER, METRO_WINDOW } }
   :bClrSelFocus      := { || { METRO_WINDOW , METRO_APPWORKSPACE } }
   :bClrRowFocus      := { || { METRO_WINDOW , METRO_APPWORKSPACE }  }
   endif

   :lAutoSort         := .F.
   :SetArray( aData )

   :aCols[1]:nWidth         := oDlg:nWidth - 2 //266
   :aCols[1]:lAllowSizing   := .F.
   :aCols[1]:nEditType       := 0

/*
      :bClrSel          := { || { CLR_WHITE, Rgb(128,128,128) } }
      :bClrSelFocus     := { || { CLR_WHITE, Rgb(051,051,051) } }
      :bClrRowFocus     := { || { CLR_BLACK, Rgb(128,128,128) } }
*/
   END
   oXBrw:CreateFromCode()

   //SetParent( oXBrw:hWnd, oDlg:hWnd )
   //oDlg:AddControl( oXBrw )

   //oDlg:SetControl( oXBrw )
   //oDlg:AddControl( oXBrw )
   oBrush1:End()
   oBrush2:End()
   //while oFont1:nCount > 0
         oFont1:End()
   //end
   oFont1   := Nil
   oBrush1  := Nil
   oBrush2  := Nil

Return oXBrw

//----------------------------------------------------------------------------//
/*
//System & API - How to open a folder in explorer in particular folder view
 // Option Explicit

private long ShellExecute Lib "shell32.dll" "ShellExecuteA" (long hwnd, string lpOperation, string lpFile, string lpParameters, string lpDirectory, long nShowCmd)
private long FindWindow Lib "user32" "FindWindowA" (string lpClassName, string lpWindowName)
private long FindWindowEx Lib "user32" "FindWindowExA" (long hWnd1, long hWnd2, string lpsz1, string lpsz2)
private long SendMessage Lib "user32" "SendMessageA" (long hwnd, long wMsg, long wParam, Any lParam)
private void Sleep Lib "kernel32" (long dwMilliseconds)

private enum FolderView{
    viewDEFAULT = 0,
    viewICON = 0x7029,
    viewLIST = 0x702B,
    viewREPORT = 0x702C,
    viewTHUMBNAIL = 0x702D,
    viewTILE = 0x702E
};

private enum ViewType{
    viewFolder = 0,
    viewExplorer = 1
};

const long SW_SHOWNORMAL = 1;
private const WM_COMMAND = 0x111;

//internal void Main() {
//    OpenFolder @"C:\", viewREPORT, viewExplorer;
//}

private void OpenFolder(string sFolderPath, FolderView = viewDEFAULT eView, ViewType = viewFolder eViewType) {
    long N, long lhWnd, long lPrevhWnd;
    if( Len(Dir(sFolderPath, vbDirectory)) == 0 ) return;

    if( eViewType == viewFolder ){
        lPrevhWnd = FindWindow("CabinetWClass", vbNullString);
        ShellExecute 0L, "Open", sFolderPath, vbNullString, vbNullString, SW_SHOWNORMAL;
    }else{
        lPrevhWnd = FindWindow("ExploreWClass", vbNullString);
        Shell @"Explorer.exe /e, C:\";
    }

    if( eView ){
        do{
            DoEvents: N = N + 1;
            if( eViewType == viewFolder ){
                lhWnd = FindWindow("CabinetWClass", vbNullString);
            }else{
                lhWnd = FindWindow("ExploreWClass", vbNullString);
            }
        }while(!( ! (lPrevhWnd == lhWnd | lhWnd == 0) | N == 100000));

        if( N == 100000 | lhWnd == 0 ) return;
         Sleep(100);

        lhWnd = FindWindowEx(lhWnd, 0L, "SHELLDLL_DefView", vbNullString);
        SendMessage lhWnd, WM_COMMAND, eView, 0L;
    }
}
*/
//----------------------------------------------------------------------------//

//----------------------------------------------------------------------------//
