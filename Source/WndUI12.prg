
/*
 *
 * Proyecto: FiveUI
 * Fichero:  WndUI12.prg
 * Descripci�n: Test Clase WindowsUI, y clases heredadas
 *              Definicion de Ventana:
 *              - Entorno UI
 *              - Paneles laterales
 *              - Imagenes de en cuadro izquierdo
 * Autor: Cristobal Navarro Lopez (C)
 * Fecha: 03/10/2013
 *
 */


#include "Fivewin.ch"

#include "colores.ch"


Static   oWinUI
Static   nRefresh          := 0

Function Main()
Local lMax    := .T.
Local lEsMdi  := .F.
Local nColor  := METRO_AZUL3
Local cTit    := "Inicio"
Local cUser   := WNetGetUser()
//Local aBtt    := { ".\Res\Guest32.bmp", ".\Res\Guest32.bmp" }
Local aBtt    := { { ".\Res\Guest32.bmp", ".\Res\Guest32.bmp" }, ;
                   { ".\Res\OnOff32.bmp", ".\Res\OnOff32.bmp" }, ;
                   { ".\Res\Busca32.bmp", ".\Res\Busca32.bmp" } }

Local oDlgR
Local oDlgL
Local oDlgT
Local oDlgB

   // Sets generales---------------------------------------------------------
   SET EPOCH TO 1990               // Admite los a�os desde el 1990 en adelante
   SET CENTURY ON                  // 4 d�gitos a�o
   SET DELETED ON                  // Impedir ver registros marcados borrar
   //SetCancel( .F. )              // Inutiliza ALT + C para abortar programa
   //SetDialogEsc( .F. )           // Impide salir Di�logos con Escape
   SET DATE FORMAT "DD/MM/YYYY"
   SET DECIMALS TO 2
   XBrNumFormat( "E", .T.)

   SetResDebug( .T. )

   //---------------------------     -----------------------------------------//
   // Modelo 002:  Ventana: Panel principal
   //                       En ventana principal: NO MDI (No es necesario MDI)
   //              Definicion de cuadros de di�logos laterales
   //---------------------------     -----------------------------------------//

   // Si el panel izquierdo solo va asociado a la ventana principal
   oWinUI := TWindowsUI():PnelPPal( .T., lMax, lEsMdi, nColor, cTit, cUser, aBtt)

   // Activa la visualizacion de ventanas en panel izquierdo
   oWinUI:lSaveBmp    := .T.

   oWinUI:lDlgRight   := .T.
   oWinUI:SRightUpUI( , , , , )

   oWinUI:lDlgLeft    := .T.
   oWinUI:SLeftUpUI( , , , , )

   oWinUI:lDlgBottom  := .F.
   oWinUI:lDlgTop     := .F.

   oWinUI:ActivaUI()

   Hb_GCall(.t.)
   CLEAR MEMORY

   if File( "checkres.txt" )
      FErase( "checkres.txt" )
   endif
   CheckRes()

Return nil

//----------------------------------------------------------------------------//

Function CalculaRes( nTp )  // nTp -> 0 Ancho   nTp -> 1 Alto
local nAncho
local nAlto
local nPorcAnc
local nPorcAlt
DEFAULT nTp   := 0
      nAncho  := GetSysMetrics( 0 ) //ScreenWidth()  //GetSysmetrics( 4 )
      nAlto   := GetSysMetrics( 1 ) //ScreenHeight() //GetSysmetrics( 3 )
      if Empty( nTp )
         nPorcAnc := Round( ( nAncho/1366 ) , 4 )
      else
         nPorcAlt := Round( ( nAlto/768 ) , 4 )
      endif
     //    1920    1058      1920            1080               17                  17            22
     // ? nAncho, nAlto , GetSysmetrics(0), GetSysmetrics(1), GetSysmetrics(2),GetSysmetrics(3),GetSysmetrics(4)

Return IF( Empty( nTp ), nPorcAnc, nPorcAlt )

//----------------------------------------------------------------------------//

//----------------------------------------------------------------------------//

//----------------------------------------------------------------------------//

//----------------------------------------------------------------------------//

//----------------------------------------------------------------------------//

//----------------------------------------------------------------------------//
