
/*
 *
 * Proyecto: FiveUI
 * Fichero:  WndUI01.prg
 * Descripci�n: Test Clase WindowsUI, y clases heredadas
 *              Definicion de Ventana
 * Autor: Cristobal Navarro Lopez (C)
 * Fecha: 03/10/2013
 *
 */


#include "Fivewin.ch"

#include "colores.ch"

#define BTN_UP              1
#define BTN_DOWN            2
#define BTN_DISABLE         3
#define BTN_OVERMOUSE       4



Static   oWinUI
Static   oMainBar
static   nLevelAlpha       := 255
Static   nRefresh          := 0

Function Main()

// Sets generales---------------------------------------------------------
   SET EPOCH TO 1990
   SET CENTURY ON
   //SET DELETED ON
   //SetCancel( .F. )                // Inutiliza ALT + C para abortar programa
   //SetDialogEsc( .F. )             // Impide salir Di�logos con Escape
   SET DATE FORMAT "DD/MM/YYYY"
   SET DECIMALS TO 2
   XBrNumFormat( "E", .T.)

   SetResDebug( .T. )

   // 1.- Definicion de Ventana
   //     La definicion de ventanas MDI est� ajust�ndose
   //                        ( lMax , lMdi, nColor, nStyl, oWnd, bWnd )
   oWinUI := TWindowsUI():New( .T., .T.  , .F. ,       ,      ,     ,      )

   oWinUI:lBttExitUI    := .T.
   oWinUI:aBttExitUI     := { ".\Res\g60645n.bmp", ".\Res\g60646.bmp", 48, 20 }

   oWinUI:ActivaUI()

   Hb_GCall(.t.)
   CLEAR MEMORY

   if File( "checkres.txt" )
      FErase( "checkres.txt" )
   endif
   CheckRes()

Return nil

//----------------------------------------------------------------------------//


//----------------------------------------------------------------------------//
