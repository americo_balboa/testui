/*
//  Clase: TBarC    ( FROM TBar )
//  A�adido:
//           - Seleccionar si se desea borde o no  ( Francisco A.)
//             Se ha a�adido la DATA lBorder
//           - Poder poner oBrush
//           - Array para gradiente
//           - O solo color de fondo
//           - METHOD ColorGrad que es llamado desde los dos CONSTRUCTOR
//             para compatibilizar ambos metodos: NEW y NEWAT
//           Y en el NewAt
//           - Inicializar la nueva DATA l2013 (que no estaba y provocaba
//             error
*/

#include "FiveWin.ch"
//----------------------------------------------------------------------------//

#define BAR_HEIGHT           28
#define GRAY_BRUSH            2

#define BAR_TOP               1
#define BAR_LEFT              2
#define BAR_RIGHT             3
#define BAR_DOWN              4
#define BAR_FLOAT             5

#define SM_CYBORDER           6
#define SM_CYCAPTION          4

#define COLOR_BTNFACE        15
#define COLOR_BTNSHADOW      16
#define COLOR_BTNHIGHLIGHT   20

#define GROUP_SEP            8

//----------------------------------------------------------------------------//

CLASS TBarC FROM TBar

CLASSDATA lRegistered AS LOGICAL
DATA lBorder

METHOD New( oWnd, nBtnWidth, nBtnHeight, l3D, cMode, oCursor, l2007, l2010,;
            lBorder, oBrush, aGrad, nColor ) CONSTRUCTOR

METHOD NewAt( nRow, nCol, nWidth, nHeight, nBtnWidth, nBtnHeight, oWnd, l3D,;
              cMode, oCursor, lBorder, oBrush, aGrad, nColor ) CONSTRUCTOR

METHOD ColorGrad( aGrad, oBrush )

ENDCLASS

//----------------------------------------------------------------------------//

METHOD New( oWnd, nBtnWidth, nBtnHeight, l3D, cMode, oCursor, l2007, l2010,;
            lBorder, oBrush, aGrad, nColor ) CLASS TBarC

   local oRect

   DEFAULT oWnd := GetWndDefault(), nBtnWidth := BAR_HEIGHT,;
           nBtnHeight := BAR_HEIGHT, l3D := .f., cMode := "TOP", l2007 := .F.,;
           l2010 := .F., lBorder := .F.,;
           nColor := RGB( 171,171,171)

   oRect = oWnd:GetCliRect()

   ::nStyle = if( lBorder, nOR( WS_BORDER, WS_CHILD, WS_VISIBLE, WS_CLIPCHILDREN ),;
                          nOR( WS_CHILD, WS_VISIBLE, WS_CLIPCHILDREN ) )

   ::lBorder     = lBorder
   ::aControls   = {}
   ::nGroups     = 0
   ::oWnd        = oWnd
   ::nTop        = If( cMode == "BOTTOM", oRect:nBottom - nBtnHeight, -1 )
   ::nLeft       = If( cMode == "RIGHT", oRect:nRight - nBtnWidth - ;
                   If( l3D, 3, 0 ), -1 )
   ::nBottom     = If( cMode == "TOP", nBtnHeight, oRect:nBottom + 1 )
   ::nRight      = If( cMode == "TOP" .or. cMode == "BOTTOM",;
                       oRect:nRight,;
                   If( cMode == "LEFT", nBtnWidth + If( l3D, 3, 0 ), oRect:nRight + 1 ) )
   ::nBtnWidth   = nBtnWidth
   ::nBtnHeight  = nBtnHeight
   ::nId         = ::GetNewId()
   ::lDrag       = .f.
   ::lCaptured   = .f.
   ::nClrPane    = If( l3D, GetSysColor( COLOR_BTNFACE ), CLR_GRAY )
   ::lVisible    = .t.
   ::l3D         = l3D
   ::nMode       = AScan( { "TOP", "LEFT", "RIGHT", "BOTTOM", "FLOAT" }, cMode )
   ::oCursor     = oCursor
   ::lValidating = .f.
   ::l2007       = l2007
   ::l2010       = l2010
   ::l2013       = .F.

   ::SetColor( If( ValType( ::nClrText ) == "B", Eval( ::nClrText, .F. ), ::nClrText ), ::nClrPane )

   ::ColorGrad( aGrad, oBrush )

   ::Register( nOR( CS_VREDRAW, CS_HREDRAW ) )
   ::Create()

   do case
      case ::nMode == BAR_TOP
           oWnd:oBar = Self
           oWnd:oTop = Self

      case ::nMode == BAR_LEFT
           oWnd:oLeft = Self

      case ::nMode == BAR_RIGHT
           oWnd:oRight = Self

      case ::nMode == BAR_DOWN
           oWnd:oBottom = Self
   endcase

return Self

//----------------------------------------------------------------------------//

METHOD NewAt( nRow, nCol, nWidth, nHeight, nBtnWidth, nBtnHeight, oWnd, l3D,;
              cMode, oCursor, lBorder, oBrush, aGrad, nColor ) CLASS TBarC
   local oRect

   DEFAULT oWnd := GetWndDefault(), nBtnWidth := BAR_HEIGHT,;
           nBtnHeight := BAR_HEIGHT, nHeight := BAR_HEIGHT,;
           l3D := .f., cMode := "TOP", lBorder := .f. ,;   //FranciscoA May, 31/2013
           nColor := RGB( 171,171,171)

   oRect = oWnd:GetCliRect()

  ::nStyle = if( lBorder, nOR( WS_BORDER, WS_CHILD, WS_VISIBLE, WS_CLIPCHILDREN ),;
                          nOR( WS_CHILD, WS_VISIBLE, WS_CLIPCHILDREN ) )

   ::lBorder     = lBorder
   ::aControls   = {}
   ::nGroups     = 0
   ::oWnd        = oWnd
   ::nTop        = nRow
   ::nLeft       = nCol
   ::nBottom     = nRow + nHeight - 1
   ::nRight      = nCol + nWidth - 1
   ::nBtnWidth   = nBtnWidth
   ::nBtnHeight  = nBtnHeight
   ::nId         = ::GetNewId()
   ::lDrag       = .f.
   ::lCaptured   = .f.
   ::nClrPane    = If( l3D, GetSysColor( COLOR_BTNFACE ), nColor )
   ::lVisible    = .t.
   ::l3D         = l3D
   ::nMode       = AScan( { "TOP", "LEFT", "RIGHT", "BOTTOM", "FLOAT" }, cMode )
   ::oCursor     = oCursor
   ::lValidating = .f.
   ::l2007       = .F.
   ::l2010       = .F.
   ::l2013       = .T.

   ::SetColor( If( ValType( ::nClrText ) == "B", Eval( ::nClrText, .F. ), ::nClrText ), ::nClrPane )

   //if empty( oBrush )
   //   DEFINE BRUSH oBrush COLOR ::nClrPane
   //endif
   ::ColorGrad( aGrad, oBrush )

   ::Register( nOR( CS_VREDRAW, CS_HREDRAW ) )
   ::Create()

   //oBrush:End()
   //oBrush := Nil
return Self

//----------------------------------------------------------------------------//

METHOD ColorGrad( aGrad, oBrush ) CLASS TBarC
Local  nEstilo := 0
if ::l2007
   nEstilo := 1
endif
if ::l2010
   nEstilo := 2
endif
if ::l2013
   nEstilo := 3
endif

   do case
      case nEstilo = 1   //::l2007
           ::bClrGrad = { | lInvert | If( ! lInvert,;
                                        { { 0.25, RGB( 219, 230, 244 ), RGB( 207, 221, 239 ) },;
                                          { 0.75, RGB( 201, 217, 237 ), RGB( 231, 242, 255 ) } },;
                                        { { 0.25, RGB( 255, 253, 222 ), RGB( 255, 231, 151 ) }, ;
                                          { 0.75, RGB( 255, 215,  84 ), RGB( 255, 233, 162 ) } } ) }
      case nEstilo = 2  //::l2010
           ::bClrGrad = { | lInvert | If( ! lInvert,;
           	                            { { 1,   RGB( 255, 255, 255 ), RGB( 229, 233, 238 ) } },;
                                        { { 2/5, RGB( 255, 253, 222 ), RGB( 255, 231, 147 ) },;
                                          { 3/5, RGB( 255, 215,  86 ), RGB( 255, 231, 153 ) } } ) }
      case nEstilo = 3  //::l2013
           ::bClrGrad = { | lInvert | If( ! lInvert,;
           	                            { { 1, RGB( 255, 255, 255 ), RGB( 255, 255, 255 ) } },;
                                        { { 1, RGB( 194, 213, 242 ), RGB( 194, 213, 242 ) } } ) }

      otherwise
           if Empty( aGrad )
           ::bClrGrad = { | lInvert | If( ! lInvert,;
                                        { { 0.25, RGB( 219, 230, 244 ), RGB( 207, 221, 239 ) },;
                                          { 0.75, RGB( 201, 217, 237 ), RGB( 231, 242, 255 ) } },;
                                        { { 0.25, RGB( 255, 253, 222 ), RGB( 255, 231, 151 ) }, ;
                                          { 0.75, RGB( 255, 215,  84 ), RGB( 255, 233, 162 ) } } ) }
           else
              if Empty( oBrush )
                 ::bClrGrad = { | lInvert | If( ! lInvert,;
                                            { aGrad[1], aGrad[2] } ,;
                                            { aGrad[3], aGrad[4] }  )  }
              else
                 ::SetBrush( oBrush )
              endif
           endif
   endcase

Return nil

//----------------------------------------------------------------------------//
