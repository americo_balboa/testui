#include "fivewin.ch"
#include "Constant.ch"
#include "hbcompat.ch"

#define GW_HWNDPREV           3
#define GW_HWNDNEXT           2
#define DT_TOP                0
#define DT_LEFT               0
#define DT_CENTER             1
#define DT_RIGHT              2
#define DT_VCENTER            4
#define DT_BOTTOM             8
#define DT_WORDBREAK         16
#define DT_SINGLELINE        32
#define DT_NOPREFIX        2048

#define WS_EX_TRANSPARENT      0x20
#define GWL_EXSTYLE            -20

#define WM_MOUSELEAVE       0x2A3 //675
#define TME_LEAVE           0x2   //2
#define WM_NCMOUSEHOVER     0x2A0 //672
#define TRANSPARENT         0x1   //1
#define COLOR_BTNFACE       0xF   //15

#define SRCCOPY             0xCC0020
#define CLRTEXT             RGB(  21,  66, 139 )
#define CLRTEXTBACK         RGB( 113, 106, 183 )
#define BLUE3               RGB( 191, 219, 255 )
#define BLUE0               RGB( 218, 229, 243 )
#define BLUE2               RGB( 194, 217, 240 )
#define BLUE1               RGB( 199, 216, 237 )
#define BLUEBOX0            RGB( 141, 178, 227 )
#define BLUEBOX1            RGB( 197, 210, 223 )
#define BLUEBOX2            RGB( 237, 242, 248 )
#define CLRLIGHT            RGB( 216, 230, 246 )
#define CLRDARK             RGB( 191, 213, 238 )
#define YELBOX0             RGB( 249, 204, 96 )
#define YELBOX1             RGB( 255, 255, 189 )
#define QUICKGRAD           { { 0.3, nRGB( 218, 228, 243 ), nRGB( 218, 228, 243 ) },;
                              { 0.65, nRGB( 199, 216, 237 ), nRGB( 217, 228, 242 ) },;
                              { 0.05, nRGB( 141, 178, 227 ), nRGB( 172, 201, 231 ) } }

#define TXTLPAD              15
#define TXTRPAD               5
#define RIGHTGAP             15
#define COLOR_GRAYTEXT       17
#define DEFAULT_GUI_FONT     17

#define QUICKR_NORMAL        1
#define QUICKR_OVER          2
#define QUICKR_PUSH          3
#define QUICKR_BITMAP        4
#define QUICKR_HANDLE        1
#define QUICKR_WIDTH         2
#define QUICKR_HEIGHT        3
#define QUICKR_HASALPHA      4


#define BARHEIGHT            160

#define METRO_HIGHLIGHT           RGB(  51,153,255 )

CLASS TCRibbonBar FROM TRibbonBar

   //CLASSDATA lRegistered AS LOGICAL


   DATA nTypeRound
   DATA l2013
   DATA lFillFld


   METHOD New( oWnd, aPrompts, bAction, nOption,;
            nWidth, nHeight, nTopMargin, nClrPaneRB, nClrBoxOut, nClrBoxIn,;
            nClrBoxSelOut, nClrBoxSelIn, aGrad, aGradFld, aGradHigh, aGradOver,;
            l2010, nStart, nTypeR, l2013 ) CONSTRUCTOR

   METHOD Paint()
   METHOD PaintFld( nPrompt, hDCMem, nType )

   //METHOD PaintOver( nPrompt, lOn )
   METHOD ReSize( nType, nWidth, nHeight )
   METHOD SetBackStage( oBackStage )
   METHOD BackStage()
   METHOD RunAction( nKey )

ENDCLASS

//--------------------------------------------------------------------//

METHOD New( oWnd, aPrompts, bAction, nOption,;
            nWidth, nHeight, nTopMargin, nClrPaneRB, nClrBoxOut, nClrBoxIn,;
            nClrBoxSelOut, nClrBoxSelIn, aGrad, aGradFld, aGradHigh, aGradOver,;
            l2010, nStart, l2013, nTypeR ) CLASS TCRibbonBar

   local oDlg, n, oBrush, hBmp


   DEFAULT l2010 := .F.
   DEFAULT aPrompts := {} ,; //{ "One", "Two", "Three" },;
           oWnd     := GetWndDefault(),;
           nOption  := 1,;
           nWidth   := 200,;
           nHeight  := BARHEIGHT,;
           nStart   := 0
   DEFAULT nTypeR   := 0
   DEFAULT l2013    := .F.

   IF l2010
      DEFAULT ;
           nTopMargin    := 25, ;
           nClrPaneRB    := RGB( 231, 228, 226 ),;
           nClrBoxOut    := RGB( 182, 186, 191 ),;
           nClrBoxIn     := RGB( 255, 255, 255 ),;
           nClrBoxSelOut := RGB( 182, 186, 191 ),;
           nClrBoxSelIn  := RGB( 255, 255, 255 ),;
           aGrad         := { { 1, RGB( 255, 255, 255 ), RGB( 229, 233, 238 ) } },;
           aGradFld      := {| nOpt | if( nOpt == ::nStart, { { 1, RGB( 30, 72, 161 ), RGB( 76, 146, 229 ) } },;
                                      { { 1, RGB( 231, 228, 226 ), RGB( 255, 255, 255 ) } } ) } ,;
           aGradHigh     := { { 1, RGB( 231, 228, 226 ), RGB( 255, 255, 255 ) } } ,;
           aGradOver     := { { 1, RGB( 231, 228, 226 ), RGB( 255, 255, 255 ) } }
   ELSE
      if l2013          // A. Linares
      DEFAULT ;
           nTopMargin    := 25
           nClrPaneRB    := RGB( 255, 255, 255 )
           nClrBoxOut    := RGB( 192, 192, 192 )
           nClrBoxIn     := RGB( 255, 255, 255 )
           nClrBoxSelOut := RGB( 255, 255, 255 )
           nClrBoxSelIn  := RGB( 255, 255, 255 )
           aGrad         := { { 1, RGB( 255, 255, 255 ), RGB( 255, 255, 255 ) } }
           aGradFld      := {| nOpt | if( nOpt == ::nStart, { { 1, RGB( 255, 255, 255 ), RGB( 255, 255, 255 ) } },;
                                     { { 1, RGB( 43, 87, 154 ), RGB( 43, 87, 154 ) } } ) }
           aGradHigh     := { { 1, RGB( 255, 255, 255 ), RGB( 255, 255, 255 ) } }
           aGradOver     := { { 1, RGB( 255, 255, 255 ), RGB( 255, 255, 255 ) } }
      else
      DEFAULT ;
           nTopMargin    := 25,;
           nClrPaneRB    := BLUE3,;
           nClrBoxOut    := BLUEBOX0,;
           nClrBoxIn     := BLUEBOX2,;
           nClrBoxSelOut := YELBOX0,;
           nClrBoxSelIn  := YELBOX1,;
           aGrad         := { {0.12, BLUE0, BLUE0 }, { 0.70, BLUE1, BLUE0 }, { 0.18, BLUE0, CLR_WHITE }} ,;
           aGradFld      := {| nOpt | if( nOpt == ::nStart, { { 1, RGB( 30, 72, 161 ), RGB( 76, 146, 229 ) } },;
                                       { { 1, CLR_WHITE, BLUE0 }  } ) } ,;
           aGradHigh     := { { 1, BLUE3, RGB( 202,172,136 ) } } ,;
           aGradOver     := { { 1, RGB( 197,221,251), RGB( 226,209,162)} }
      endif
   ENDIF

   ::aPrompts := CheckArray( aPrompts )
   ::nStart   = nStart

   ::nTop             = 0
   ::nLeft            = 0
   ::nBottom          = nHeight
   ::nRight           = nWidth
   ::oWnd             = oWnd
   ::nTopMargin       = nTopMargin
   ::nRightMargin     = RIGHTGAP
   if l2010
      ::nLeftMargin   = 10
   else
      ::nLeftMargin      = 60
   endif
   ::nGroupSeparation = 3

   ::nOption      = ::ScanOption( nOption )
   ::nOffSet      = 1
   ::nHeightFld   = 20
   ::hSeparation  = 15
   ::nLastOver    = 0
   ::nType        = 4
   ::l2010        = l2010

   ::aDialogs   = {}
   ::aEnable    = {}
   ::aVisible   = {}
   ::aLeft      = {}
   ::aClrTabTxt = {}

   ::bMMoved     = { |nRow, nCol | ::MouseHover( nRow, ncol ) }

   ::nStyle      = nOR( WS_CHILD, WS_VISIBLE, WS_CLIPCHILDREN)
   ::nId         = ::GetNewId()

   //Colors
   ::aGrad         = aGrad
   ::aGradFld      = aGradFld
   ::aGradHigh     = aGradHigh
   ::aGradOver     = aGradOver

   ::nClrPaneRB    = nClrPaneRB
   ::nClrBoxOut    = nClrBoxOut
   ::nClrBoxIn     = nClrBoxIn
   ::nClrBoxSelOut = nClrBoxSelOut
   ::nClrBoxSelIn  = nClrBoxSelIn
   ::lUseAcc       = .T.
   ::aControlsList = {}
   ::lAccActivated = .F.
   ::lOverQ        = .F.
   ::lPushQ        = .F.
   ::lQuickRound   = .F.

   ::nTypeRound    = nTypeR
   ::l2013         = l2013
   ::lFillFld      = .T.

   ::bPaintAcc     = {| hDC | ::PaintAccTabs( hDC ) }
   ::bAction       = bAction

   ::hBmpBrushEx   = GradientBmp( Self, ::nRight - ::nRightMargin - 3, ::nBottom - ::nTopMargin + 1, ::aGrad )
   ::hBrushEx      = CreatePatternBrush( ::hBmpBrushEx )

   ::Register( nOr( CS_VREDRAW, CS_HREDRAW ) )

   if ! Empty( oWnd:hWnd )
      ::Create()
      ::lVisible = .T.
      oWnd:AddControl( Self )
   else
      oWnd:DefControl( Self )
      ::lVisible  = .F.
   endif

   if ::oFont == nil
      ::oFont := TFont():New()
      ::oFont:hFont := GetStockObject( DEFAULT_GUI_FONT )
   endif

   for n = 1 to Len( ::aPrompts )
      oDlg := TRPanel():New( ::nTopMargin+1, 3, ::nBottom-5, ::nRight - ::nRightMargin - 3 , Self )
      oDlg:Hide()
      oDlg:lTransparent := .T.
      AAdd( ::aDialogs, oDlg )
      AAdd( ::aLeft, 0 )
      AAdd( ::aVisible, .T. )
      AAdd( ::aEnable, .T. )
      AAdd( ::aClrTabTxt, {|| CLRTEXT } )
   next

//   ::oWnd:oTop := Self

   ::Default()

   // SetWndDefault( oWnd )

return self

//--------------------------------------------------------------------//

//--------------------------------------------------------------------//

METHOD Paint() CLASS TCRibbonBar

   local nTop, nBottom
   local aRect := GetClientRect( ::hWnd )
   local hDCMem, hBmpMem
   local hOldBmp
   local hBrush, hBrush2
   local n, nType
   local nLastPrmpt := Len( ::aDialogs )
   local aInfo := ::DispBegin()
   local nBmpQ
   local nTopBmp, nLeftBmp

   DEFAULT nType := 4

   hDCMem  = CreateCompatibleDC( ::hDC )

   hBmpMem = CreateCompatibleBitmap( ::hDC, aRect[ 4 ], aRect[ 3 ] )

   hOldBmp = SelectObject( hDCMem, hBmpMem )


   hBrush2 =  CreateSolidBrush( ::nClrPaneRB )
   FillRect( hDCMem, aRect, hBrush2 )

   nTop    = aRect[ 1 ] + ::nTopMargin

   if ::oBackStage == NIL .OR. ! ::oBackStage:lVisible                   // 7, 7
      RoundBox( hDCMem, 1, nTop-1, aRect[ 4 ]+1, aRect[ 3 ] - 2, ::nTypeRound, ::nTypeRound, ::nClrBoxOut )
      RoundBox( hDCMem, 2, nTop  , aRect[ 4 ], aRect[ 3 ] - 3, ::nTypeRound, ::nTypeRound, ::nClrBoxIn )
   endif

   if Len( ::aDialogs ) > 0
      for n = nLastPrmpt to ::nOffSet step -1
          if n != ::nOption
             ::PaintFld( n, hDCMem, 4 )
          endif
      next
      if ::aLeft[ ::nOption ] < ( ::nWidth )
         nType := if ( ::aVisible[ ::nOption ], if( ::nLastOver == ::nOption, 2,1 ), 4 )
         ::PaintFld( ::nOption, hDCMem, nType )
      else
         ::PaintFld( ::nOption, hDCMem, 4)
      endif
   endif


   BitBlt( ::hDC, 0, 0, aRect[4], aRect[3], hDCMem, 0, 0, SRCCOPY )


   SelectObject( hDCMem, hOldBmp )

   DeleteObject( hBrush )
   DeleteObject( hBrush2 )
   DeleteObject( hBmpMem )

   DeleteDC( hDCMem )

   if ::aQuickRGrad != NIL
      GradientFill( ::hDC, 0, 0, 32, ::nWidth, ::aQuickRGrad )
   endif

   if ::lQuickRound

      if ::lOverQ
         if ::lPushQ
            nBmpQ = QUICKR_PUSH
         else
            nBmpQ = QUICKR_OVER
         endif
      else
         nBmpQ = QUICKR_NORMAL
      endif
      //Paint QuickButton
      if ::aQuickBmp[ nBmpQ ][ QUICKR_HANDLE ] != 0
         nTopBmp  = Max( 0, 26 - ::aQuickBmp[ nBmpQ ][ QUICKR_HEIGHT ] / 2 )
         nLeftBmp = Max( 5, 27 - ::aQuickBmp[ nBmpQ ][ QUICKR_WIDTH ] / 2 )
         if ::aQuickBmp[ nBmpQ ][ QUICKR_HASALPHA ]
            ABPaint( ::hDC, nLeftBmp, nTopBmp + 2, ::aQuickBmp[ nBmpQ ][ QUICKR_HANDLE ], 255 )
         else
            DrawTransparent( ::hDC, ::aQuickBmp[ nBmpQ ][ QUICKR_HANDLE ], nTopBmp, nLeftBmp )
         endif
      endif
      //Paint bitmap Over Quick Button
      if ::aQuickBmp[ QUICKR_BITMAP ][ QUICKR_HANDLE ] != 0
         nTopBmp = Max( 0, 26 - ::aQuickBmp[ QUICKR_BITMAP ][ QUICKR_HEIGHT ] / 2 )
         nLeftBmp = ( ::aQuickBmp[ QUICKR_NORMAL ][ QUICKR_WIDTH ] / 2 ) - ( ::aQuickBmp[ QUICKR_BITMAP ][ QUICKR_WIDTH ] / 2 ) + 3
         if ::aQuickBmp[ QUICKR_BITMAP ][ QUICKR_HASALPHA ]
            ABPaint( ::hDC, nLeftBmp, nTopBmp, ::aQuickBmp[ QUICKR_BITMAP ][ QUICKR_HANDLE ], 255 )
         else
            DrawTransparent( ::hDC, ::aQuickBmp[ QUICKR_BITMAP ][ QUICKR_HANDLE ], nTopBmp, nLeftBmp )
         endif
      endif

   endif

   if ::oQuickAcc != NIL
      ::oQuickAcc:Paint()
   endif

   ::DispEnd( aInfo )

   //SetWindowLong( ::hWnd, GWL_EXSTYLE, 0 )

return 0

//----------------------------------------------------------------------------//

METHOD PaintFld( nPrompt, hDCMem, nType )  CLASS TCRibbonBar

   local hBrush2
   local hOldFont
   local nOldClr
   local lRet
   local hBrush3
   local nRow
   local nCol
   local lSelected
   local nMode
   local nClrTxt
   local aRect        := GetClientRect( ::hWnd )
   local bPaintTxt
   local hPen
   local hOldPen
   local aGradFld

   if nPrompt > Len( ::aDialogs ) .or. ! ::aVisible[ nPrompt ]
      return nil
   endif

   if ::bWhen != nil
      lRet := eval( ::bWhen, self )
      ::lEnable := lRet
   endif

   hBrush2 :=  CreateSolidBrush( ::nClrPaneRB )
   hBrush3 :=  CreateSolidBrush( ::aGrad[ 1,2 ] )

   nRow := ::nTopMargin - ::nHeightFld
   nCol := ::aLeft[ nPrompt ]

   lSelected := ( nPrompt == ::nOption )

   aGradFld = Eval( ::aGradFld, nPrompt )

   do case
      case nType == 1
         if ::lFillFld
         FillRect( hDCMem, { nRow - 2, nCol - 3, nRow + ::nHeightFld - 1,  nCol + ::aSizes[ nPrompt ] + 3 }, hBrush2 )
         GradientFill( hDCMem, nRow, nCol + 1, nRow + ::nHeightFld - 1, nCol +  ::aSizes[ nPrompt ], aGradFld )
         endif
         if !::l2013                                                                                                              // 7, 7
         RoundBox( hDCMem, nCol - 1, nRow - 1,  nCol+::aSizes[ nPrompt ] + 1, nRow + ::nHeightFld + 3, ::nTypeRound, ::nTypeRound, ::nClrBoxOut )
         RoundBox( hDCMem, nCol, nRow,  nCol + ::aSizes[ nPrompt ], nRow + ::nHeightFld + 3, ::nTypeRound, ::nTypeRound, ::nClrBoxIn )
         else
            if !::lFillFld
         RoundBox( hDCMem, nCol - 1, nRow - 1,  nCol+::aSizes[ nPrompt ] + 1, nRow + ::nHeightFld + 3, ::nTypeRound, ::nTypeRound, ::nClrBoxOut )
         RoundBox( hDCMem, nCol, nRow,  nCol + ::aSizes[ nPrompt ], nRow + ::nHeightFld + 3, ::nTypeRound, ::nTypeRound, ::nClrBoxIn )
            endif
         endif
      case nType == 2
         if ! ::l2010 .and. nPrompt != ::nStart
            if ::lFillFld
            GradientFill( hDCMem, nRow, nCol - 3, nRow + ::nHeightFld - 2, nCol +  ::aSizes[ nPrompt ] + 3, ::aGradHigh )
            endif
         endif
         if ::lFillFld
         GradientFill( hDCMem, nRow + 1, nCol, nRow + ::nHeightFld + 1, nCol +  ::aSizes[ nPrompt ], aGradFld )
         endif
         if !::l2013                                                                                   // 7,7
         RoundBox( hDCMem, nCol - 1, nRow - 1,  nCol+::aSizes[ nPrompt ] + 1, nRow + ::nHeightFld + 3, ::nTypeRound, ::nTypeRound, ::nClrBoxSelOut )
         RoundBox( hDCMem, nCol, nRow,  nCol + ::aSizes[ nPrompt ], nRow + ::nHeightFld + 3, ::nTypeRound, ::nTypeRound, ::nClrBoxSelIn )
         else
           if !::lFillFld
         RoundBox( hDCMem, nCol - 1, nRow - 1,  nCol+::aSizes[ nPrompt ] + 1, nRow + ::nHeightFld + 3, ::nTypeRound, ::nTypeRound, ::nClrBoxSelOut )
         RoundBox( hDCMem, nCol, nRow,  nCol + ::aSizes[ nPrompt ], nRow + ::nHeightFld + 3, ::nTypeRound, ::nTypeRound, ::nClrBoxSelIn )
           endif
         endif
      case nType == 3
         if ::aEnable[ nPrompt ]
            if ::l2010 .or. nPrompt == ::nStart
               GradientFill( hDCMem, nRow + 1, nCol, nRow + ::nHeightFld + 1,nCol +  ::aSizes[ nPrompt ], aGradFld )
            else
               if ::lFillFld
               GradientFill( hDCMem, nRow + 1, nCol, nRow + ::nHeightFld + 1,nCol +  ::aSizes[ nPrompt ], ::aGradOver )
               endif
            endif
            if nPrompt != ::nStart                                                                       //  7,7
               if !::l2013
               RoundBox( hDCMem, nCol - 1, nRow - 1,  nCol+::aSizes[ nPrompt ] + 1, nRow + ::nHeightFld + 3, ::nTypeRound, ::nTypeRound, ::nClrBoxOut )
               RoundBox( hDCMem, nCol, nRow,  nCol + ::aSizes[ nPrompt ], nRow + ::nHeightFld + 3, ::nTypeRound, ::nTypeRound, ::nClrBoxIn )
               else
                 if !::lFillFld
               RoundBox( hDCMem, nCol - 1, nRow - 1,  nCol+::aSizes[ nPrompt ] + 1, nRow + ::nHeightFld + 3, ::nTypeRound, ::nTypeRound, ::nClrBoxOut )
               RoundBox( hDCMem, nCol, nRow,  nCol + ::aSizes[ nPrompt ], nRow + ::nHeightFld + 3, ::nTypeRound, ::nTypeRound, ::nClrBoxIn )
                 endif
               endif
            else                                                                                       //7,7
               if !::l2013
               RoundBox( hDCMem, nCol - 1, nRow ,  nCol+::aSizes[ nPrompt ] + 1, nRow + ::nHeightFld + 3, ::nTypeRound, ::nTypeRound, ::nClrBoxOut )
               else
                  if ::lFillFld
                  endif
               endif
            endif

         else

            FillRect( hDCMem, { nRow - 2, nCol - 2, nRow + ::nHeightFld - 1,  nCol + ::aSizes[ nPrompt ] + 2 }, hBrush2 )

         endif

      case nType == 4
         if nPrompt == ::nStart
            if ::lFillFld
            GradientFill( hDCMem, nRow + 1, nCol, nRow + ::nHeightFld + 1,nCol +  ::aSizes[ nPrompt ], aGradFld )
            endif                                                                                       // 7,7
            if !::l2013
            RoundBox( hDCMem, nCol - 1, nRow ,  nCol+::aSizes[ nPrompt ] + 1, nRow + ::nHeightFld + 3, ::nTypeRound, ::nTypeRound, ::nClrBoxOut )
            else
               if !::lFillFld
            //RoundBox( hDCMem, nCol - 1, nRow ,  nCol+::aSizes[ nPrompt ] + 1, nRow + ::nHeightFld + 3, ::nTypeRound, ::nTypeRound, ::nClrBoxOut )
               endif
            endif
            if ::lFillFld
            FillRect( hDCMem, { nRow + ::nHeightFld - 1, nCol - 1, nRow + ::nHeightFld + 3, nCol + ::aSizes[ nPrompt ] + 1 }, hBrush3 )
            endif
         else

            FillRect( hDCMem, { nRow - 2, nCol - 2, nRow + ::nHeightFld - 1,  nCol + ::aSizes[ nPrompt ] + 2 }, hBrush2 )

         endif
   endcase

   if lSelected .and. ::aVisible[ nPrompt ]
      FillRect( hDCMem, { nRow + ::nHeightFld - 1, nCol - 1, nRow + ::nHeightFld + 3, nCol + ::aSizes[ nPrompt ] + 1 }, hBrush3 )
   endif

   if nPrompt == ::nStart
      hPen = CreatePen( PS_SOLID, 1, ::nClrBoxOut )
      hOldPen = SelectObject( hDCMem, hPen )
      MoveTo( hDCMem, nCol - 2, nRow + ::nHeightFld - 1 )
      LineTo( hDCMem, nCol + ::aSizes[ nPrompt ] + 1 , nRow + ::nHeightFld - 1 )
      SelectObject( hDCMem, hOldPen )
      DeleteObject( hPen )
   endif


  bPaintTxt  := <  | oSelf |
                    local hOldFont, nClrTxt, nOldClr, nMode
                    hOldFont  = SelectObject( hDCMem, oSelf:oFont:hFont  )
                    if ValType( oSelf:aClrTabTxt[ nPrompt ] ) == "N"
                       nClrTxt = oSelf:aClrTabTxt[ nPrompt ]
                    elseif ValType( oSelf:aClrTabTxt[ nPrompt ] ) == "B"
                       nClrTxt = Eval( oSelf:aClrTabTxt[ nPrompt ], oSelf )
                    else
                       nClrTxt = CLRTEXT
                    endif
                    if nPrompt == oSelf:nStart
                       nClrTxt = CLR_WHITE
                    endif
                    nOldClr   = SetTextColor( hDCMem, if( oSelf:aEnable[ nPrompt ], nClrTxt, GetSysColor( COLOR_GRAYTEXT ) ) )
                    nMode     = SetBkMode( hDCMem, TRANSPARENT )

                    DrawText( hDCMem, oSelf:aPrompts[ nPrompt ], { nRow+2, nCol, nRow + oSelf:nHeightFld+1, nCol + oSelf:aSizes[ nPrompt ] }, ;
                                       nOr( DT_SINGLELINE, DT_VCENTER, DT_CENTER, DT_NOPREFIX ) )
                    SetBkMode(hDCMem, nMode )

                    SetTextColor( hDCMem, nOldClr )
                    SelectObject( hDCMem, hOldFont  )
                    return nil
                    >

   Eval( bPaintTxt, Self )

   DeleteObject( hBrush2 )
   DeleteObject( hBrush3 )
//   DeleteObject( hOldFont )
//   DeleteObject( nOldClr )


return 0

//----------------------------------------------------------------------------//
/*
METHOD PaintOver( nPrompt, nType ) CLASS TRibbonBar

return 0
*/
//---------------------------------------------------------------------------//

METHOD ReSize( nType, nWidth, nHeight ) CLASS TCRibbonBar

   local aRect := GetClientRect(::hWnd)
   local nMaxWidth, n, j
   local hDCMem, hBmpMem, hOldBmp
   local oDlg, oControl

   oDlg = ::aDialogs[ ::nOption ]

   nMaxWidth = ::CalMaxWidth( oDlg )

      oDlg:Setsize( aRect[4] - aRect[2] - 5 , aRect[3] - aRect[1] - ::nTopMargin-5,.T. )

      if !empty( oDlg:aControls ) .and. nMaxWidth + 3 > nWidth
         for j = Len( oDlg:aControls ) to 1 step -1
            oControl := oDlg:aControls[ j ]
            if oControl:ClassName() == "TRBGROUP"
               if !oControl:lStretch .and. nMaxWidth - 3 > nWidth
                  oControl:lStretch := .T.
                  if !empty( oControl:aControls )
                     aeval( oControl:aControls, {| o | o:Hide() } )
                  endif
                  nMaxWidth := ::CalMaxWidth( oDlg )
               endif
            endif

         next

      elseif !empty( oDlg:aControls ) .and. nMaxWidth < nWidth

         for j = 1 to Len( oDlg:aControls )
            oControl := oDlg:aControls[ j ]
            if oControl:ClassName() == "TRBGROUP"
               if oControl:lStretch .and. nMaxWidth + oControl:nOldWidth - oControl:nStretchWidth + 3 < nWidth
                  oControl:lStretch := .F.
                  if !empty( oControl:aControls )
                     aeval( oControl:aControls, {| o | o:Show() } )
                  endif
                  nMaxWidth := ::CalMaxWidth( oDlg )
               endif
            endif
         next
      endif

   if !empty( ::aDialogs[ ::nOption ]:aControls )
      aeval( ::aDialogs[ ::nOption ]:aControls, {| o | o:Display() } )
   endif

   ::oWnd:ReSize( nType, nWidth, nHeight )

   if ::oBackStage != nil
      ::oBackStage:SetSize( ::oWnd:nWidth,;
                            GetClientRect( ::oWnd:hWnd )[ 3 ] - ::nTopMargin - ;
                            If( ::oWnd:oMsgBar != nil, ::oWnd:oMsgBar:nHeight, 0 ) )
      if ::oWnd:oMsgBar != nil
         ::oWnd:oMsgBar:Refresh()
      endif
   endif

return ::Super:ReSize( nType, nWidth, nHeight )

//---------------------------------------------------------------------------//

METHOD SetBackStage( oBackStage ) CLASS TCRibbonBar

   Local oSelf := Self

   ::oBackStage = oBackStage        //0
   //::oBackStage:Move( ::nTopMargin, ::nLeft, ::nWidth, GetClientRect( ::oWnd:hWnd )[ 3 ] - ::nTopMargin )
   ::oBackStage:Move( (::nTopMargin+1)*2, ::nLeft, oBackStage:nWidth+7, ;
                      GetClientRect( ::oWnd:oWnd:hWnd )[ 3 ] - (::nTopMargin+1)*2 )
   ::oBackStage:oWnd = ::oWnd

   ::oBackStage:bOnShow = {| | oSelf:Move( , , , oSelf:nTopMargin + 2 ) }
   ::oBackStage:bOnHide = {| | oSelf:Move( , , , oSelf:Move( , , , oSelf:nHeightCtrl ) ) }
   ::nHeightCtrl = ::nHeight
   ::oBackStage:SetColor( "N/W*" )

RETURN NIL

//---------------------------------------------------------------------------//

METHOD BackStage() CLASS TCRibbonBar

   if ::oBackStage != nil
      if ! IsWindowVisible( ::oBackStage:hWnd )
         //? ::oBackStage:nTop, ::oBackStage:nLeft, ::oBackStage:nWidth, ::oBackStage:nHeight
         SetWindowPos( ::oBackStage:hWnd, 0, ::oBackStage:nTop, ::oBackStage:nLeft, ::oBackStage:nWidth, ::oBackStage:nHeight, 0x40 )
         ::oBackStage:Show()
         AEval( ::aDialogs, { | o | o:Hide() } )
      else
         ::oBackStage:Hide()
         ::aDialogs[ ::nOption ]:Show()
      endif
      if ::bOnBackStage != NIl
         Eval( ::bOnBackStage, Self )
      endif
   endif

return nil

//----------------------------------------------------------------------------//

METHOD RunAction( nKey, lControl ) CLASS TCRibbonBar

   local nCurrent := 0
   local nTemp

   if nKey > 95 .and. nKey < 106
      nKey -= 48
   endif

   DEFAULT ::cBufferAcc := ""  // new !!!

   If nKey > 0
      ::cBufferAcc += Chr( nKey )
      if Len( ::cBufferAcc ) == 1
         if AScan( ::aAcc, {| uItem | Left( uItem[ 2 ], 1 ) == ::cBufferAcc } ) == 0
            ::cBufferAcc = ""
         endif
      endif

      if Len( ::aAcc ) > 10
         if Len( ::cBufferAcc ) == 2
            nCurrent = AScan( ::aAcc, {| uItem | uItem[ 2 ] == ::cBufferAcc } )
            if nCurrent == 0
               ::ResetAcc()
            endif
         else
            nTemp = AScan( ::aAcc, {| uItem | SubStr( uItem[ 2 ], 1, 1 ) == ::cBufferAcc } )
            if nTemp == 0
               ::ResetAcc()
            endif
         endif
      else
         nCurrent = AScan( ::aAcc, {| uItem | uItem[ 2 ] == ::cBufferAcc } )
      endif
      if nCurrent > 0
         Eval( ::aAcc[ nCurrent, 1 ] )
      endif
   else
      ::ResetAcc()
   endif

return nil


//-------------------------------------------------//
/*
METHOD PaintAccTabs( hDC ) CLASS TRibbonBar

return nil
*/
//-------------------------------------------------//
/*
METHOD PaintAccControls( hDC ) CLASS TRibbonBar

return nil
*/
//-------------------------------------------------//


//BACKSTAGE TYPE OPTION
#define BCKTYPE_SELECT    1
#define BCKTYPE_BUTTON    2
//-------------------------------------------------//

CLASS TCBackStage FROM TBackStage

   CLASSDATA lRegistered AS LOGICAL

   METHOD New( nTop, nLeft, nBottom, nRight, oWnd, nMainWidth ) CONSTRUCTOR
   METHOD Paint()
   METHOD AddOption( cPrompt, nClrText, aGradOver,;
                  nType, nHeigh, nClrStart, nClrEnd, nBorderClr,;
                  cBitmap, nClrTextOver, nLeftMargin, ;
                  nAlphaLevel, bAction )

ENDCLASS

//----------------------------------------------------------------------------//

METHOD New( nTop, nLeft, nBottom, nRight, oWnd, nMainWidth ) CLASS TCBackStage

   DEFAULT nTop := 0, nLeft := 0, nBottom := 100, nRight := 100,;
           oWnd := GetWndDefault(),;
           nMainWidth := 155

   ::nTop    = nTop
   ::nLeft   = nLeft
   ::nBottom = nBottom
   ::nRight  = nRight
   ::oWnd    = oWnd
   ::nStyle  = nOr( WS_CHILD, WS_VISIBLE, WS_CLIPCHILDREN )
   ::lDrag   = .F.
   ::nClrPane = GetSysColor( COLOR_BTNFACE )
   ::nMainWidth := nMainWidth
   ::nOverOpt = 0

   ::aOptions  = {}

   //? ::oWnd, ::nTop

   ::Register()

   if ! Empty( ::oWnd:hWnd )
      ::Create()
      ::oWnd:AddControl( Self )
      if ::oWnd:oBrush != nil
         ::SetBrush( ::oWnd:oBrush )
      endif
   else
      ::oWnd:DefControl( Self )
   endif

   if ::oFont == nil
      ::oFont := TFont():New()
      ::oFont:hFont := GetStockObject( DEFAULT_GUI_FONT )
   endif

   ::Hide()

return Self

//----------------------------------------------------------------------------//

METHOD AddOption( cPrompt, nClrText, aGradOver,;
                  nType, nHeigh, nClrStart, nClrEnd, nBorderClr,;
                  cBitmap, nClrTextOver, nLeftMargin, ;
                  nAlphaLevel, bAction ) CLASS TCBackStage

   local oContainer := Self
   local nSelOpt
   local nTop

   nTop = ::CalOptPos()

   AAdd( ::aOptions, TCBackStageOption():New( oContainer, nTop, cPrompt, nClrText, ;
                                             aGradOver, nType, nHeigh, nClrStart, nClrEnd,;
                                             nBorderClr, cBitmap, nClrTextOver, nLeftMargin, ;
                                             nAlphaLevel, bAction ) )
   nSelOpt = ::GetSelectedOpt()

   if nSelOpt > 0 .and. ! ::aOptions[ nSelOpt ]:lSelected
      ::aOptions[ nSelOpt ]:lSelected = .T.
   endif

RETURN ATail( ::aOptions )

//-----------------------------------------------

METHOD Paint() CLASS TCBackStage

   local nTop, nLeft, nHeight, nWidth, nBevel
   local aInfo   := ::DispBegin()
   local hDC     := ::hDC  //::oWnd:oWnd:hWnd   //::hDC

   //::nTop    = 54
   //::nLeft   = 270
   //::nBottom = 100
   //::nRight  = ::nTop + ::nMainWidth



   nTop     := ::nTop
   nLeft    := ::nLeft   // 270
   nHeight  := ::nHeight()
   nWidth   := ::nMainWidth
                                    //::oWnd:oWnd:hWnd
                                    // ::hWnd
   FillRect( hDC, GetClientRect( ::hWnd ), ::oBrush:hBrush )
   //FillRect( hDC, { nTop, nLeft+1, nWidth, nHeight }, ::oBrush:hBrush )

   Gradient( hDC, { nTop, nLeft, nHeight,  nWidth },;
                  nRGB( 251, 252, 253 ), nRGB( 224, 227, 231 ), .T. )

   Gradient( hDC, { nTop, nWidth-2, nHeight, nWidth },;
                   nRGB( 234, 236, 239 ), nRGB( 202, 203, 204 ), .F. )


   AEval( ::aOptions, {| o, nId | o:Paint( ::nOverOpt == nId, hDC ) } )

   if ValType( ::bPainted ) == "B"
      Eval( ::bPainted, hDC, ::cPS, Self )
   endif

   ::DispEnd( aInfo )

return 0

//-----------------------------------------------

CLASS TCBackStageOption FROM TBackStageOption

   DATA nTypeRound
   DATA hArrowBck

   METHOD New( oContainer, nTop, cPrompt, nClrText, ;
               aGradOver, nType, nHeight, nClrStart, nClrEnd,;
               cBitmap, nLeftMargin, nClrTextOver,;
               nAlphaLevel, bAction)

   METHOD Paint( lOver, hDC )

ENDCLASS

//-----------------------------------------------

METHOD New( oContainer, nTop, cPrompt, nClrText, aGradOver, ;
            nType, nHeight, nClrStart, nClrEnd, nBorderClr, ;
            cBitmap, nLeftMargin, nClrTextOver, nAlphaLevel, bAction ) ;
            CLASS TCBackStageOption

Local nCol

                            //nRGB( 231, 240, 250 ), nRGB( 234, 236, 239 )
   DEFAULT aGradOver  := { { 1, METRO_HIGHLIGHT, METRO_HIGHLIGHT } },;
           nBorderClr := nRGB( 82, 125, 224 ),;
           nHeight    := 38,;
           nType      := BCKTYPE_SELECT,;
           nLeftMargin := 20,;
           nClrText   := 0,;
           nClrTextOver := If( nType == BCKTYPE_SELECT, nRGB( 255, 255, 255 ), 0 ),;
           nAlphaLevel := 255,;
           nClrStart  := nRGB( 96, 166, 242 ),;
           nClrEnd    := nRGB( 37, 99, 195 )


   ::lSelected  = .F.
   ::oContainer = oContainer
   ::nTop       = nTop
   ::cPrompt    = cPrompt
   ::nClrText   = nClrText
   ::aGradOver  = aGradOver
   ::nBorderClr = nBorderClr
   ::nType      = nType

   //nCol         := ::oContainer:nLeft
   //::nTop       := ::oContainer:nBottom
                               // 0                        //   0  , 0
   ::hSelected  = CircleGradient( ::oContainer:oWnd:hWnd, { 0, 0, nHeight, oContainer:nMainWidth }, nClrStart, nClrEnd, 0, 20, 2 )
   ::nClrTextOver = nClrTextOver
   ::nLeftMargin = nLeftMargin  //::oContainer:nLeftMargin   //nLeftMargin
   ::bAction    = bAction

   ::nHeight    = nHeight - If( nType == BCKTYPE_BUTTON, 12, 0 )
   ::nWidth     = oContainer:nMainWidth

   ::hArrowBck  = bmp_arrowbck()
   ::hBitmap    = fnAddBitmap( cBitmap )
   ::lHasAlpha  = HasAlpha( ::hBitmap )

   ::nTypeRound = 0

RETURN Self

//-----------------------------------------------

METHOD Paint( lOver, hDC ) CLASS TCBackStageOption

   local nTop
   local oBmp
   local oCon := ::oContainer
   local nBmpW
   local nBmpH
   local hPen
   local hOldPen
   local hBrush
   local hOldBrush
   local nRound := ::nTypeRound   // 5
   local hOldFont
   local nOldClr
   local nCol     := oCon:nLeft    //0
   local nAdj     := Max( 8, nCol )

   //nTop           := ::nTop
   //::nTop           := oCon:nBottom + 2

   //? nCol

   DEFAULT lOver := .F.

   if ::lSelected .and. ::nType == BCKTYPE_SELECT
      DrawBitmap( hDC, ::hSelected, ::nTop, 0 )
      nBmpH = nBmpHeight( ::hArrowBck )
      nBmpW = nBmpWidth( ::hArrowBck )
      DrawTransparent( hDC, ::hArrowBck, ::nTop + ( Int( ::nHeight / 2 ) - Int( nBmpH / 2 ) ), ::nWidth - nBmpW )
   else

      if lOver

         if ::nType == BCKTYPE_SELECT
            GradientFill( hDC, ::nTop + 1, nCol,;        //0,;
                          ::nTop + ::nHeight - 1, ;
                          ::nWidth - 3, ::aGradOver, .F. )

            hPen = CreatePen( PS_SOLID, 1, ::nBorderClr )
            hOldPen = SelectObject( hDC, hPen )
            MoveTo( hDC, nCol, ::nTop + 1 )
            LineTo( hDC, ::nWidth, ::nTop + 1 )
            MoveTo( hDC, nCol, ::nTop + ::nHeight - 1 )
            LineTo( hDC, ::nWidth, ::nTop + ::nHeight - 1)
            SelectObject( hDC, hOldPen )
            DeleteObject( hPen )
         else
            hBrush = GradientBrush( hDC, 0, nCol, ::nWidth, ::nHeight, ::aGradOver, .F. )
            hOldBrush = SelectObject( hDC, hBrush )

            Roundrect( hDC, nAdj, ;
                      ::nTop , ;
                      ::nWidth - nAdj, ;
                      ::nTop + ::nHeight, nRound, nRound )

            RoundBox( hDC, nAdj, ;
                      ::nTop, ;
                      ::nWidth - nAdj, ;
                      ::nTop + ::nHeight, nRound, nRound, ::nBorderClr )

            RoundBox( hDC, nAdj + 1, ;
                      ::nTop + 1, ;
                      ::nWidth - nAdj - 1, ;
                      ::nTop + ::nHeight - 1, nRound, nRound, nRGB( 255, 255, 255 ) )


            SelectObject( hDC, hOldBrush )
            DeleteObject( hBrush )
         endif
      endif

   endif
   hOldFont  = SelectObject( hDC, oCon:oFont:hFont  )

   nOldClr   = SetTextColor( hDC, If( ::lSelected, ::nClrTextOver, ::nClrText )  )

   nBmpH = 0
   nBmpW = 0
   if IsGdiObject( ::hBitmap )
      nBmpH = nBmpHeight( ::hBitmap )
      nBmpW = nBmpWidth( ::hBitmap )
      if ::lHasAlpha
         ABPaint( hDC, ::nLeftMargin, ::nTop + ( Int( ::nHeight / 2 ) - Int( ( nBmpH / 2 ) ) ),;
                  ::hBitmap, ::nAlphaLevel )
      else

         DrawTransparent( hDC, ::hBitmap, ;
                         ::nTop + ( Int( ::nHeight / 2 ) - Int( ( nBmpH / 2 ) ) ), ;
                         ::nLeftMargin )
      endif
   endif


   DrawTextTransparent( hDC, ::cPrompt, { ::nTop, ::nLeftMargin + nBmpW + If( nBmpW > 0, 5, 0 ), ;
                             ::nTop + ::nHeight, ::nWidth - If( ::nType == BCKTYPE_BUTTON, nAdj, 0 ) - 2 }, ;
                             nOR( DT_VCENTER, DT_SINGLELINE ) )
   SetTextColor( hDC, nOldClr )
   SelectObject( hDC, hOldFont )

RETURN NIL

//-------------------------------------------------------------------//
//-------------------------------------------------------------------//
static function CheckArray( aArray )

   if ValType( aArray ) == 'A' .and. ;
      Len( aArray ) == 1 .and. ;
      ValType( aArray[ 1 ] ) == 'A'

      aArray   := aArray[ 1 ]
   endif

return aArray
