
/*
 *
 * Proyecto: FiveUI
 * Fichero:  WndUI10.prg
 * Descripci�n: Test Clase WindowsUI, y clases heredadas
 *              Definicion de Ventana:
 *              - Entorno UI
 * Autor: Cristobal Navarro Lopez (C)
 * Fecha: 03/10/2013
 *
 */


#include "Fivewin.ch"

#include "colores.ch"


Static   oWinUI
Static   nRefresh          := 0

Function Main()
Local lMax    := .T.
Local lEsMdi  := .F.
Local nColor  := METRO_AZUL3
Local cTit    := "Inicio"
Local cUser   := WNetGetUser()
//Local aBtt    := { ".\Res\Guest32.bmp", ".\Res\Guest32.bmp" }
Local aBtt    := { { ".\Res\Guest32.bmp", ".\Res\Guest32.bmp" }, ;
                   { ".\Res\OnOff32.bmp", ".\Res\OnOff32.bmp" }, ;
                   { ".\Res\Busca32.bmp", ".\Res\Busca32.bmp" } }

   // Sets generales---------------------------------------------------------
   SET EPOCH TO 1990               // Admite los a�os desde el 1990 en adelante
   SET CENTURY ON                  // 4 d�gitos a�o
   SET DELETED ON                  // Impedir ver registros marcados borrar
   //SetCancel( .F. )              // Inutiliza ALT + C para abortar programa
   //SetDialogEsc( .F. )           // Impide salir Di�logos con Escape
   SET DATE FORMAT "DD/MM/YYYY"
   SET DECIMALS TO 2
   XBrNumFormat( "E", .T.)

   SetResDebug( .T. )

   //---------------------------     -----------------------------------------//
   // Modelo 001:  Ventana: Panel principal
   //                       En ventana principal: NO MDI (No es necesario MDI)
   //---------------------------     -----------------------------------------//

   oWinUI := TWindowsUI():PnelPPal( .T., lMax, lEsMdi, nColor, cTit, cUser, aBtt )
   oWinUI:ActivaUI()

   Hb_GCall(.t.)
   CLEAR MEMORY

   if File( "checkres.txt" )
      FErase( "checkres.txt" )
   endif
   CheckRes()

Return nil

//----------------------------------------------------------------------------//

//----------------------------------------------------------------------------//

//----------------------------------------------------------------------------//

//----------------------------------------------------------------------------//

//----------------------------------------------------------------------------//
